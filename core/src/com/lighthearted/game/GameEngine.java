package com.lighthearted.game;

import com.badlogic.gdx.Gdx;

import com.lighthearted.LightheartEd;
import com.lighthearted.game.cam.CamHandler;
import com.lighthearted.game.entities.mage.Mage;
import com.lighthearted.game.entities.player.Player;
import com.lighthearted.game.hud.StageHandler;
import com.lighthearted.game.input.InputHandler;
import com.lighthearted.game.map.MapHandler;
import com.lighthearted.game.objects.GameObjects;
import com.lighthearted.game.physics.Physics;
import com.lighthearted.game.sound.SoundHandler;


//consists of all modules needed to render the game
public class GameEngine {
    private static final String TAG = GameEngine.class.getName();

    private LightheartEd main;

    public static Updater updater;
    public static Renderer renderer;

    public static CamHandler camHandler;
    public static Player player;
    public static Mage mage;

    public static InputHandler inputHandler;
    public static MapHandler mapLoader;
    public static Physics physics;
    public static StageHandler stageHandler;
    public static SoundHandler soundHandler;

    public static GameObjects gameObjects;

    public static boolean hardmode;

    public GameEngine(LightheartEd main, boolean hardmode){
        this.main = main;
        this.hardmode = hardmode;
        init();
    }

    public void init(){

        camHandler = new CamHandler(main);
        mapLoader = new MapHandler(main);
        physics = new Physics(main);
        stageHandler = new StageHandler(main);
        inputHandler = new InputHandler(main);
        gameObjects = new GameObjects(main);
        soundHandler = new SoundHandler(main);
        //logic related updates
        updater = new Updater(main);
        //drawing related updates
        renderer = new Renderer(main);
    }

    // game loop
    public void run(float delta){
        updater.update(delta);
        renderer.render(delta);

    }
}
