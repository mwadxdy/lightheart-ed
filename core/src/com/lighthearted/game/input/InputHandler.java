package com.lighthearted.game.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;

public class InputHandler extends InputInterface{
    private static final String TAG = InputHandler.class.getName();

    private LightheartEd main;

    public KeyboardInput keyboardInput;
    public TouchInput touchInput;

    public InputHandler(LightheartEd main){
        this.main = main;

        keyboardInput = new KeyboardInput(main);
        touchInput = new TouchInput(main);

        setupInputProcessor();
    }

    public void update(float delta){
        keyboardInput.update();
        touchInput.update();
    }


    private void setupInputProcessor(){
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        touchInput.initGestureDetector();

        //touchdownhandler must be added before stage!
        inputMultiplexer.addProcessor(touchInput);
        inputMultiplexer.addProcessor(keyboardInput);
        inputMultiplexer.addProcessor(touchInput.gestureDetector);
        inputMultiplexer.addProcessor(GameEngine.stageHandler.stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }


}
