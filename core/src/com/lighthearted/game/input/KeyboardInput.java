package com.lighthearted.game.input;
import com.badlogic.gdx.Gdx;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;

public  class KeyboardInput extends InputInterface  {
    private static final String TAG = KeyboardInput.class.getName();
    
    private LightheartEd main;
    
    public KeyboardInput(LightheartEd main){
        this.main = main;
    }


    public void update(){
        //move player left - A
        if(Gdx.input.isKeyPressed(29)){
            GameEngine.player.movement.moveLeft();
            GameEngine.inputHandler.touchInput.isLeftTouched = true;
        }

        //move player right - D
        if(Gdx.input.isKeyPressed(32)){
            GameEngine.player.movement.moveRight();
            GameEngine.inputHandler.touchInput.isRightTouched = true;

        }


        //jump - SPACE
        if(Gdx.input.isKeyJustPressed(62) || Gdx.input.isKeyJustPressed(144) || Gdx.input.isKeyJustPressed(51)){
            GameEngine.player.movement.jump();

        }
    }


    //ZOOM - MOUSE
    @Override
    public boolean scrolled(int amount) {
        GameEngine.camHandler.zoom(amount);
        return super.scrolled(amount);
    }




}
