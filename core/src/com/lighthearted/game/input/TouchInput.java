package com.lighthearted.game.input;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public  class TouchInput extends InputInterface {
    private static final String TAG = TouchInput.class.getName();
    
    private LightheartEd main;
    public GestureDetector gestureDetector;
    public boolean isTouched;

    public Rectangle leftRect, rightRect, jumpRect, toolRect, toolMenuRect,
            toolMenuTopLeftRect, toolMenuTopRightRect,
            toolMenuBottomLeftRect, toolMenuBottomRightRect,
            toolQuickslot1Rect, toolQuickslot2Rect, toolQuickslot3Rect;

    public boolean isJumpTouched,
            isLeftTouched, isRightTouched,
            isToolTouched, isElseTouched,
            isToolMenuButtonTouched,
            isQuickToolPanelOpened = false,
            isQuickToolPanelTouched = false;

    public float pushCooldown = 0;
    public float shootCooldown = 0f;

    public float previousDelta = 0;
    public double delta = 0;

    public TouchInput(LightheartEd main){
        this.main = main;
        createUIRects();
    }

    public void update(){
        buttonTouchCheck();
    }

    public void buttonTouchCheck(){
        isTouched = Gdx.input.isTouched();

        if (!(isTouched)){
            if(!(Gdx.input.isKeyPressed(32) || Gdx.input.isKeyPressed(29))){
                isLeftTouched = false;
                isRightTouched = false;
            }
            if(isQuickToolPanelOpened) {
                GameEngine.stageHandler.quickMenuPanelSwitch();
                isQuickToolPanelOpened = false;
            }

            isJumpTouched = false;
            isToolTouched = false;
            isElseTouched = false;
            GameEngine.player.isShooting = false;
        }

        for (int i = 0; i<3; i++){

            if (Gdx.input.isTouched(i)){
                Vector3 touchPos = new Vector3(Gdx.input.getX(i), Gdx.input.getY(i), 0);

                GameEngine.camHandler.UIcam.unproject(touchPos);
                GameEngine.camHandler.UIcam.update();
                Rectangle touch = new Rectangle(touchPos.x, touchPos.y, 32, 32);


                Vector3 touchPosFFCanon = new Vector3(Gdx.input.getX(i), Gdx.input.getY(i), 0);

                GameEngine.camHandler.gameCam.unproject(touchPosFFCanon);
                GameEngine.camHandler.gameCam.update();
                Rectangle touchFFCanon = new Rectangle(touchPosFFCanon.x, touchPosFFCanon.y, 4, 4);


                /*if(GameEngine.stageHandler.isToolMenuPanelVisible){
                    if(touch.overlaps(toolMenuTopLeftRect)) {
                        GameEngine.player.actions.equipTool(GameEngine.stageHandler.topLeftTool);
                    }else if(touch.overlaps(toolMenuTopRightRect)) {
                        GameEngine.player.actions.equipTool(GameEngine.stageHandler.topRightTool);
                    }else if(touch.overlaps(toolMenuBottomLeftRect)) {
                    }else if(touch.overlaps(toolMenuBottomRightRect)) {
                    }
                }*/
                if(GameEngine.stageHandler.isQuicktoolMenupanelVisible){
                    if(touch.overlaps(toolQuickslot1Rect)) { //QUICKSLOT1
                    }else if(touch.overlaps(toolQuickslot2Rect)) { //QUICKSLOT2
                    }else if(touch.overlaps(toolQuickslot3Rect)) { //QUICKSLOT3
                    }
                }
                if(touch.overlaps(leftRect)){ //LEFT
                    if(!touch.overlaps(toolMenuRect)) {
                        isToolMenuButtonTouched = false;
                    }
                    if(!isToolMenuButtonTouched && isQuickToolPanelOpened){
                        isQuickToolPanelOpened = false;
                    }
                    isRightTouched = false;
                    isLeftTouched = true;
                    GameEngine.player.movement.moveLeft();
                }else if(touch.overlaps(rightRect)) { //RIGHT
                    if(!touch.overlaps(toolMenuRect)) { //TOOLMENU
                        isToolMenuButtonTouched = false;
                    }
                    if(!isToolMenuButtonTouched && isQuickToolPanelOpened){
                        isQuickToolPanelOpened = false;
                    }
                    isLeftTouched = false;
                    isRightTouched = true;
                    GameEngine.player.movement.moveRight();
                }else if(touch.overlaps(toolRect)) { //TOOLBUTTON
                    isToolTouched = true;

                }else if(touch.overlaps(jumpRect)) { // JUMP
                    if(!touch.overlaps(toolMenuRect)) { //TOOLMENU
                        isToolMenuButtonTouched = false;
                    }
                    if(!isToolMenuButtonTouched && isQuickToolPanelOpened){
                        isQuickToolPanelOpened = false;
                    }

                    if(GameEngine.player.isPogoing && GameEngine.player.body.getLinearVelocity().y > 0){
                        pushCooldown += delta;
                        if(pushCooldown < 0.2f){
                            GameEngine.player.body.applyLinearImpulse(new Vector2(0, 0.5f), GameEngine.player.body.getPosition(), true);
                        }
                    }
                    isLeftTouched = false;
                    isRightTouched = false;
                    isJumpTouched = true;



                    if(GameEngine.player.actions.hook.hooked){
                        GameEngine.player.actions.hook.pullRope();
                    }
                }else if(touch.overlaps(toolMenuRect)) { //TOOLMENU
                    isToolMenuButtonTouched = true;
                    if(!isQuickToolPanelOpened) {
                        GameEngine.stageHandler.quickMenuPanelSwitch();
                        isQuickToolPanelOpened = true;
                    }
                }else{ //ELSE TOUCHED
                    isToolMenuButtonTouched = false;

                    isElseTouched = true;
                    shootCooldown += Gdx.graphics.getDeltaTime();
                    if(shootCooldown > 0.1) {
                        if(GameEngine.player.energy > 0) {
                            GameEngine.player.actions.shootFireflies(touchFFCanon.x, touchFFCanon.y);
                        }
                        shootCooldown = 0;
                    }
                }
            }
        }
    }



    // input-processor for gestures
    public void initGestureDetector(){

        gestureDetector = new GestureDetector(20, 0.5f, 2, 0.15f, this);
    }

    public void createUIRects(){
        leftRect = new Rectangle(Constants.LEFT_BUTTON_X, Constants.LEFT_BUTTON_Y, Constants.LEFT_BUTTON_WIDTH, Constants.LEFT_BUTTON_HEIGHT);
        rightRect = new Rectangle(Constants.RIGHT_BUTTON_X, Constants.RIGHT_BUTTON_Y, Constants.RIGHT_BUTTON_WIDTH,  Constants.RIGHT_BUTTON_HEIGHT);

        jumpRect = new Rectangle(Constants.JUMP_BUTTON_X, Constants.JUMP_BUTTON_Y, Constants.JUMP_BUTTON_WIDTH, Constants.JUMP_BUTTON_HEIGHT);

        toolRect = new Rectangle(Constants.THROW_BUTTON_X, Constants.THROW_BUTTON_Y, Constants.THROW_BUTTON_WIDTH, Constants.THROW_BUTTON_HEIGHT);


        //toolmenu
        toolMenuRect = new Rectangle(Constants.TOOLMENU_BUTTON_X,
                Constants.TOOLMENU_BUTTON_Y,
                Constants.TOOLMENU_BUTTON_WIDTH,
                Constants.TOOLMENU_BUTTON_HEIGHT);

        //quickslot1
        toolQuickslot1Rect = new Rectangle(Constants.TOOLMENU_QUICKSLOT1_X,
                Constants.TOOLMENU_QUICKSLOT1_Y,
                Constants.TOOLMENU_QUICKSLOT1_WIDTH,
                Constants.TOOLMENU_QUICKSLOT1_HEIGHT);

        //quickslot2
        toolQuickslot2Rect = new Rectangle(Constants.TOOLMENU_QUICKSLOT2_X,
                Constants.TOOLMENU_QUICKSLOT2_Y,
                Constants.TOOLMENU_QUICKSLOT2_WIDTH,
                Constants.TOOLMENU_QUICKSLOT2_HEIGHT);

        //quickslot3
        toolQuickslot3Rect = new Rectangle(Constants.TOOLMENU_QUICKSLOT3_X,
                Constants.TOOLMENU_QUICKSLOT3_Y,
                Constants.TOOLMENU_QUICKSLOT3_WIDTH,
                Constants.TOOLMENU_QUICKSLOT3_HEIGHT);



        //top left
        toolMenuTopLeftRect = new Rectangle(Constants.TOOLMENU_TOP_LEFT_BUTTON_X,
                Constants.TOOLMENU_TOP_LEFT_BUTTON_Y,
                Constants.TOOLMENU_TOP_LEFT_BUTTON_WIDTH,
                Constants.TOOLMENU_TOP_LEFT_BUTTON_HEIGHT);



        //top right
        toolMenuTopRightRect = new Rectangle(Constants.TOOLMENU_TOP_RIGHT_BUTTON_X,
                Constants.TOOLMENU_TOP_RIGHT_BUTTON_Y,
                Constants.TOOLMENU_TOP_RIGHT_BUTTON_WIDTH,
                Constants.TOOLMENU_TOP_RIGHT_BUTTON_HEIGHT);



        //bottom left
        toolMenuBottomLeftRect = new Rectangle(Constants.TOOLMENU_BOTTOM_LEFT_BUTTON_X,
                Constants.TOOLMENU_BOTTOM_LEFT_BUTTON_Y,
                Constants.TOOLMENU_BOTTOM_LEFT_BUTTON_WIDTH,
                Constants.TOOLMENU_BOTTOM_LEFT_BUTTON_HEIGHT);


        //bottom right
        toolMenuBottomRightRect = new Rectangle(Constants.TOOLMENU_BOTTOM_RIGHT_BUTTON_X,
                Constants.TOOLMENU_BOTTOM_RIGHT_BUTTON_Y,
                Constants.TOOLMENU_BOTTOM_RIGHT_BUTTON_WIDTH,
                Constants.TOOLMENU_BOTTOM_RIGHT_BUTTON_HEIGHT);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        Vector3 touchPos = new Vector3(screenX, screenY, 0);
        GameEngine.camHandler.UIcam.unproject(touchPos);
        GameEngine.camHandler.UIcam.update();
        Rectangle touch = new Rectangle(touchPos.x-16, touchPos.y-16, 32, 32);
        if(GameEngine.mage.isSpeaking){
            GameEngine.mage.thinkingTime = 5;

        }
        // JUMP - BUTTON DOWN
        if (touch.overlaps(jumpRect)) {
          /*  if(GameEngine.player.actions.hook.hooked){
                GameEngine.player.actions.hook.pullRope();
            }else{
                GameEngine.player.movement.jump();

            }*/
            GameEngine.player.movement.jump();

        }

        // TOOL - BUTTON DOWN
        if (touch.overlaps(toolRect)) {

            GameEngine.player.actions.useTool();
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    //ZOOM
    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        /*Gdx.app.log(TAG, "initialPointer1 " + initialPointer1);
        Gdx.app.log(TAG, "initialPointer2 " + initialPointer2);
        Gdx.app.log(TAG, "pointer1 " + pointer1);
        Gdx.app.log(TAG, "pointer2 " + pointer2);*/

        float a = pointer1.x - pointer2.x;
        float b = pointer1.y - pointer2.y;

        previousDelta = (float) delta;
        delta = (float) (Math.sqrt(Math.abs(a)) + Math.sqrt(Math.abs(b)));

        if(!GameEngine.inputHandler.touchInput.isJumpTouched && !GameEngine.inputHandler.touchInput.isRightTouched && !GameEngine.inputHandler.touchInput.isLeftTouched
                && !GameEngine.inputHandler.touchInput.isRightTouched && !GameEngine.inputHandler.touchInput.isLeftTouched && GameEngine.player.body.getLinearVelocity().x == 0) {


            if (GameEngine.camHandler.gameCam.zoom > 0.2f && GameEngine.camHandler.gameCam.zoom < 2) {
                if (delta < previousDelta) {
                    GameEngine.camHandler.gameCam.zoom += 0.001111111f;
                } else {
                    GameEngine.camHandler.gameCam.zoom -= 0.001111111f;

                }
            } else {
                if (GameEngine.camHandler.gameCam.zoom < 1) {
                    GameEngine.camHandler.gameCam.zoom = 0.21f;
                } else {
                    GameEngine.camHandler.gameCam.zoom = 1.99f;

                }
            }
        }

        return super.pinch(initialPointer1, initialPointer2, pointer1, pointer2);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        Vector3 touchPos = new Vector3(screenX, screenY, 0);
        GameEngine.camHandler.UIcam.unproject(touchPos);
        GameEngine.camHandler.UIcam.update();
        Rectangle touch = new Rectangle(touchPos.x-16, touchPos.y-16, 32, 32);

        if (touch.overlaps(toolMenuRect)) {
            if(GameEngine.player.actions.currentTool.equals("hook")){
                GameEngine.player.actions.equipTool("pogo");
            }else if(GameEngine.player.actions.currentTool.equals("pogo")){
                GameEngine.player.actions.equipTool("hook");
            }
        }

        // TOOL - BUTTON DOWN
        if (touch.overlaps(toolQuickslot1Rect)) {
            GameEngine.player.actions.equipTool(GameEngine.stageHandler.quicktool1_string);
        }else if (touch.overlaps(toolQuickslot2Rect)) {
            GameEngine.player.actions.equipTool(GameEngine.stageHandler.quicktool2_string);
        }else if (touch.overlaps(toolQuickslot3Rect)) {
            //GameEngine.player.actions.equipTool(GameEngine.stageHandler.quicktool3_string);

        }





        return super.touchUp(screenX, screenY, pointer, button);
    }
}
