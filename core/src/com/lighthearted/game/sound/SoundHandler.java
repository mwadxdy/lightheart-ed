package com.lighthearted.game.sound;
import com.lighthearted.LightheartEd;

public class SoundHandler{
    private static final String TAG = SoundHandler.class.getName();


        private LightheartEd main;
        public boolean musicButtonPressed = false;

        public SoundHandler(LightheartEd main){
            this.main = main;


        }

        public void update(){
      /*  if(musicButtonPressed){
            if(musicToogle){
                main.assets.menuMusic.stop();
                musicToogle = false;
            }else {
                main.assets.menuMusic.setLooping(true);
                main.assets.menuMusic.play();
                musicToogle = true;
            }
            musicButtonPressed = false;

        }*/
        }

        public void musicToggle(){
            if(main.assets.isSoundActivated){
                main.assets.menuMusic.stop();
                main.assets.isSoundActivated = false;
            }else {
                main.assets.menuMusic.setLooping(true);
                main.assets.menuMusic.play();
                main.assets.isSoundActivated = true;
            }
        }
}
