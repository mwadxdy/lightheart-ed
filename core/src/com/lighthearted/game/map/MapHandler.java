package com.lighthearted.game.map;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.lighthearted.LightheartEd;

public class MapHandler {
    private static final String TAG = MapHandler.class.getName();

    private LightheartEd main;
    public TiledMap gameWorld;

    public MapHandler(LightheartEd main){
        this.main = main;
        loadMap();
    }

    public void loadMap() {
        gameWorld = new TmxMapLoader().load("map/level0.tmx");
    }




}
