package com.lighthearted.game.map.environment;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class Polylines   {
    private static final String TAG = Polylines.class.getName();

    private LightheartEd main;

    public Polylines(LightheartEd main){
        this.main = main;

    }
    
    public void update(float delta){
    
    }


    public void createPolylines() {
        Shape gShape;
        Body body;
        BodyDef bdef;
        FixtureDef fdef;
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_POLYLINES).getObjects().getByType(PolylineMapObject.class)) {

            bdef = new BodyDef();
            bdef.type = BodyDef.BodyType.StaticBody;

            gShape = createPolyline((PolylineMapObject) object);

            body = B2DWorldCreator.world.createBody(bdef);
            body.setUserData(this);

            fdef = new FixtureDef();
            fdef.shape = gShape;
            fdef.friction = 0f;
            fdef.filter.categoryBits = Constants.CATEGORY_SCENERY;
            fdef.filter.maskBits = Constants.MASK_SCENERY;
            body.createFixture(fdef);
        }
    }


    private ChainShape createPolyline(PolylineMapObject polyline) {

        float[] vertices = polyline.getPolyline().getTransformedVertices();

        Vector2[] worldVertices = new Vector2[vertices.length / 2];

        for (int i = 0; i < worldVertices.length; i++) {
            float x = vertices[i * 2] / Constants.PPM;
            float y = vertices[i * 2 + 1] / Constants.PPM;
            worldVertices[i] = new Vector2(x, y);
        }

        ChainShape cs = new ChainShape();
        cs.createChain(worldVertices);
        return cs;
    }
}
