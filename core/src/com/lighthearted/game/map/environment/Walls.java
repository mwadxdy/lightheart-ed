package com.lighthearted.game.map.environment;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.utils.Constants;

public class Walls   {
    private static final String TAG = Walls.class.getName();

    private LightheartEd main;

    public Walls(LightheartEd main){
        this.main = main;

    }
    
    public void update(float delta){
    
    }

    public void createWalls() {

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_WALLS).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            B2DBodyCreator.createBox((rect.getX() + rect.getWidth()/2)/ Constants.PPM,
                    (rect.getY() + rect.getHeight()/2)/Constants.PPM,
                    (rect.getWidth() / 2)/Constants.PPM,
                    (rect.getHeight() / 2)/Constants.PPM,
                    BodyDef.BodyType.StaticBody,
                    0, false, 1, 0,
                    Constants.CATEGORY_SCENERY, Constants.MASK_SCENERY,
                    this);
        }
    }
}
