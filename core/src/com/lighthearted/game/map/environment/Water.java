package com.lighthearted.game.map.environment;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class Water extends Sprite {
    private static final String TAG = Water.class.getName();

    private LightheartEd main;



    public Water(LightheartEd main, float x, float y, float width, float height){
        this.main = main;

        setupSprite(x,y,width,height);
        spawnLiquid();
    }


    public void setupSprite(float x, float y, float width, float height){
        setRegion(main.assets.gameObjectAtlas.findRegion("Water0"));
        setBounds(x, y, width, height);
        setOrigin(getWidth()/2, getHeight()/2);
    }


    public void spawnLiquid() {
        Body body;
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_WATER).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2)/ Constants.PPM, (rect.getY() + rect.getHeight() / 2)/Constants.PPM);
            body = B2DWorldCreator.world.createBody(bdef);
            body.setUserData(this);

            shape.setAsBox((rect.getWidth() / 2)/Constants.PPM, (rect.getHeight() / 2)/Constants.PPM);

            fdef.shape = shape;
            fdef.friction = 1;
            fdef.isSensor = true;
            fdef.filter.categoryBits = Constants.CATEGORY_SCENERY;
            fdef.filter.maskBits = Constants.MASK_SCENERY;
            body.createFixture(fdef).setUserData(this);
        }
    }


    public void draw(Batch batch){
        if(GameEngine.player.isSwimming){
            setAlpha(0.5f);
        }else{
            setAlpha(1);
        }
        super.draw(batch);
    }
}
