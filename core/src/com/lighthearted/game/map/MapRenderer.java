package com.lighthearted.game.map;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.mage.strategies.MageRenderer;
import com.lighthearted.game.entities.player.strategies.PlayerRenderer;
import com.lighthearted.game.map.environment.Water;
import com.lighthearted.game.objects.actions.Altar;
import com.lighthearted.game.objects.animals.Bear;
import com.lighthearted.game.objects.actions.Torch;
import com.lighthearted.game.objects.animals.Kingfisher;
import com.lighthearted.game.objects.collectables.HealthPotion;
import com.lighthearted.game.objects.collectables.SunStone;
import com.lighthearted.game.objects.environment.LavaPlatform;
import com.lighthearted.utils.Constants;

public  class MapRenderer   {
    private static final String TAG = MapRenderer.class.getName();

    private LightheartEd main;

    private PlayerRenderer playerRenderer;
    private MageRenderer mageRenderer;

    public OrthogonalTiledMapRenderer tiledMapRenderer;

    private int[] layer0 = { 0 };
    private int[] layer1 = { 1 };
    private int[] layer2 = { 2 };
    private int[] layer3 = { 3 };
    private int[] layerForeground = { 4 , 5 , 6 };

    public Sprite skyTint;
    private float randR, randG, randB;

    public MapRenderer(LightheartEd main){
        this.main = main;
        playerRenderer = new PlayerRenderer(main);
        mageRenderer = new MageRenderer(main);

        initTiledMapRenderer();
    }

    public void initTiledMapRenderer(){

        tiledMapRenderer = new OrthogonalTiledMapRenderer(GameEngine.mapLoader.gameWorld, 1 / Constants.PPM);
        skyTint = new Sprite();
        skyTint.setRegion(main.assets.gameObjectAtlas.findRegion("SkyTint0"));
        skyTint.setBounds(0,0, 1000, 1000);

        /*randR = MathUtils.random(40,50);
        randG = MathUtils.random(120,130);
        randB = MathUtils.random(240,250); blueish*/

        randR = MathUtils.random(100,250);
        randG = MathUtils.random(0,50);
        randB = MathUtils.random(100,230);

        skyTint.setColor(randR / 255f, randG / 255f, randB / 255f, .3f);

    }


    public void render(float delta){
        renderSky();
        renderMountains();
        renderHills();
        renderPlataeu();

        renderTorches();
        renderLavaPlatforms();
        renderAltars();
        renderEnvironment();

        playerRenderer.render(delta);
        renderAnimals(delta);
        mageRenderer.render(delta);

        renderWater();

        GameEngine.physics.lightning.render();
        if(GameEngine.physics.b2DWorldCreator.hookTreasure != null){
            renderHookTreasure();
        }
        if(GameEngine.physics.b2DWorldCreator.pogoTreasure != null){
            renderPogoTreasure();
        }

        renderSunStones();
        renderHealthPotions();

    }



    public void renderSky(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.layer0Cam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer0Cam);
        tiledMapRenderer.render(layer0);
        main.batch.end();

        main.batch.setProjectionMatrix(GameEngine.camHandler.layer0CopyCam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer0CopyCam);
        tiledMapRenderer.render(layer0);
        main.batch.end();

        main.batch.setProjectionMatrix(GameEngine.camHandler.layer0CopyCam.combined);
        main.batch.begin();
        skyTint.draw(main.batch);
        main.batch.end();
    }

    public void renderMountains(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.layer1Cam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer1Cam);
        tiledMapRenderer.render(layer1);

        main.batch.end();

        main.batch.setProjectionMatrix(GameEngine.camHandler.layer1CopyCam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer1CopyCam);
        tiledMapRenderer.render(layer1);
        main.batch.end();

    }

    public void renderHills(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.layer2Cam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer2Cam);
        tiledMapRenderer.render(layer2);
        main.batch.end();

      /*  main.batch.setProjectionMatrix(GameEngine.camHandler.layer2CopyCam.combined);
        main.batch.begin();
        layer2Renderer.setView(GameEngine.camHandler.layer2CopyCam);
        layer2Renderer.render(layer2);
        main.batch.end();*/
    }

    public void renderPlataeu(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.layer3Cam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.layer3Cam);
        tiledMapRenderer.render(layer3);
        main.batch.end();

       /* main.batch.setProjectionMatrix(GameEngine.camHandler.layer3CopyCam.combined);
        main.batch.begin();
        layer3Renderer.setView(GameEngine.camHandler.layer3CopyCam);
        layer3Renderer.render(layer3);
        main.batch.end();*/
    }

    public void renderEnvironment(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        tiledMapRenderer.setView(GameEngine.camHandler.gameCam);
        tiledMapRenderer.render(layerForeground);
        main.batch.end();
    }

    public void renderTorches(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(Torch torch : GameEngine.physics.b2DWorldCreator.torchList) {
            torch.draw(main.batch);
        }
        main.batch.end();
    }

    public void renderLavaPlatforms(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(LavaPlatform lavaPlatform : GameEngine.physics.b2DWorldCreator.lavaPlatformList) {
            lavaPlatform.draw(main.batch);
        }
        main.batch.end();
    }




    public void renderAnimals(float delta){

        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(Bear bear : GameEngine.physics.b2DWorldCreator.bearList) {
            bear.draw(main.batch, delta);
        }
        main.batch.end();

        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(Kingfisher kingfisher : GameEngine.physics.b2DWorldCreator.kingfisherList) {
            kingfisher.draw(main.batch);
        }
        main.batch.end();
    }

    public void renderSunStones(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(SunStone sunStone : GameEngine.physics.b2DWorldCreator.sunstoneList) {
            sunStone.draw(main.batch);
        }
        main.batch.end();

    }

    public void renderHealthPotions(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(HealthPotion healthPotion : GameEngine.physics.b2DWorldCreator.healthPotionList) {
            healthPotion.draw(main.batch);
        }
        main.batch.end();

    }

    public void renderHookTreasure(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        GameEngine.physics.b2DWorldCreator.hookTreasure.draw(main.batch);
        main.batch.end();

    }

    public void renderPogoTreasure(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        GameEngine.physics.b2DWorldCreator.pogoTreasure.draw(main.batch);
        main.batch.end();

    }



    public void renderAltars(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(Altar altar : GameEngine.physics.b2DWorldCreator.altarList) {
            altar.draw(main.batch);
        }
        main.batch.end();

    }

    public void renderWater(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        for(Water water : GameEngine.physics.b2DWorldCreator.waterList) {
            water.draw(main.batch);
        }
        main.batch.end();
    }


}
