package com.lighthearted.game;
import com.badlogic.gdx.utils.Array;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.objects.animals.Firefly;
import com.lighthearted.screens.GameScreen;
import com.lighthearted.screens.LoadingScreen;
import com.lighthearted.utils.Constants;

public  class Updater   {
    private static final String TAG = Updater.class.getName();
    
    private LightheartEd main;
    public Array<Firefly> fireflyList;



    public Updater(LightheartEd main){
        this.main = main;
        fireflyList = new Array<Firefly>();
    }

    public void update(float delta){
        GameEngine.stageHandler.update(delta);
        GameEngine.camHandler.update(delta);
        if(GameEngine.player.isAlive) {
            GameEngine.inputHandler.update(delta);
        }

        GameEngine.player.update(delta);
        GameEngine.mage.update(delta);
        GameEngine.gameObjects.update(delta);
        GameEngine.physics.update(delta);
    }

    public void restart(){
        main.setScreen(new LoadingScreen(main, GameEngine.hardmode));
    }


}
