package com.lighthearted.game.objects;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.objects.actions.Altar;
import com.lighthearted.game.objects.animals.Bear;
import com.lighthearted.game.objects.actions.Torch;
import com.lighthearted.game.objects.animals.Kingfisher;
import com.lighthearted.game.objects.collectables.HealthPotion;
import com.lighthearted.game.objects.collectables.SunStone;
import com.lighthearted.game.objects.environment.LavaPlatform;

public class GameObjects   {
    private static final String TAG = GameObjects.class.getName();

    private LightheartEd main;

    public boolean firefliesAlive = false;

    /**
     * @see com.lighthearted.game.map.MapRenderer
     */
    public GameObjects(LightheartEd main){
        this.main = main;
    }

    public void update(float delta){
        updateFireflies(delta);
        updateTorches(delta);
        updateBears(delta);
        updateKingfishers(delta);
        updateSunStones(delta);
        updateHealthPotions(delta);
        updateLavaPlatforms(delta);
        updateAltars(delta);
        updateHookTreasure(delta);
        updatePogoTreasure(delta);

    }

    public void updateFireflies(float delta){
        firefliesAlive = false;
        for (com.lighthearted.game.objects.animals.Firefly firefly : GameEngine.updater.fireflyList) {
            if(firefly.isAlive) {
                firefly.update();
                firefliesAlive = true;
            }
        }
        if(!firefliesAlive && GameEngine.updater.fireflyList.size > 0){
            GameEngine.updater.fireflyList.clear();
        }
    }

    public void updateTorches(float delta){

        for (Torch torch : GameEngine.physics.b2DWorldCreator.torchList) {
            torch.update(delta);
        }
    }

    public void updateHookTreasure(float delta){
        if(GameEngine.physics.b2DWorldCreator.hookTreasure != null) {
            GameEngine.physics.b2DWorldCreator.hookTreasure.update(delta);
        }
    }

    public void updatePogoTreasure(float delta){
        if(GameEngine.physics.b2DWorldCreator.pogoTreasure != null) {
            GameEngine.physics.b2DWorldCreator.pogoTreasure.update(delta);
        }
    }
    public void updateBears(float delta){

        for (Bear bear : GameEngine.physics.b2DWorldCreator.bearList) {
            bear.update(delta);
        }
    }

    public void updateKingfishers(float delta){
        for (Kingfisher kingfisher : GameEngine.physics.b2DWorldCreator.kingfisherList) {
            kingfisher.update(delta);
        }
    }

    public void updateSunStones(float delta){
        for (SunStone sunStone : GameEngine.physics.b2DWorldCreator.sunstoneList) {
            sunStone.update(delta);
        }
    }

    public void updateHealthPotions(float delta){
        for (HealthPotion healthPotion : GameEngine.physics.b2DWorldCreator.healthPotionList) {
            healthPotion.update(delta);
        }
    }

    public void updateLavaPlatforms(float delta){
        for (LavaPlatform lavaPlatform : GameEngine.physics.b2DWorldCreator.lavaPlatformList) {
            lavaPlatform.update(delta);
        }
    }


    public void updateAltars(float delta){
        for (Altar altar : GameEngine.physics.b2DWorldCreator.altarList) {
            altar.update(delta);
        }
    }
}
