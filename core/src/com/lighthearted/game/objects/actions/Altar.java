package com.lighthearted.game.objects.actions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.Lightning;
import com.lighthearted.utils.Constants;

import box2dLight.PointLight;

public class Altar extends Sprite {
    private static final String TAG = Altar.class.getName();

    private LightheartEd main;

    public Body body;
    private PointLight altarlight;
    private float runTime;

    private int holes = 0;
    public int hits = 0;
    public boolean isHealed;

    private float r, g, b;
    private float distance = 1f;
    public Vector2 spawnCoordinates;
    public Sprite sunSprite;
    public Altar(LightheartEd main){
        this.main = main;

    }

    public Altar(LightheartEd main, float x, float y, Lightning lightning, boolean healed, int mapX, int mapY) {
        this.main = main;
        spawnCoordinates = new Vector2(mapX, mapY);
        isHealed = healed;
        if(healed){
            this.holes = Constants.ALTAR_FRAME_NR;
            distance = 5;
        }
        setupSprite(x,y);
        createBody(lightning);
        setupLight(lightning);


    }

    public void update(float delta){
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - 7);

        if(hits > 1 && holes < Constants.ALTAR_FRAME_NR){
            hits = 0;
            holes++;
            if(distance < 5) {
                distance += 0.2;
            }

            if(r < 200){
                r += 10;

            }

            if(g < 80) {
                g += 5;
            }

            if(b > 0) {
                b -= 1;
            }


            altarlight.setColor(r / 255f, g /255f , b / 255f, 1);
            altarlight.setDistance(distance);
        }

        if(!isHealed && holes >= Constants.ALTAR_FRAME_NR){
            isHealed = true;
            GameEngine.physics.lightning.globalLightness += 0.05f;
            GameEngine.physics.lightning.toLit = true;

            GameEngine.physics.b2DWorldCreator.healedAltars += (int) spawnCoordinates.x + "," + (int) spawnCoordinates.y + ";";
            GameEngine.physics.b2DWorldCreator.altarsCleansed++;

            GameEngine.physics.b2DWorldCreator.save();
        }



    }

    public void hit(){
        hits++;
    }

    public void setupSprite(float x, float y){

        setBounds(x, y, 12f, 12f);
        setOrigin(getWidth()/2, getHeight()/2);
        sunSprite = new Sprite();
        sunSprite.setRegion(main.assets.gameObjectAtlas.findRegion("SunStone0"));
        sunSprite.setBounds(getX() - 2.9f,
                getY() - 2.8f,
                6,6);
        sunSprite.setOrigin(getWidth()/2, getHeight()/2);

    }






    public void createBody(Lightning lightning){
        body = B2DBodyCreator.createBox(getX(), getY(),
                2f, 2f,
                BodyDef.BodyType.StaticBody,
                0, true, 1, 0,
                Constants.CATEGORY_MONSTER, Constants.MASK_MONSTER,
                this);
    }

    public void setupLight(Lightning lightning){
        r = 40;
        g = 10;
        b = 150;


        altarlight = new PointLight(lightning.rayHandler, 32,
                new Color(r / 255f, g / 255f, b / 255f, 1),
                distance,
                getX()  , getY() + .2f );

        altarlight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.MASK_LIGHT);

        altarlight.setStaticLight(true);
    }


    public void draw(Batch batch) {
        if(holes <10){
            setRegion(main.assets.gameObjectAtlas.findRegion("Altar0" + holes));
        }else{
            setRegion(main.assets.gameObjectAtlas.findRegion("Altar" + holes));
        }


        super.draw(batch);
        if(isHealed){
            sunSprite.draw(main.batch);
        }

    }

    public void lit() {

    }


}
