package com.lighthearted.game.objects.actions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.Lightning;
import com.lighthearted.utils.Constants;

import box2dLight.PointLight;

public class Torch extends Sprite {
    private static final String TAG = Torch.class.getName();

    private LightheartEd main;
    public Body body;
    private PointLight torchlight;
    private boolean isUpdated = false;
    public Vector2 spawnCoordinates;

    public Torch(LightheartEd main){
        this.main = main;

    }

    public Torch(LightheartEd main, float x, float y, Lightning lightning, boolean lit, float mapX, float mapY) {
        this.main = main;
        spawnCoordinates = new Vector2(mapX, mapY);

        setupSprite(x,y);
        createBody(lightning);

        torchlight = new PointLight(lightning.rayHandler, 8,
                new Color(242f / 255f, 125f / 255f, 12f / 255f, 1),
                1.1f,
                getX() - 0.5f , getY() - 0.8f );

        torchlight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.MASK_LIGHT);

        torchlight.setStaticLight(true);

        if(lit){
            torchlight.setDistance(7);

        }
    }
    
    public void update(float delta){
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - 11.5f);

    }

    public void setupSprite(float x, float y){
        setRegion(main.assets.gameObjectAtlas.findRegion("Torch0"));
        setBounds(x, y, 15f, 15f);
        setOrigin(getWidth()/2, getHeight()/2);
    }


    public void createBody(Lightning lightning){
            body = B2DBodyCreator.createBox(getX(), getY(),
                    2f, 2f,
                    BodyDef.BodyType.StaticBody,
                    0, true, 1, 0,
                    Constants.CATEGORY_MONSTER, Constants.MASK_MONSTER,
                    this);
    }

    public void draw(Batch batch) {
        super.draw(batch);

    }

    public void lit() {
        if(torchlight.getDistance() < 7) {
            torchlight.setDistance(torchlight.getDistance() * 1.5f);
        }else {
            if(!isUpdated){
                isUpdated = true;
                GameEngine.physics.b2DWorldCreator.torchesLit++;
                GameEngine.physics.b2DWorldCreator.litTorches += (int) spawnCoordinates.x + "," + (int) spawnCoordinates.y + ";";

            }
        }
    }


}
