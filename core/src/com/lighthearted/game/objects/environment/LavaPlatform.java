package com.lighthearted.game.objects.environment;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class LavaPlatform extends Sprite {
    private static final String TAG = LavaPlatform.class.getName();

    private LightheartEd main;

    private Body body;

    public float runTime = 0, destroyTime = 0;

    public LavaPlatform(LightheartEd main) {
        this.main = main;
    }

    public LavaPlatform(LightheartEd main, float x, float y) {
        this.main = main;

        setRegion(main.assets.gameObjectAtlas.findRegion("LavaPlatform0"));
        setBounds(x, y, 15f, 10f);
        setOrigin(getWidth() / 2, getHeight() / 2);
        body = B2DBodyCreator.createBox(x, y,
                4, 1,
                BodyDef.BodyType.StaticBody,
                1, false, 0, 0,
                Constants.CATEGORY_SCENERY,
                Constants.MASK_SCENERY,
                this);
    }

    public void update(float delta) {
        setPosition(body.getPosition().x - getWidth() / 2,
                (body.getPosition().y - getHeight() / 2 ));

        setRegion(getFrame(delta));
    }

    public void draw(Batch batch) {
        super.draw(batch);
    }


    public TextureRegion getFrame(float delta) {
        TextureRegion region = new TextureRegion();

        runTime += delta;

        region = main.assets.lavaPlatformAnim.getKeyFrame(runTime);

        return region;
    }

}


