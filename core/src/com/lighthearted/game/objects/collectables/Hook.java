package com.lighthearted.game.objects.collectables;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class Hook extends Sprite {
    private static final String TAG = Hook.class.getName();

    private LightheartEd main;
    public Body body;
    public boolean toDestroy = false, destroyed;


    public Hook(LightheartEd main){
        this.main = main;

    }

    public Hook(LightheartEd main, float x, float y){
        this.main = main;

        setupSprite(x,y);
        createBody(x,y);
    }
    
    public void update(float delta){
        attachSprite();

        if(toDestroy ) {
            B2DWorldCreator.world.destroyBody(body);
            toDestroy = false;
            destroyed = true;
        }
    }

    public void attachSprite(){
        setPosition(body.getPosition().x - getWidth() / 2,
                (body.getPosition().y - getHeight() / 2 ));

    }


    public void setupSprite(float x, float y){
        setRegion(main.assets.gameObjectAtlas.findRegion("Hook0"));
        setBounds(x, y, 8.5f, 8.5f);
        setOrigin(getWidth()/2, getHeight()/2);
    }

    public void createBody(float x, float y){
        body = B2DBodyCreator.createCircle(x,y,
                3,
                BodyDef.BodyType.StaticBody,
                0, true, 0,
                Constants.CATEGORY_SCENERY, Constants.MASK_SCENERY,
                this);

    }

    @Override
    public void draw(Batch batch) {
        if(!destroyed) {
            super.draw(batch);
        }
    }


    public void collect(){
        toDestroy = true;
        GameEngine.player.actions.toolsAvailable += "hook;";
        //GameEngine.stageHandler.loadToolMenuTopLeft("hook");
        GameEngine.stageHandler.loadQuicktool1("hook");
        //GameEngine.stageHandler.isToolMenuPanelVisible = false;
        GameEngine.player.actions.equipTool("hook");
        GameEngine.stageHandler.toolButton.setAlpha(1);



    }

}
