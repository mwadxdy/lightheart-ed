package com.lighthearted.game.objects.collectables;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.player.Player;
import com.lighthearted.game.objects.animals.Firefly;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.game.physics.Lightning;
import com.lighthearted.utils.Constants;

import box2dLight.PointLight;

public class SunStone extends Sprite {
    private static final String TAG = SunStone.class.getName();

    private LightheartEd main;
    public Body body;
    private PointLight splinterlight;
    private boolean toSpawnFireflies = true;

    public boolean toDestroy = false, destroyed,  toRespawn = false;;
    private float destroyTimer = 0;

    private float colorChangeCooldown = 0;
    private int randR, randG, randB;
    private float radius = 2;

    public float respawnTimer = 0;

    public SunStone(LightheartEd main, float x, float y, Lightning lightning){
        this.main = main;

        setupSprite(x,y);
        createBody(x,y);
        createLight(lightning);
    }
    
    public void update(float delta){
        attachSprite();
        if(toRespawn){
            respawnTimer += Gdx.graphics.getDeltaTime();
            if(respawnTimer > 60){
                createBody(getX(), getY());
                toRespawn = false;
                respawnTimer = 0;
                destroyed = false;
                toDestroy = false;
            }
        }
        if(toDestroy ) {
            destroyTimer += delta;

            if (destroyTimer < 0.4f) {
                destroyed = true;
                lit();
            }else if (destroyTimer < 0.8f){

                if(toSpawnFireflies){
                    for(int i = 0; i < 10; i++) {
                        GameEngine.updater.fireflyList.add(new Firefly(main, getX() + MathUtils.random(-3, 3),
                                getY() + MathUtils.random(-1, 4),
                                getX(),
                                getY(),
                                true));
                    }
                    toSpawnFireflies = false;
                }
                dim();
            }else{

                B2DWorldCreator.world.destroyBody(body);
                splinterlight.remove(true);
                toDestroy = false;
            }
        }
    }

    public void attachSprite(){
        setPosition(body.getPosition().x - getWidth() / 2,
                (body.getPosition().y - getHeight() / 2 + 1));

    }

    public void setupSprite(float x, float y){
        setRegion(main.assets.gameObjectAtlas.findRegion("SunStone0"));
        setBounds(x, y, 4.5f, 4.5f);
        setOrigin(getWidth()/2, getHeight()/2);
    }

    public void createBody(float x, float y){
        body = B2DBodyCreator.createCircle(x,y,
                2,
                BodyDef.BodyType.StaticBody,
                0, true, 0,
                Constants.CATEGORY_SCENERY, Constants.MASK_SCENERY,
                this);

    }

    public void createLight(Lightning lightning){
        splinterlight = new PointLight(lightning.rayHandler, 8,
                new Color(250f / 255f, 120f / 255f, 42f / 255f, 1),
                radius,
                getX() - 0.1f, getY() + 1);

        splinterlight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.MASK_LIGHT);

        splinterlight.setStaticLight(true);
    }

    @Override
    public void draw(Batch batch) {
        if(!destroyed) {
            super.draw(batch);
        }
    }



    public void lit() {
        splinterlight.setDistance(splinterlight.getDistance() * 1.05f);

    }

    public void dim() {
        splinterlight.setDistance(splinterlight.getDistance() * 0.90f);

    }

    public void collect(){
        toDestroy = true;

    }
}
