package com.lighthearted.game.objects.collectables;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class HealthPotion extends Sprite {
    private static final String TAG = HealthPotion.class.getName();

    private LightheartEd main;

    private boolean toDestroy;
    private boolean destroyed = false, toRespawn = false;
    private Body body;

    public float runTime;

    private float x, y;

    public float respawnTimer = 0;

    public HealthPotion(LightheartEd main){
        this.main = main;

    }

    public HealthPotion(LightheartEd main, float x, float y){
        this.x = x;
        this.y = y;
        this.main = main;
        setRegion(main.assets.gameObjectAtlas.findRegion("HealthPotion0"));
        setBounds(x, y, 10f, 10f);
        setOrigin(getWidth()/2, getHeight()/2);
        spawnPotion(x, y);
    }

    public void update(float delta){
        setPosition(x - getWidth()/2, y - getHeight()/2);
        setRegion(getFrame(Gdx.graphics.getDeltaTime()));
        if(toDestroy && !destroyed){
            B2DWorldCreator.world.destroyBody(body);
            //GameScreen.b2DWorldCreator.potionList.removeIndex(GameScreen.b2DWorldCreator.potionList.size - 1);
            destroyed = true;
        }
        if(toRespawn){
            respawnTimer += Gdx.graphics.getDeltaTime();
            if(respawnTimer > 60){
                spawnPotion(x, y);
                toRespawn = false;
                respawnTimer = 0;
                destroyed = false;
                toDestroy = false;
            }
        }
    }



    public void draw(Batch batch) {
        if (!destroyed) {
            super.draw(batch);
        }
    }

    public void destroy(){
        toDestroy = true;
        toRespawn = true;
    }

    public void spawnPotion(float x, float y) {
        body = B2DBodyCreator.createCircle(x,y,
                6, BodyDef.BodyType.StaticBody,
                0, true, 0,
                Constants.CATEGORY_SCENERY,
                Constants.MASK_SCENERY,
                this);

    }



    public void collect() {
        GameEngine.player.health = 100;
        destroy();
    }


    // ANIMATION

    public TextureRegion getFrame(float delta){

        TextureRegion region;
        runTime += delta;
        region = main.assets.healthPotionAnim.getKeyFrame(runTime);
        return region;
    }
}
