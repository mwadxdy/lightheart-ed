package com.lighthearted.game.objects.animals;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.utils.Constants;

public class Bear extends Sprite {
    private static final String TAG = Bear.class.getName();

    private LightheartEd main;

    public Body body;

    public boolean lookRight;

    private float runTime = 0;
    private float walkTime = 0;
    private float rushTime = 0;
    private float attackTime = 0;

    private float movingSpeed = 4f;
    public float maxSpeed = Constants.BEAR_DEFAULT_MAXSPEED;

    public float darknessy = 100;
    public boolean isHealed = false;

    public boolean attacking,
            toHeal,
            playerContact;

    private Sprite heart;
    private float heartTime = 0;
    private float heartOffsetY = 4;
    public Vector2 spawnCoordinates;
    private boolean isUpdated = false;


    public enum State {
        STANDING, WALKING, ATTACKING
    }

    public Bear(LightheartEd main, float x, float y, boolean healed, int mapX, int mapY) {
        this.main = main;
        spawnCoordinates = new Vector2(mapX, mapY);

        isHealed = healed;
        if(isHealed){
            darknessy = 0;
            heartTime = 3;
        }
        heart = new Sprite();
        heart.setRegion(main.assets.gameObjectAtlas.findRegion("Heart0"));
        heart.setBounds(x, y, 10, 10);

        createBody(x, y);
        setSpriteAttributes(x, y);
    }

    public void createBody(float x, float y){

        body = B2DBodyCreator.createBox(x, y,
                Constants.BEAR_HITBOX_WIDTH / Constants.PPM, Constants.BEAR_HITBOX_HEIGHT / Constants.PPM,
                BodyDef.BodyType.DynamicBody,
                1, false, 0, 0,
                Constants.CATEGORY_MONSTER,
                Constants.MASK_MONSTER,
                this);
    }


    public void setSpriteAttributes(float x, float y){
        setPosition(x,y);
        setBounds(x, y, Constants.BEAR_SPRITE_WIDTH, Constants.BEAR_SPRITE_HEIGHT);
        setOrigin(getWidth()/2, getHeight()/2);

    }

    public void update(float delta){
        float deltaX = Math.abs(GameEngine.player.body.getPosition().x - body.getPosition().x);
        float deltaY = Math.abs(GameEngine.player.body.getPosition().y - body.getPosition().y);

        if(darknessy < 1 && !isHealed){
            toHeal = true;

        }
        if(toHeal){
            if(!isUpdated){
                isUpdated = true;
                GameEngine.physics.b2DWorldCreator.wildlifeCleansed++;
            }
            isHealed = true;
            toHeal = false;
            attacking = false;
            GameEngine.physics.b2DWorldCreator.healedBears += (int) spawnCoordinates.x + "," + (int) spawnCoordinates.y + ";";
        }

        if(deltaX > 20){
            attacking = false;
            walk(delta);
        }else{
            if(!isHealed && deltaY < 5) {
                if(!playerContact) {
                    rush(delta);
                }else {
                    if(GameEngine.player.isAlive) {
                        attack(delta);
                    }

                }
            }else {
                walk(delta);
            }
        }



        attachSprite();
    }

    public void attack(float delta) {
        attacking = true;
        attackTime += delta;

        if(attackTime > 0.2f){
            GameEngine.player.health -= 30;
            if(lookRight) {
                GameEngine.player.body.applyLinearImpulse(new Vector2(30, 50), GameEngine.player.body.getLocalCenter(), true);
            }else{
                GameEngine.player.body.applyLinearImpulse(new Vector2(-30, 50), GameEngine.player.body.getLocalCenter(), true);
            }
            GameEngine.player.movement.jumpCounter++;

            attackTime = 0;
            attacking = false;
        }
    }


    public void walk(float delta){
        walkTime += delta;
        if(walkTime < 3){
            moveRight();
        }else if(walkTime < 6){
            moveLeft();
        }else {
            walkTime = 0;
        }
    }

    public void rush(float delta){
        rushTime += delta;
        if(rushTime < 2) {
            if (GameEngine.player.getX() - 5 < getX()) {
                if (body.getLinearVelocity().x > -maxSpeed * 1.5f) {
                    body.applyLinearImpulse(new Vector2(-movingSpeed * 2, 0), body.getLocalCenter(), true);
                }
                lookRight = false;
            } else {
                if (body.getLinearVelocity().x < maxSpeed * 1.5f) {
                    body.applyLinearImpulse(new Vector2(movingSpeed * 2, 0), body.getLocalCenter(), true);
                }
                lookRight = true;
            }
        }else if(rushTime > 3){
            rushTime = 0;
        }
    }

    public void moveLeft(){
        if(lookRight ) {
            lookRight = false;
        }
        if (body.getLinearVelocity().x > -maxSpeed) {
            body.applyLinearImpulse(new Vector2(-movingSpeed, 0), body.getLocalCenter(), true);
        }
    }

    public void moveRight(){
        if(!lookRight ) {
            lookRight = true;
        }

        if (body.getLinearVelocity().x < maxSpeed) {
            body.applyLinearImpulse(new Vector2(movingSpeed, 0), body.getLocalCenter(), true);
        }
    }

    public void attachSprite(){
        setPosition(body.getPosition().x - getWidth() / 2,
                (body.getPosition().y - getHeight() / 2 + 1));

    }

    public TextureRegion getFrame(float delta){
        TextureRegion region = new TextureRegion();
        runTime += delta;
        //Gdx.app.log(TAG, "state                " + GameEngine.player.getState() );

        switch (getState()){
            case STANDING:
                region = lookRight ? main.assets.bearIdleRightAnim.getKeyFrame(runTime) : main.assets.bearIdleLeftAnim.getKeyFrame(runTime);
                break;
            case WALKING:
                region = lookRight ? main.assets.bearWalkRightAnim.getKeyFrame(runTime) : main.assets.bearWalkLeftAnim.getKeyFrame(runTime);
                break;
            case ATTACKING:
                region = lookRight ? main.assets.bearAttackRightAnim.getKeyFrame(runTime) : main.assets.bearAttackLeftAnim.getKeyFrame(runTime);
                break;
        }


        return region;
    }
    public TextureRegion getDarkFrame(float delta){
        TextureRegion region = new TextureRegion();
        //Gdx.app.log(TAG, "state                " + GameEngine.player.getState() );

        switch (getState()){
            case STANDING:
                region = lookRight ? main.assets.bearIdleDarkRightAnim.getKeyFrame(runTime) : main.assets.bearIdleDarkLeftAnim.getKeyFrame(runTime);
                break;
            case WALKING:
                region = lookRight ? main.assets.bearWalkDarkRightAnim.getKeyFrame(runTime) : main.assets.bearWalkDarkLeftAnim.getKeyFrame(runTime);
                break;
            case ATTACKING:
                region = lookRight ? main.assets.bearAttackDarkRightAnim.getKeyFrame(runTime) : main.assets.bearAttackDarkLeftAnim.getKeyFrame(runTime);
                break;
        }


        return region;
    }

    public State getState() {

        if (body.getLinearVelocity().y != 0 && !GameEngine.physics.b2DContactListener.bearRampContact) {
            return State.STANDING;
        } else if (body.getLinearVelocity().x != 0) {
            return State.WALKING;
        } else  {
            if(attacking){
                return State.ATTACKING;
            }else {
                return State.STANDING;

            }
        }

    }

    @Override
    public void draw(Batch batch, float delta) {
        setRegion(getFrame(delta));
        setAlpha(1);
        super.draw(batch);
        setRegion(getDarkFrame(delta));
        if(darknessy > 0) {
            setAlpha(darknessy / 100);
        }else {
            setAlpha(0);
        }
        if(isHealed){
            heartTime += delta;
            if(heartTime < 3){
                heartOffsetY += 0.2f;
                heart.setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight()/2 + heartOffsetY);
                heart.draw(batch);
            }else {

            }
        }
        super.draw(batch);


    }

    public void heal(){
        if(darknessy > 0){
            darknessy -= 10;
        }else {
            isHealed = true;

        }
    }
}
