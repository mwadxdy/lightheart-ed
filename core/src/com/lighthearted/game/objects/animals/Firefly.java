package com.lighthearted.game.objects.animals;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

import box2dLight.PointLight;

public class Firefly {
    private static final String TAG = Firefly.class.getName();

    private LightheartEd main;

    public Body body;

    private PointLight fireflyLight;

    private float lifeTime;
    private float directionX,directionY;

    public float movingSpeed;

    public boolean isAlive = false;
    public boolean targetFound = false;
    public boolean freeFly = false;

    public boolean groundContact;
    public float deltaYPlayer;
    public boolean toRemove;
    private float randR, randG, randB;

    private boolean toSearchPlayer;

    public Firefly(LightheartEd main){
        this.main = main;

    }

    public Firefly(LightheartEd main, float x, float y, float directionX, float directionY, boolean searchPlayer) {
        this.main = main;
        this.directionX = directionX;
        this.directionY = directionY;
        this.toSearchPlayer = searchPlayer;
        movingSpeed = Constants.FIREFLY_SPEED;
        float min = -5;
        float max = 5;//
        float randomX = (float) (min + Math.random() * (max - min));
        float randomY = (float) (min + Math.random() * (max - min));
        createBody(x, y);


    }
    
    public void update(){
        lifeTime += Gdx.graphics.getDeltaTime();
        deltaYPlayer = Math.abs(body.getPosition().y - GameEngine.player.body.getPosition().y);

        if(!targetFound ){
            if(toSearchPlayer){
                if(lifeTime > 0.5f){
                    searchPlayer();
                }else{
                    freeFly();
                }

            }else{
                searchTarget();
            }
        }else if(lifeTime < 10){
            freeFly();
        }else {
            toRemove = true;
        }

        if(toRemove){

            B2DWorldCreator.world.destroyBody(body);
            fireflyLight.remove();
            isAlive = false;
        }

    }

    public void createBody(float x, float y){
        body = B2DBodyCreator.createCircle(x,y,
                0.2f,
                BodyDef.BodyType.DynamicBody,
                0, true, 0.0001f,
                Constants.CATEGORY_PLAYER, Constants.MASK_PLAYER,
                this);

        body.setFixedRotation(true);
        body.setLinearDamping(400);

        randR = MathUtils.random(0,255);
        randG = MathUtils.random(0,255);
        randB = MathUtils.random(0,255);


        fireflyLight = new PointLight(GameEngine.physics.lightning.rayHandler, 8,
                new Color(randR/255f,randG/255f,randB/255f, 1), 1f, x, y);
        fireflyLight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.CATEGORY_LIGHT);
        fireflyLight.attachToBody(body, -0.08f, 0.05f);

        isAlive = true;
    }

    public void moveRight(){
        body.applyLinearImpulse(new Vector2(movingSpeed, 0), body.getLocalCenter(), true);
    }

    public void moveLeft(){
        body.applyLinearImpulse(new Vector2(-movingSpeed, 0), body.getLocalCenter(), true);

    }

    public void moveUp(){
        body.applyLinearImpulse(new Vector2(0, movingSpeed), body.getLocalCenter(), true);

    }

    public void boostUp(){
        body.applyLinearImpulse(new Vector2(0, movingSpeed * 10), body.getLocalCenter(), true);

    }

    public void moveDown(){
        body.applyLinearImpulse(new Vector2(0, -movingSpeed), body.getLocalCenter(), true);


    }

    public void searchTarget(){
        float deltax = directionX - body.getPosition().x;
        float deltay = directionY - body.getPosition().y;

        if(Math.abs(deltax) < 0.3 && Math.abs(deltay) < 0.3f){
            targetFound = true;
        }

        int rand = MathUtils.random(0,2);

        if(toSearchPlayer){
            directionX = GameEngine.player.getX() + GameEngine.player.getWidth()/2;
            directionY = GameEngine.player.getY() + GameEngine.player.getHeight()/2;
        }

        switch (rand){
            case 0:
                if (body.getPosition().x < directionX) {
                    moveRight();
                } else {
                    moveLeft();
                }

                break;
            case 1:
                if (body.getPosition().y < directionY) {

                    moveUp();
                } else {

                    moveDown();
                }
                break;
        }



    }

    public void searchPlayer(){

        directionX = GameEngine.player.getX() + GameEngine.player.getWidth()/2;
        directionY = GameEngine.player.getY() + GameEngine.player.getHeight()/2;

        movingSpeed = movingSpeed * 1.5f;

        float deltax = directionX - body.getPosition().x;
        float deltay = directionY - body.getPosition().y;



        if (body.getPosition().x < directionX) {
            moveRight();
        } else {
            moveLeft();
        }

        if (body.getPosition().y < directionY) {

            moveUp();
        } else {

            moveDown();
        }

        if(Math.abs(deltax) < 0.01 && Math.abs(deltay) < 0.01f){
            targetFound = true;
        }





    }


    public void freeFly(){
        freeFly = true;

        body.setLinearDamping(200);
        int rand = MathUtils.random(0, 3);
        switch (rand){
            case 0:
                moveUp();
                break;
            case 1:
                moveRight();
                break;
            case 2:
                moveLeft();
                break;
            case 3:
                moveDown();
                break;
        }
    }



}
