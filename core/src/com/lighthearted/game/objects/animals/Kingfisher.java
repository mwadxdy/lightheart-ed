package com.lighthearted.game.objects.animals;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class Kingfisher extends Sprite {
    private static final String TAG = Kingfisher.class.getName();

    private LightheartEd main;

    private Body body;
    public boolean toDestroy, destroyed, toSpawn;
    public boolean lookRight, follow;
    private float runTime, followTime;
    private float flyTime = 0;
    private int movespeed = 30;


    public Kingfisher(LightheartEd main, float x, float y, boolean idle) {
        this.main = main;
        setBounds(x , y , 3f, 3f);
        setOrigin(getWidth()/2 , getHeight()/2);
        createBody(x, y, idle);
        if(!idle){
            follow = true;
        }


    }
    private int speakvar = 5;
    
    public void update(float delta){

        if(toDestroy && !destroyed){
            B2DWorldCreator.world.destroyBody(body);
            destroyed = true;
        }
        if(toSpawn){
            if(GameEngine.player.isLookingRight) {
                createBody(GameEngine.player.body.getPosition().x - 3, GameEngine.player.body.getPosition().y + 8, false);
            }else {
                createBody(GameEngine.player.body.getPosition().x + 3, GameEngine.player.body.getPosition().y + 8, false);
            }
            follow = true;
        }


        if (follow){
            if(body.getLinearVelocity().x < 0){
                lookRight = false;
            }else {
                lookRight = true;
            }
            fly();
            follow();
        }else {
            if(GameEngine.player.getX() < getX()){
                lookRight = false;
            }else {
                lookRight = true;
            }
        }
        setPosition(body.getPosition().x - getWidth()/2, body.getPosition().y - getHeight()/2);
        setRegion(getFrame(Gdx.graphics.getDeltaTime()));
    }

    private void createBody(float x, float y, boolean idle) {
        toDestroy = false;
        destroyed = false;
        toSpawn = false;

        if(idle){
            body = B2DBodyCreator.createCircle(x, y,
                    20 / Constants.PPM,
                    BodyDef.BodyType.StaticBody,
                    0, true, 0,
                    Constants.CATEGORY_PLAYER, Constants.MASK_PLAYER,
                    this);
        }else{
            body = B2DBodyCreator.createCircle(x, y,
                    20 / Constants.PPM,
                    BodyDef.BodyType.DynamicBody,
                    0, false, 0,
                    Constants.CATEGORY_PLAYER, Constants.MASK_PLAYER,
                    this);
            GameEngine.player.hasPet = true;
        }



    }

    public void fly(){
        flyTime += Gdx.graphics.getDeltaTime();

        float deltaY = GameEngine.player.body.getPosition().y - body.getPosition().y;
        float deltaX = GameEngine.player.body.getPosition().x - body.getPosition().x;

        if(body.getLinearVelocity().y < -10){
            body.setLinearVelocity(body.getLinearVelocity().x, 0);
        }
        //Gdx.app.log(TAG, "deltaY : " + deltaY);
        //Gdx.app.log(TAG, "deltaX : " + deltaX);

        if(deltaY > - 10 && deltaY < 0) {
            //Gdx.app.log(TAG, "deltaY > -10");

            if (flyTime > 0.1f) {
                body.applyLinearImpulse(new Vector2(0, 15), body.getLocalCenter(), true);
                flyTime = 0;
            }
        }else if(deltaY < -40 || deltaX > 30 || deltaY > 40 || deltaX < -30) {
            //Gdx.app.log(TAG, "deltaY > -10");

            destroy();
            toSpawn = true;
        }else if(deltaY < - 20) {
            body.applyLinearImpulse(new Vector2(0, - 30 ), body.getLocalCenter(), true);

        }else if(deltaY > 20){
            body.applyLinearImpulse(new Vector2(0,  30 ), body.getLocalCenter(), true);

        }
    }

    private void follow(){
        //body.setLinearVelocity(0, 0);

        float deltaX = Math.abs(GameEngine.player.body.getPosition().x - body.getPosition().x);
        //Gdx.app.log(TAG, "deltaX: " + deltaX);
        //Gdx.app.log(TAG, "deltaY: " + deltaY);
        //Gdx.app.log(TAG, "velx: " + body.getLinearVelocity().x);
        //Gdx.app.log(TAG, "vely: " + body.getLinearVelocity().y);

        if(deltaX > 12) {
            //body.setTransform(GameEngine.player.getX() - 0.5f, GameEngine.player.getY() + 4f, 0);
            if(GameEngine.player.body.getPosition().x < body.getPosition().x){
                body.applyLinearImpulse(new Vector2(-2, 0), body.getLocalCenter(), true);
                if(body.getLinearVelocity().x < -movespeed){
                    body.setLinearVelocity(-movespeed, body.getLinearVelocity().y);
                }

            }else {
                body.applyLinearImpulse(new Vector2(2, 0), body.getLocalCenter(), true);
                if(body.getLinearVelocity().x > movespeed){
                    body.setLinearVelocity(movespeed, body.getLinearVelocity().y);
                }
            }
        }else{
            body.setLinearVelocity(body.getLinearVelocity().x /2, body.getLinearVelocity().y);
        }
    }


    public void draw(Batch batch){

        if(!destroyed) {

            super.draw(batch);

        }
    }

    private TextureRegion getFrame(float delta){
        TextureRegion region;
        runTime += delta;
        if(lookRight){
            region = main.assets.kingfisherAnimRight.getKeyFrame(runTime);
        }else {
            region = main.assets.kingfisherAnimLeft.getKeyFrame(runTime);
        }
        return region;
    }


    public void destroy(){
        toDestroy = true;

    }


}
