package com.lighthearted.game.hud;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.screens.MenuScreen;
import com.lighthearted.utils.Constants;

public class StageHandler {
    private static final String TAG = StageHandler.class.getName();

    private LightheartEd main;


    public Sprite toolButton;
    public Sprite toolMenuButton;


    public Sprite toolMenuPanel;
    public Sprite toolMenuPanelTopRight;
    public Sprite toolMenuPanelTopLeft;
    public Sprite toolMenuPanelBottomLeft;
    public Sprite toolMenuPanelBottomRight;
    public Sprite quicktool1;
    public Sprite quicktool2;
    public Sprite quicktool3;



    public String topLeftTool = "";
    public String topRightTool = "";
    public String quicktool1_string = "";
    public String quicktool2_string = "";
    public String quicktool3_string = "";

    public Sprite quicktoolMenupanel;
    public boolean isToolMenuPanelVisible;
    public boolean isQuicktoolMenupanelVisible;



    public Stage stage;

    public Sprite heartSymbol;
    public Sprite heartHardSymbol;
    public Sprite energyBarFrame;
    public Sprite energyBar;

    private float colorChangeCooldown = 0;
    private int randR, randG, randB;


    public StageHandler(LightheartEd main){
        this.main = main;
        setupStage();
    }
    
    public void update(float delta){
        updateEnergyBar(delta);
        //updateToolMenuPanel(delta);
        updateQuicktoolMenupanel(delta);
        /*updateToolMenuTopLeft(delta);
        updateToolMenuTopRight(delta);*/

        updateQuicktool1(delta);
        updateQuicktool2(delta);
    }

    public void updateToolMenuTopLeft(float delta){
        toolMenuPanelTopLeft.setBounds(GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getHeight());

    }

    public void updateQuicktool1(float delta){
        quicktool1.setBounds(GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getX() - 25,
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getY() - 20,
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getWidth() * 2,
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getHeight() * 2);

    }


    public void updateQuicktool2(float delta){
        quicktool2.setBounds(GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getX() - 37,
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getY() + 5,
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getWidth() * 2,
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getHeight() * 2);

    }

    public void updateToolMenuTopRight(float delta){
        toolMenuPanelTopRight.setBounds(GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getHeight());

    }

    public void updateToolMenuPanel(float delta){
        toolMenuPanel.setBounds(stage.getWidth() * 0.32f , stage.getHeight() * 0.37f,
                stage.getWidth() * 0.35f, stage.getHeight() * 0.50f);
    }

    public void updateQuicktoolMenupanel(float delta){

        quicktoolMenupanel.setBounds(stage.getWidth() * 0.48f, stage.getHeight() * 0.09f,
                stage.getWidth() * 0.15f, stage.getHeight() * 0.13f);




    }

    public void setupStage(){
        stage = new Stage(new StretchViewport(Constants.UI_VIEWPORT_WIDTH, Constants.UI_VIEWPORT_HEIGHT));
        stage.clear();

        Table rightButtonTable = createRightButtonTable();
        Table leftButtonTable = createLeftButtonTable();
        Table jumpButtonTable = createJumpButtonTable();
        Table restartButtonTable = createRestartButtonTable();
        Table musicButtonTable = createMusicButtonTable();
        Table menuButtonTable = createMenuButtonTable();


        Stack stack = new Stack();
        stack.setSize(Constants.UI_VIEWPORT_WIDTH, Constants.UI_VIEWPORT_HEIGHT);
        stack.add(rightButtonTable);
        stack.add(leftButtonTable);
        stack.add(jumpButtonTable);
        stack.add(restartButtonTable);
        stack.add(musicButtonTable);
        stack.add(menuButtonTable);

        stage.addActor(stack);

        toolButton = new Sprite();
        loadToolButton(GameEngine.physics.b2DWorldCreator.toolInUse);
        loadToolMenuButton();
        loadHeartSymbol();
        loadHeartHardSymbol();
        loadEnergyBar();
        //loadToolMenuPanel(GameEngine.physics.b2DWorldCreator.tools);
        loadQuicktoolMenupanel(GameEngine.physics.b2DWorldCreator.tools);


    }

    private Table createLeftButtonTable() {
        Table leftButtonsLayer = new Table();
        leftButtonsLayer.bottom().left();

        Button leftButton = new Button(main.assets.skinGameUI, "left");

        leftButtonsLayer.add(leftButton).width(stage.getWidth() / 5).height(stage.getHeight() / 5).padBottom(10);

        return leftButtonsLayer;
    }

    private Table createRightButtonTable() {
        Table rightButtonsLayer = new Table();
        rightButtonsLayer.bottom();

        Button rightButton = new Button(main.assets.skinGameUI, "right");

        rightButtonsLayer.add(rightButton).width(stage.getWidth() / 5).height(stage.getHeight() / 5).padRight(350).padBottom(10);

        return rightButtonsLayer;
    }

    private Table createJumpButtonTable() {
        Table jumpButtonsLayer = new Table();
        jumpButtonsLayer.center();

        //jumpButtonsLayer.setDebug(true);

        Button jumpButton = new Button(main.assets.skinGameUI, "jump");

        jumpButtonsLayer.add(jumpButton).width(stage.getWidth()/3).height(stage.getHeight() / 4).padTop(480).padLeft(550);

        return jumpButtonsLayer;
    }


    //MUSIC
    private Table createMusicButtonTable() {
        Table musicButtonsLayer = new Table();
        musicButtonsLayer.center().padRight(700).padBottom(400);
        if(main.assets.isSoundActivated){
            final CheckBox musicOffButton = new CheckBox("", main.assets.skinGameUI, "musicOff");
            musicOffButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    Gdx.app.log(TAG, "music button pressed");

                    GameEngine.soundHandler.musicToggle();
                }
            });
            musicButtonsLayer.add(musicOffButton).width(stage.getWidth()/5).height(stage.getHeight()/4);


        }else{

            final CheckBox musicOnButton = new CheckBox("", main.assets.skinGameUI, "musicOn");
            musicOnButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    Gdx.app.log(TAG, "music button pressed");

                    GameEngine.soundHandler.musicToggle();
                }
            });
            musicButtonsLayer.add(musicOnButton).width(stage.getWidth()/5).height(stage.getHeight()/4);

        }




        return musicButtonsLayer;
    }

    //MENU
    private Table createMenuButtonTable() {
        Table menuButtonsLayer = new Table();
        menuButtonsLayer.top().padRight(200).padBottom(50);

        Button menuButton = new Button(main.assets.skinGameUI, "menu");

        menuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(main.assets.isSoundActivated){
                    GameEngine.soundHandler.musicToggle();
                }
                main.setScreen(new MenuScreen(main));
            }
        });

        menuButtonsLayer.add(menuButton).width(stage.getWidth()/7).height(stage.getHeight()/7);


        return menuButtonsLayer;
    }

    //RESTART
    private Table createRestartButtonTable() {
        Table restartButtonsLayer = new Table();
        restartButtonsLayer.center().padBottom(500).padRight(450);

        Button restartButton = new Button(main.assets.skinGameUI, "restart");

        restartButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GameEngine.updater.restart();
            }
        });

        restartButtonsLayer.add(restartButton).width(stage.getWidth()/11).height(stage.getHeight()/11);


        return restartButtonsLayer;
    }

    //TOOL BUTTON

    public void loadToolButton(String currentTool) {
        if(currentTool.equals("")){
            toolButton = new Sprite(main.assets.skinGameUI.getRegion("toolButton"));
            if(GameEngine.physics.b2DWorldCreator.tools.equals("")){
                toolButton.setAlpha(0);

            }
        }else if(currentTool.equals("hook")){
            toolButton.setRegion(main.assets.skinGameUI.getRegion("hookButton"));

        }else if(currentTool.equals("pogo")){
            toolButton.setRegion(main.assets.skinGameUI.getRegion("pogoButton"));

        }

        toolButton.setBounds(stage.getWidth() * 0.82f, stage.getHeight() * 0.25f,
                stage.getWidth() * 0.15f, stage.getHeight() * 0.13f);

    }

    //TOOLMENU
    public void loadToolMenuButton() {
        toolMenuButton = new Sprite(main.assets.skinGameUI.getRegion("toolMenuButton"));

        toolMenuButton.setBounds(stage.getWidth() * 0.60f, stage.getHeight() * 0.10f,
                stage.getWidth() * 0.07f, stage.getHeight() * 0.05f);


    }

    //QUICKTOOL MENU

    public void loadQuicktoolMenupanel(String toolsAvailable) {
        quicktoolMenupanel = new Sprite(main.assets.skinGameUI.getRegion("quicktoolMenupanel"));
        quicktoolMenupanel.setAlpha(0.6f);
        quicktoolMenupanel.scale(1.1f);
        quicktoolMenupanel.setBounds(stage.getWidth() * 0.48f, stage.getHeight() * 0.09f,
                stage.getWidth() * 0.15f, stage.getHeight() * 0.13f);

        String[] tools = toolsAvailable.split(";");

        loadQuicktool1(tools[0]);
        if(tools.length > 1){
            loadQuicktool2(tools[1]);
        }else{
            loadQuicktool2("");

        }

    }

    public void quickMenuPanelSwitch(){
        if (isQuicktoolMenupanelVisible) {
            isQuicktoolMenupanelVisible = false;
        } else {
            isQuicktoolMenupanelVisible = true;
        }
    }

    public void loadQuicktool1(String tool){
        if(tool.equals("")){
            quicktool1 = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
            quicktool1.setAlpha(0);
        }else if(tool.equals("hook")) {
            quicktool1 = new Sprite(main.assets.gameObjectAtlas.findRegion("Hook0"));
            quicktool1_string = tool;
        }else if(tool.equals("pogo")) {
            quicktool1 = new Sprite(main.assets.gameObjectAtlas.findRegion("PogoStick0"));
            quicktool1_string = tool;
        }

        quicktool1.setBounds(0,0,0,0);

    }

    public void loadQuicktool2(String tool){
        if(tool.equals("")){
            quicktool2 = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
            quicktool2.setAlpha(0);
        }else if(tool.equals("hook")) {
            quicktool2 = new Sprite(main.assets.gameObjectAtlas.findRegion("Hook0"));
            quicktool2_string = tool;
        }else if(tool.equals("pogo")) {
            quicktool2 = new Sprite(main.assets.gameObjectAtlas.findRegion("PogoStick0"));
            quicktool2_string = tool;
        }

        quicktool2.setBounds(0,0,0,0);

    }

    public void loadQuicktool3(String tool){
        if(tool.equals("")){
            quicktool3 = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
            quicktool3.setAlpha(0);
        }else if(tool.equals("hook")) {
            quicktool3 = new Sprite(main.assets.gameObjectAtlas.findRegion("Hook0"));
            quicktool3_string = tool;
        }else if(tool.equals("pogo")) {
            quicktool3 = new Sprite(main.assets.gameObjectAtlas.findRegion("PogoStick0"));
            quicktool3_string = tool;
        }

        quicktool3.setBounds(0,0,0,0);

    }

    //HEART SYMBOL

    public void loadHeartSymbol(){
        heartSymbol = new Sprite(main.assets.skinGameUI.getRegion("heart"));
        heartSymbol.scale(5f);

        heartSymbol.setPosition(80, stage.getHeight() * 0.87f);
    }

    public void loadHeartHardSymbol(){
        heartHardSymbol = new Sprite(main.assets.skinGameUI.getRegion("heartHard"));
        heartHardSymbol.scale(1f);

        heartHardSymbol.setPosition(80, stage.getHeight() * 0.87f);
    }

    //ENERGYBAR

    public void loadEnergyBar(){
        energyBarFrame = new Sprite(main.assets.skinGameUI.getRegion("lightbarframe"));
        energyBarFrame.setBounds(stage.getWidth() * 0.35f,
                stage.getHeight() * 0.01f,
                stage.getWidth() * 0.3f,
                stage.getHeight() * 0.05f );

        energyBar = new Sprite(main.assets.skinGameUI.getRegion("lightbar"));
        energyBar.setColor(Color.YELLOW);
        energyBar.setAlpha(0.6f);
        energyBar.setBounds(energyBarFrame.getX() + 11,
                energyBarFrame.getY() + 9,
                15,
                energyBarFrame.getHeight() * 0.7f);

        energyBar.setOrigin(energyBar.getWidth()/2, energyBar.getHeight()/2);

    }

    public void updateEnergyBar(float delta){
        energyBar.setBounds(energyBarFrame.getX() + 11 - GameEngine.player.energy * 0.12f,
                energyBarFrame.getY() + 3.5f,
                GameEngine.player.energy,
                energyBarFrame.getHeight() * 0.75f);

        colorChangeCooldown += delta;
        if(colorChangeCooldown > 0.2f) {
            randR = MathUtils.random(100,255);
            randG = MathUtils.random(50,150);
            randB = MathUtils.random(0,50);

            energyBar.setColor(randR, randG, randB, 0.4f);
            colorChangeCooldown = 0;
        }
    }




    public void loadToolMenuPanel(String toolsAvailable) {
        toolMenuPanel = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
        toolMenuPanel.setAlpha(0.6f);
        toolMenuPanel.setBounds(stage.getWidth() * 0.82f, stage.getHeight() * 0.25f,
                stage.getWidth() * 0.15f, stage.getHeight() * 0.13f);

        String[] tools = toolsAvailable.split(";");

        loadToolMenuTopLeft(tools[0]);
        if(tools.length > 1){
            loadToolMenuTopRight(tools[1]);
        }else{
            loadToolMenuTopRight("");

        }

    }

    public void loadToolMenuTopLeft(String tool){
        if(tool.equals("")){
            toolMenuPanelTopLeft = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
            toolMenuPanelTopLeft.setAlpha(0);
        }else if(tool.equals("hook")) {
            toolMenuPanelTopLeft = new Sprite(main.assets.gameObjectAtlas.findRegion("Hook0"));
            topLeftTool = tool;
        }else if(tool.equals("pogo")) {
            toolMenuPanelTopLeft = new Sprite(main.assets.gameObjectAtlas.findRegion("PogoStick0"));
            topLeftTool = tool;
        }

        toolMenuPanelTopLeft.setBounds(0,0,0,0);

    }

    public void loadToolMenuTopRight(String tool){
        if(tool.equals("")){
            toolMenuPanelTopRight = new Sprite(main.assets.skinGameUI.getRegion("toolMenuPanel"));
            toolMenuPanelTopRight.setAlpha(0);
        }else if(tool.equals("hook")) {
            toolMenuPanelTopRight = new Sprite(main.assets.gameObjectAtlas.findRegion("Hook0"));
            topRightTool = tool;
        }else if(tool.equals("pogo")) {
            toolMenuPanelTopRight = new Sprite(main.assets.gameObjectAtlas.findRegion("PogoStick0"));
            topRightTool = tool;
        }

        toolMenuPanelTopRight.setBounds(0,0,0,0);

    }


}
