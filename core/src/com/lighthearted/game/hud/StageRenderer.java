package com.lighthearted.game.hud;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public class StageRenderer   {
    private static final String TAG = StageRenderer.class.getName();

    private LightheartEd main;
    private float youdiedTimer = 0;
    private ShapeRenderer shapeRenderer;


    public StageRenderer(LightheartEd main){
        this.main = main;
        shapeRenderer = new ShapeRenderer();
    }
    
    public void render(float delta){


        if(!GameEngine.player.isAlive) { //if player is dead
            youdiedTimer += delta;
            if (youdiedTimer < 3) {
                renderYouDied();
            }else {
                GameEngine.updater.restart();
            }
        }else{
            renderHeartSymbol();
            renderObjectives();
            GameEngine.stageHandler.stage.draw();
            renderToolButton();
            rendeToolMenuButton();
            if(GameEngine.stageHandler.isToolMenuPanelVisible){
                renderToolMenuPanel();
            }
            if(GameEngine.inputHandler.touchInput.isQuickToolPanelOpened){
                renderQuicktoolMenupanel();
            }
            renderLightbar();
        }
    }

    public void renderToolButton(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.toolButton.draw(main.batch);
        main.batch.end();


    }

    public void rendeToolMenuButton(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.toolMenuButton.draw(main.batch);
        main.batch.end();


    }

    public void renderToolMenuPanel(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.toolMenuPanel.draw(main.batch);
        main.assets.toolMenuPanelFont.draw(main.batch, "TOOLS" ,
                GameEngine.stageHandler.toolMenuPanel.getX() +  GameEngine.stageHandler.toolMenuPanel.getWidth()/2 - 15,
                GameEngine.stageHandler.toolMenuPanel.getY() +  GameEngine.stageHandler.toolMenuPanel.getHeight() - 20);
        GameEngine.stageHandler.toolMenuPanelTopLeft.draw(main.batch);
        GameEngine.stageHandler.toolMenuPanelTopRight.draw(main.batch);
        main.batch.end();


    }

    public void renderQuicktoolMenupanel(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.quicktoolMenupanel.draw(main.batch);
        GameEngine.stageHandler.quicktool1.draw(main.batch);
        GameEngine.stageHandler.quicktool2.draw(main.batch);
        //GameEngine.stageHandler.quicktool3.draw(main.batch);
        main.batch.end();


    }

    public void renderHeartSymbol(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.heartSymbol.draw(main.batch, GameEngine.player.health / 100) ;
        if(GameEngine.hardmode){
            GameEngine.stageHandler.heartHardSymbol.draw(main.batch, GameEngine.player.health / 100) ;
        }
        main.batch.end();
    }

    public void renderLightbar(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        GameEngine.stageHandler.energyBarFrame.draw(main.batch) ;
        GameEngine.stageHandler.energyBar.draw(main.batch) ;
        main.batch.end();
    }

    public void renderYouDied(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        main.assets.fontSmall.draw(main.batch, "You can rest now..." , Constants.UI_VIEWPORT_WIDTH * 0.45f, Constants.UI_VIEWPORT_HEIGHT * 0.7f);
        main.batch.end();

    }

    public void renderObjectives(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        if(GameEngine.mage.hasSpoken) {
            main.assets.pressStartFont.draw(main.batch, "Wildtiere geheilt  " + GameEngine.physics.b2DWorldCreator.wildlifeCleansed + " / " + GameEngine.physics.b2DWorldCreator.bearList.size, 20, Constants.UI_VIEWPORT_HEIGHT * 0.70f);
            main.assets.pressStartFont.draw(main.batch, "Fackeln entzündet  " + GameEngine.physics.b2DWorldCreator.torchesLit + " / " + GameEngine.physics.b2DWorldCreator.torchList.size, 20, Constants.UI_VIEWPORT_HEIGHT * 0.67f);
            main.assets.pressStartFont.draw(main.batch, "Sonnenaltare geläutert  " + GameEngine.physics.b2DWorldCreator.altarsCleansed + " / " + GameEngine.physics.b2DWorldCreator.altarList.size, 20, Constants.UI_VIEWPORT_HEIGHT * 0.64f);
        }
        main.batch.end();

    }

    public void debug(){
        main.batch.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        main.batch.begin();
        main.assets.fontSmall.draw(main.batch, "" + main.assets.prefs.getBoolean("started"),
                Constants.UI_VIEWPORT_WIDTH * 0.7f, Constants.UI_VIEWPORT_HEIGHT * 0.9f);
        main.batch.end();

    }

    public void renderRects(){

        main.batch.begin();
        shapeRenderer.setProjectionMatrix(GameEngine.camHandler.UIcam.combined);
        // LEFT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.leftRect.getX(), GameEngine.inputHandler.touchInput.leftRect.getY(),
                GameEngine.inputHandler.touchInput.leftRect.getWidth(), GameEngine.inputHandler.touchInput.leftRect.getHeight());
        shapeRenderer.end();

        // RIGHT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.rightRect.getX(), GameEngine.inputHandler.touchInput.rightRect.getY(),
                GameEngine.inputHandler.touchInput.rightRect.getWidth(), GameEngine.inputHandler.touchInput.rightRect.getHeight());
        shapeRenderer.end();

        // JUMP-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.jumpRect.getX(), GameEngine.inputHandler.touchInput.jumpRect.getY(),
                GameEngine.inputHandler.touchInput.jumpRect.getWidth(), GameEngine.inputHandler.touchInput.jumpRect.getHeight());
        shapeRenderer.end();

        /*
        // TOOL-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolRect.getX(), GameEngine.inputHandler.touchInput.toolRect.getY(),
                GameEngine.inputHandler.touchInput.toolRect.getWidth(), GameEngine.inputHandler.touchInput.toolRect.getHeight());
        shapeRenderer.end();*/

        // TOOLMENU-RECTS

        // TOOLMENU-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolMenuRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuRect.getHeight());
        shapeRenderer.end();

        // TOOLQUICK1-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getX(),
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getY(),
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getWidth(),
                GameEngine.inputHandler.touchInput.toolQuickslot1Rect.getHeight());
        shapeRenderer.end();

        // TOOLQUICK2-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getX(),
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getY(),
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getWidth(),
                GameEngine.inputHandler.touchInput.toolQuickslot2Rect.getHeight());
        shapeRenderer.end();


        // TOOLQUICK3-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolQuickslot3Rect.getX(),
                GameEngine.inputHandler.touchInput.toolQuickslot3Rect.getY(),
                GameEngine.inputHandler.touchInput.toolQuickslot3Rect.getWidth(),
                GameEngine.inputHandler.touchInput.toolQuickslot3Rect.getHeight());
        shapeRenderer.end();



/*
        // TOOLMENUTOPLEFT-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuTopLeftRect.getHeight());
        shapeRenderer.end();

        // TOOLMENUTOPRIGHT-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuTopRightRect.getHeight());
        shapeRenderer.end();

        // TOOLMENUBOTTOMLEFT-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolMenuBottomLeftRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuBottomLeftRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuBottomLeftRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuBottomLeftRect.getHeight());
        shapeRenderer.end();

        // TOOLMENUBOTTOMRIGHT-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.toolMenuBottomRightRect.getX(),
                GameEngine.inputHandler.touchInput.toolMenuBottomRightRect.getY(),
                GameEngine.inputHandler.touchInput.toolMenuBottomRightRect.getWidth(),
                GameEngine.inputHandler.touchInput.toolMenuBottomRightRect.getHeight());
        shapeRenderer.end();*/


        /*// RUN-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.runRect.getX(), GameEngine.inputHandler.touchInput.runRect.getY(),
                GameEngine.inputHandler.touchInput.runRect.getWidth(), GameEngine.inputHandler.touchInput.runRect.getHeight());
        shapeRenderer.end();

        // FLY-RECT
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(GameEngine.inputHandler.touchInput.flyRect.getX(), GameEngine.inputHandler.touchInput.flyRect.getY(),
                GameEngine.inputHandler.touchInput.flyRect.getWidth(), GameEngine.inputHandler.touchInput.flyRect.getHeight());
        shapeRenderer.end();*/

        main.batch.end();

        shapeRenderer.end();
    }
}
