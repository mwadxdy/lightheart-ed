package com.lighthearted.game.cam;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public class CamHandler {
    private static final String TAG = CamHandler.class.getName();

    private LightheartEd main;

    public OrthographicCamera UIcam, gameCam;

    public OrthographicCamera layer0Cam, layer0CopyCam,
            layer1Cam, layer1CopyCam,
            layer2Cam, layer2CopyCam,
            layer3Cam, layer3CopyCam;
    public float cameraMovePosXOffset = 10;
    private float cameraMovePosYOffset = 8;

    private int randomOffset;

    public float defaultZoom;
    public float pogoOffset = 0;


    public CamHandler(LightheartEd main){
        this.main = main;
        initCams();
        randomOffset = MathUtils.random(0,20);
    }

    public void update(float delta){
        cameraTurn();
        attachCamerasToPlayerParallax();

        layer0Cam.update(); //SKY0
        layer0CopyCam.update(); //SKY1
        layer1Cam.update(); //MOUNTAINS
        layer1CopyCam.update(); //MOUNTAINS
        layer2Cam.update(); //HILLS
        layer2CopyCam.update(); //HILLS
        layer3Cam.update(); //PLATEAU
        layer3CopyCam.update(); //PLATEAU
        gameCam.update();
    }

    public void cameraTurn(){
        if(GameEngine.player.isLookingRight){
            if(cameraMovePosXOffset < 20) {
                cameraMovePosXOffset += 0.1f;
            }
        }else {
            if(cameraMovePosXOffset > -10) {
                cameraMovePosXOffset -= 0.1f;
            }
        }

        if(GameEngine.player.body.getLinearVelocity().y < -55){
            if(cameraMovePosYOffset > 0){
                cameraMovePosYOffset -= 0.1f;
            }
        }else{
            if(cameraMovePosYOffset < 8){
                cameraMovePosYOffset += 0.5f;
            }
        }


    }



    public void attachCamerasToPlayerParallax(){


        gameCam.position.set(GameEngine.player.getX() + cameraMovePosXOffset, GameEngine.player.getY() + cameraMovePosYOffset, 0);


        //SKY
        layer0Cam.position.set(GameEngine.player.getX() * 0.07f + Constants.LAYER0_OFFSET_X  ,
                GameEngine.player.getY() * 0.01f + Constants.LAYER0_OFFSET_Y , 0);
        layer0CopyCam.position.set(GameEngine.player.getX() * 0.09f + Constants.LAYER0_OFFSET_X + randomOffset  ,
                GameEngine.player.getY() * 0.015f + Constants.LAYER0_OFFSET_Y + randomOffset , 0);


        //MOUNTAINS
        layer1Cam.position.set(GameEngine.player.getX()* 0.12f + Constants.LAYER1_OFFSET_X ,
                GameEngine.player.getY() * 0.010f  + Constants.LAYER1_OFFSET_Y - GameEngine.player.getY() * 0.010f, 0);
        layer1CopyCam.position.set(GameEngine.player.getX()* 0.15f + Constants.LAYER1_OFFSET_X ,
                GameEngine.player.getY() * 0.013f  + Constants.LAYER1_OFFSET_Y + 5 - GameEngine.player.getY() * 0.010f, 0);
        //HILLS
        layer2Cam.position.set(GameEngine.player.getX()* 0.2f + Constants.LAYER2_OFFSET_X ,
                GameEngine.player.getY() * 0.078f + Constants.LAYER2_OFFSET_Y, 0);
        //layer2CopyCam.position.set(GameEngine.player.getX()* 0.45f  , GameEngine.player.getY() * 0.99f, 0);

        //PLATEAU
        layer3Cam.position.set(GameEngine.player.getX()* 0.4f + Constants.LAYER3_OFFSET_X ,
                GameEngine.player.getY() * 0.8f + Constants.LAYER3_OFFSET_Y - GameEngine.player.getY() * 0.20f, 0);
        //layer3CopyCam.position.set(GameEngine.player.getX()* 0.65f , GameEngine.player.getY() * 0.60f + 10, 0);
    }


    public void initCams(){
        UIcam = new OrthographicCamera();
        UIcam.setToOrtho(false, Constants.UI_VIEWPORT_WIDTH, Constants.UI_VIEWPORT_HEIGHT);
        UIcam.update();

        layer0Cam = new OrthographicCamera();
        layer0Cam.setToOrtho(false, Constants.LAYER0_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER0_VIEWPORT_HEIGHT / Constants.PPM);
        layer0Cam.update();

        layer0CopyCam = new OrthographicCamera();
        layer0CopyCam.setToOrtho(false, Constants.LAYER0COPY_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER0COPY_VIEWPORT_HEIGHT / Constants.PPM);
        layer0CopyCam.update();

        layer1Cam = new OrthographicCamera();
        layer1Cam.setToOrtho(false, Constants.LAYER1_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER1_VIEWPORT_HEIGHT / Constants.PPM);
        layer1Cam.update();

        layer1CopyCam = new OrthographicCamera();
        layer1CopyCam.setToOrtho(false, Constants.LAYER1COPY_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER1COPY_VIEWPORT_HEIGHT / Constants.PPM);
        layer1CopyCam.update();

        layer2Cam = new OrthographicCamera();
        layer2Cam.setToOrtho(false, Constants.LAYER2_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER2_VIEWPORT_HEIGHT / Constants.PPM);
        layer2Cam.update();

        layer2CopyCam = new OrthographicCamera();
        layer2CopyCam.setToOrtho(false, Constants.LAYER2COPY_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER2COPY_VIEWPORT_HEIGHT / Constants.PPM);
        layer2CopyCam.update();

        layer3Cam = new OrthographicCamera();
        layer3Cam.setToOrtho(false, Constants.LAYER3_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER3_VIEWPORT_HEIGHT / Constants.PPM);
        layer3Cam.update();

        layer3CopyCam = new OrthographicCamera();
        layer3CopyCam.setToOrtho(false, Constants.LAYER3COPY_VIEWPORT_WIDTH / Constants.PPM, Constants.LAYER3COPY_VIEWPORT_HEIGHT / Constants.PPM);
        layer3CopyCam.update();

        gameCam = new OrthographicCamera();
        gameCam.setToOrtho(false, Constants.GAME_VIEWPORT_WIDTH, Constants.GAME_VIEWPORT_HEIGHT);
        gameCam.zoom = Constants.DEFAULT_ZOOM;
        gameCam.update();
        defaultZoom = gameCam.zoom;

    }

    public void zoom(float amount){
        if (amount > 0) {    //scroll down (zoom out)
            gameCam.zoom += 0.013f;
        } else {            //scroll up (zoom in)
            gameCam.zoom -= 0.013f;
        }
    }


}
