package com.lighthearted.game.entities;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.lighthearted.LightheartEd;

public class SpeakBubble extends Sprite {
    private static final String TAG = SpeakBubble.class.getName();

    private LightheartEd main;

    public SpeakBubble(LightheartEd main, float x, float y){
        this.main = main;
        setRegion(main.assets.gameObjectAtlas.findRegion("Speak0"));
        setAlpha(0.6f);
        setBounds(x, y, 50, 35);

    }

    public void draw(Batch batch) {
        super.draw(batch);
    }
}
