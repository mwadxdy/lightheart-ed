package com.lighthearted.game.entities.mage;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.SpeakBubble;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.utils.Constants;

public class Mage extends Sprite {
    private static final String TAG = Mage.class.getName();

    private LightheartEd main;

    public enum State {
        STANDING, WALKING
    }


    public float turnTime = 0;

    public boolean lookRight = true;
    public boolean isWalkingRight = false;
    public boolean toTurn = false;

    private float rand = 3;
    public float movingSpeed = 2f;
    public float maxSpeed = Constants.MAGE_DEFAULT_MAXSPEED;

    public float deltaX = 100;


    public State currentState = State.STANDING;

    //box2d body
    public Body body;


    //speaking
    public SpeakBubble speakBubble;

    public float thinkingTime = 0;
    public float fillingTime = 0;

    public boolean thoughtsFilled = false;
    public boolean sentenceFilled = false;

    public int letterCount = 0;
    public int sentenceCount = 0;
    public int breakCount = 0;
    public int linelength = 0;


    public String[] sentences;
    public String currentSentence = "";
    public String[] letters;
    public String currentParagraph = "";
    public String thoughts = "";
    public String currentLine = "";

    public boolean isSpeaking = false;
    public boolean isThinking = false;
    public boolean hasSpoken = false;

    public Mage(LightheartEd main, float x, float y, int story){
        this.main = main;
        setupSprite(x,y);
        createBody(x,y);
        if(story > 1){
            hasSpoken = true;
        }


    }

    public void setupSprite(float x, float y){
        setRegion(main.assets.mageAtlas.findRegion("MageIdle0"));
        setBounds(x, y, Constants.MAGE_SPRITE_WIDTH, Constants.MAGE_SPRITE_HEIGHT);
        setOrigin(getWidth()/2, getHeight()/2);
        speakBubble = new SpeakBubble(main, x -5 , y - 5);

    }


    public void draw(Batch batch) {
        super.draw(batch);

    }

    public void update(float delta) {
        //Gdx.app.log(TAG, "turntime  " + turnTime);
        deltaX = Math.abs(GameEngine.player.body.getPosition().x - body.getPosition().x);

        if(isSpeaking) {
            think();
            speakBubble.setPosition(getX() - 5, getY() - 5);
            if(deltaX > 50){
                GameEngine.physics.b2DWorldCreator.story++;
                hasSpoken = true;
                isSpeaking = false;
                isThinking = false;
            }
        }else if(deltaX < 15){

            if(!hasSpoken){
                isSpeaking = true;
                isThinking = true;
            }

        }else {
            turnCheck();
            if(isWalkingRight){
                moveRight();
            }else {
                moveLeft();
            }


        }




    }


    public void  think(){
        thinkingTime += Gdx.graphics.getDeltaTime();

        if(!thoughtsFilled) {
            thoughts += repertoire(GameEngine.physics.b2DWorldCreator.story);
            thoughtsFilled = true;
        }

        if(thinkingTime < 3){
            if(letterCount < thoughts.length()) {
                if(!sentenceFilled) {
                    sentences = thoughts.split("\\|");
                    if(sentenceCount < sentences.length)
                        currentSentence += sentences[sentenceCount];
                    if(currentSentence.length() < 2){
                        isThinking = false;
                        isSpeaking = false;
                        hasSpoken = true;
                        GameEngine.physics.b2DWorldCreator.story++;
                    }
                    sentenceFilled = true;
                }

                if(isSpeaking) {
                    if (letterCount < currentSentence.length() && fillingTime < 0.1f) {
                        fillingTime += Gdx.graphics.getDeltaTime();
                        currentParagraph += currentSentence.split("(?!^)")[letterCount];
                        fillingTime = 0;
                        letterCount++;
                    }
                }
            }
        }else if(thinkingTime > 5){
            if(isSpeaking && !hasSpoken) {
                thinkingTime = 0;
                sentenceFilled = false;
                letterCount = 0;
                sentenceCount++;
                currentSentence = "";
                currentParagraph = "";
            }
        }
    }

    public String repertoire(int num){
        String story = "";
        switch (num){
            case 1:
                story = "Gut dass du auch wieder wach\n\nbist, Ed!|" +
                        "Der Trank hat uns wohl beide \n\nziemlich umgehauen...|" +
                        "Leider sind meine \n\nZauberkräfte um keinen \n\nDeut mächtiger geworden... |" +
                        "Ich hab' wohl den falschen\n\nPilz verwendet. Nun gut, ähhm.|Wir müssen uns jetzt\n\nkonzentrieren!|" +
                        "Während wir geschlafen haben,\n\nhüllten böse Mächte Æbenwald\n\nin Dunkelheit.|" +
                        "Sie haben die Sonne gebannt,\n\ndie Natur verdorben...|" +
                        "...und all unsere Freunde\n\nin Geiselhaft genommen.\n\n\n\n" +
                        "Du musst uns retten, Ed!|" +
                        "Sammle die Energie\n\nder Sonnensteine,\n\num damit die Tiere\n\nvon ihrem Bann zu befreien|" +
                        "und die Herzen der\n\nBewohner Æbenwalds wieder\n\nzu erleuchten." +
                        "\n";
                break;
            default:
                return "\n";

        }
        return story;

    }


    public void turnCheck(){
        turnTime += Gdx.graphics.getDeltaTime();

        if(toTurn) {
            if (isWalkingRight) {
                isWalkingRight = false;
            } else {
                isWalkingRight = true;
            }
            toTurn = false;

        }

        if(turnTime > rand){
            toTurn = true;
            rand = MathUtils.random(3, 5);
            turnTime = 0;
        }


    }



    public void createBody(float x, float y) {
        body = B2DBodyCreator.createBox(getX(), getY(),
                Constants.MAGE_HITBOX_WIDTH / Constants.PPM, Constants.MAGE_HITBOX_HEIGHT / Constants.PPM,
                BodyDef.BodyType.DynamicBody,
                1, false, 0, 0,
                Constants.CATEGORY_MONSTER, Constants.MASK_MONSTER,
                this);
    }

    public State getState() {

        if (body.getLinearVelocity().y != 0 ) {
            return State.STANDING;
        } else if (body.getLinearVelocity().x != 0 && body.getLinearVelocity().y == 0) {
            return State.WALKING;
        } else  {
            return State.STANDING;
        }

    }



    public void moveLeft(){
        lookRight = false;
        if (body.getLinearVelocity().x > -maxSpeed) {
            body.applyLinearImpulse(new Vector2(-movingSpeed, 0), body.getLocalCenter(), true);
        }

    }

    public void moveRight(){
        lookRight = true;


        if (body.getLinearVelocity().x < maxSpeed) {
            body.applyLinearImpulse(new Vector2(movingSpeed, 0), body.getLocalCenter(), true);
        }

    }
}
