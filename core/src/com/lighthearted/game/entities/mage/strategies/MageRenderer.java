package com.lighthearted.game.entities.mage.strategies;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public class MageRenderer {
    private static final String TAG = MageRenderer.class.getName();

    private LightheartEd main;

    public float runTime = 0;

    public MageRenderer(LightheartEd main) {
        this.main = main;
    }

    public void render(float delta) {
        GameEngine.mage.setRegion(getFrame(delta));
        attachSprite();

        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        GameEngine.mage.draw(main.batch);
        if(GameEngine.mage.isSpeaking){
            GameEngine.mage.speakBubble.draw(main.batch);
            main.assets.speakBubbleFont.draw(main.batch, GameEngine.mage.currentParagraph, GameEngine.mage.speakBubble.getX() + 15,
                    GameEngine.mage.speakBubble.getY() + 20);
        }
        main.batch.end();
    }

    public TextureRegion getFrame(float delta) {

        TextureRegion region = new TextureRegion();

        runTime += delta;
        switch (GameEngine.mage.getState()) {
            case STANDING:
                region = (TextureRegion) main.assets.mageIdleAnim.getKeyFrame(runTime);
                break;
            case WALKING:
                region = (TextureRegion) (GameEngine.mage.lookRight ? main.assets.mageRightAnim.getKeyFrame(runTime) :  main.assets.mageLeftAnim.getKeyFrame(runTime));
                break;
        }
        return region;
    }



    public void attachSprite() {
        GameEngine.mage.setPosition(GameEngine.mage.body.getPosition().x - GameEngine.mage.getWidth() / 2, GameEngine.mage.body.getPosition().y - GameEngine.mage.getHeight() / 3);

    }

}
