package com.lighthearted.game.entities.player.strategies.tools.hook;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJoint;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.utils.Array;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;


public class Hook extends Sprite {
    private static final String TAG = Hook.class.getName();

    private LightheartEd main;
    public Body body;
    private float x,y,w,h;
    public boolean hooked, toConnect;
    public boolean toDestroy;
    public boolean destroyed = true;
    public float respawnTimer = 2;
    public RopeJoint ropeJoint;
    public float impactX, impactY;
    public boolean isSpawned;
    private float deltaY;
    public boolean isLookingRight;

    public Hook(LightheartEd main){
        this.main = main;

    }

    public void setupSprite(){

        setRegion(main.assets.gameObjectAtlas.findRegion("Hook0"));
        setBounds(x,y,w,h);
        setOrigin(getWidth()/2, getHeight()/2);

    }

    public void update(float delta){
        if(body != null) {
            deltaY = Math.abs(GameEngine.player.body.getPosition().y - body.getPosition().y);
            if(!hooked && (isLookingRight != GameEngine.player.isLookingRight)){
                isLookingRight = GameEngine.player.isLookingRight;
            }
            setPosition(body.getPosition().x - getWidth()/ 2,
                    body.getPosition().y - getHeight() / 2 + 0.5f);

            if(toConnect){
                createRope();
                GameEngine.player.movement.jumpCounter = 0;
                if(deltaY > 5) {
                    if (GameEngine.player.isLookingRight) {
                        GameEngine.player.body.applyLinearImpulse(new Vector2(10, 10), GameEngine.player.body.getLocalCenter(), true);
                    } else {
                        GameEngine.player.body.applyLinearImpulse(new Vector2(-10, 10), GameEngine.player.body.getLocalCenter(), true);
                    }
                }
                impactX = body.getPosition().x;
                impactY = body.getPosition().y;
                hooked = true;
                toConnect = false;
            }

            if(hooked){
                freezeHook();
            }


            if(toDestroy){
                B2DWorldCreator.world.destroyBody(body);
                toDestroy = false;
                destroyed = true;
                hooked = false;
            }
        }

    }



    public void draw(Batch batch)
    {
        if(!destroyed) {
            super.draw(batch);
        }else{
            respawnTimer += Gdx.graphics.getDeltaTime();

        }
    }

    public void createHook(float x, float y){

        if(respawnTimer > 0.1f) {
            if(GameEngine.player.isLookingRight) {
                this.x = x;
            }else {
                this.x = x - 5.5f;
            }
            this.y = y;
            this.w = Constants.HOOK_WIDTH;
            this.h = Constants.HOOK_HEIGHT;
            setupSprite();

            body = B2DBodyCreator.createBox(this.x, this.y,
                    w / Constants.PPM, h / Constants.PPM,
                    BodyDef.BodyType.DynamicBody,
                    1, false, 0, 0,
                    Constants.CATEGORY_PLAYER, Constants.MASK_PLAYER,
                    this);

            if(GameEngine.player.isLookingRight) {
                body.applyLinearImpulse(new Vector2(60, 100), body.getLocalCenter(), true);
            }else{
                body.applyLinearImpulse(new Vector2(-60, 100), body.getLocalCenter(), true);
                flip(true,false);
            }
            body.setLinearDamping(2);
            destroyed = false;
            respawnTimer = 0;
            isSpawned = true;
        }



    }

    public void createRope()  {
        Array<Body> bodies = new Array<Body>();

        bodies.add(body);
        bodies.add(GameEngine.player.body);

        RopeJointDef ropeBodyPart = new RopeJointDef();
        ropeBodyPart.bodyA = bodies.get(0);
        ropeBodyPart.bodyB = bodies.get(1);
        ropeBodyPart.collideConnected = true;
        ropeBodyPart.maxLength = 20f;
        ropeBodyPart.localAnchorA.set(0, 1f);
        ropeBodyPart.localAnchorB.set(0, 0f);

        ropeJoint = (RopeJoint) B2DWorldCreator.world.createJoint(ropeBodyPart);
    }

    public void pullRope(){
        ropeJoint.setMaxLength(ropeJoint.getMaxLength() * 0.95f);

    }

    public void freezeHook(){
        body.setLinearVelocity(0,0);
        body.setTransform(new Vector2(impactX,impactY), 0);

    }

    public void destroy(){
        toDestroy = true;
        hooked = false;
        isSpawned = false;
        GameEngine.player.movement.jumpCounter = 0;

    }

}
