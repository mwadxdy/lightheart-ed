package com.lighthearted.game.entities.player.strategies.tools.hook;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;

public class HookRenderer   {
    private static final String TAG = HookRenderer.class.getName();

    private LightheartEd main;
    private ShapeRenderer shapeRenderer = new ShapeRenderer();

    public HookRenderer(LightheartEd main){
        this.main = main;

    }
    
    public void update(float delta){

    }


    public void render(float delta){
        if(GameEngine.player.actions.hook.body != null) {
            main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
            main.batch.begin();
            GameEngine.player.actions.hook.draw(main.batch);
            main.batch.end();
            if(GameEngine.player.actions.hook.isSpawned) {
                if(GameEngine.player.actions.hook.hooked){
                    if(GameEngine.player.actions.hook.isLookingRight){
                        drawRope(new Vector2(GameEngine.player.getX() + 4.0f,
                                        GameEngine.player.getY() + 2),
                                new Vector2(GameEngine.player.actions.hook.getX() + 2f,
                                        GameEngine.player.actions.hook.getY() + 1.5f),
                                4,
                                Color.BROWN);
                    }else {
                        drawRope(new Vector2(GameEngine.player.getX() + 1.0f,
                                        GameEngine.player.getY() + 2),
                                new Vector2(GameEngine.player.actions.hook.getX() + 3f,
                                        GameEngine.player.actions.hook.getY() + 1.5f),
                                4,
                                Color.BROWN);
                    }
                }else{
                    if(GameEngine.player.isLookingRight){
                        drawRope(new Vector2(GameEngine.player.getX() + 4.0f,
                                        GameEngine.player.getY() + 2),
                                new Vector2(GameEngine.player.actions.hook.getX() + 2.0f,
                                        GameEngine.player.actions.hook.getY() + 1.5f),
                                4,
                                Color.BROWN);
                    }else {
                        drawRope(new Vector2(GameEngine.player.getX() + 1.0f,
                                        GameEngine.player.getY() + 2),
                                new Vector2(GameEngine.player.actions.hook.getX() + 3f,
                                        GameEngine.player.actions.hook.getY() + 1.5f),
                                4,
                                Color.BROWN);
                    }
                }


            }
        }
    }

    public void drawRope(Vector2 start, Vector2 end, int lineWidth, Color color)
    {

        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        Gdx.gl.glLineWidth(lineWidth);
        shapeRenderer.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(color);
        shapeRenderer.line(start, end);
        shapeRenderer.end();
        Gdx.gl.glLineWidth(1);
        main.batch.end();
    }
}
