package com.lighthearted.game.entities.player.strategies.tools.pogo;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.physics.B2DWorldCreator;
import com.lighthearted.utils.Constants;

public class Pogo   {
    private static final String TAG = Pogo.class.getName();

    private LightheartEd main;


    public Pogo(LightheartEd main){
        this.main = main;

    }
    
    public void update(float delta){
        if(GameEngine.player.isPogoing) {

            if (!GameEngine.player.isTransformed) { //EQUIP POGO STICK
                B2DWorldCreator.world.destroyBody(GameEngine.player.body);
                GameEngine.player.setRotation(0);
                GameEngine.camHandler.gameCam.zoom += 0.1;
                GameEngine.player.createBody(GameEngine.player.previousPosition.x + 5, GameEngine.player.previousPosition.y + 5, Constants.PLAYER_HITBOX_WIDTH  / Constants.PPM, Constants.PLAYER_HITBOX_HEIGHT  / Constants.PPM, 1);
                GameEngine.player.setBounds(GameEngine.player.previousPosition.x, GameEngine.player.previousPosition.y, Constants.PLAYER_SPRITE_WIDTH * 2, Constants.PLAYER_SPRITE_HEIGHT * 2);
                GameEngine.player.body.setLinearVelocity(GameEngine.player.previousVelocity.x, GameEngine.player.previousVelocity.y);
                GameEngine.player.isTransformed = true;
            }
        }else{
            if(GameEngine.player.isTransformed){ //UNEQUIP
                B2DWorldCreator.world.destroyBody(GameEngine.player.body);
                GameEngine.camHandler.gameCam.zoom = GameEngine.camHandler.defaultZoom;
                GameEngine.player.createBody(GameEngine.player.previousPosition.x + 5, GameEngine.player.previousPosition.y + 1, Constants.PLAYER_HITBOX_WIDTH  / Constants.PPM, Constants.PLAYER_HITBOX_HEIGHT  / Constants.PPM, 0);
                GameEngine.player.setBounds(GameEngine.player.previousPosition.x, GameEngine.player.previousPosition.y, Constants.PLAYER_SPRITE_WIDTH, Constants.PLAYER_SPRITE_HEIGHT);

                GameEngine.player.isTransformed = false;
            }
        }
    }

}
