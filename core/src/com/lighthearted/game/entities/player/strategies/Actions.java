package com.lighthearted.game.entities.player.strategies;
import com.badlogic.gdx.Gdx;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.player.strategies.tools.pogo.Pogo;
import com.lighthearted.game.entities.player.strategies.tools.hook.Hook;
import com.lighthearted.game.objects.animals.Firefly;
import com.lighthearted.utils.Constants;

public class Actions   {
    private static final String TAG = Actions.class.getName();

    private LightheartEd main;
    public Hook hook;

    public boolean isTargetBehind = false;

    public String currentTool = "";
    public String toolsAvailable = "";
    private float transformCooldown = 0;
    public Pogo pogo;

    public Actions(LightheartEd main, String tool, String tools){
        this.main = main;
        hook = new Hook(main);
        pogo = new Pogo(main);
        currentTool = tool;
        toolsAvailable = tools;

    }
    
    public void update(float delta){
        pogo.update(delta);
        if(GameEngine.player.isPogoing){
            transformCooldown += Gdx.graphics.getDeltaTime();
        }
    }

    //ABILITIES
    public void shootFireflies(float directionX, float directionY){
        GameEngine.player.energy --;
        GameEngine.player.isShooting = true;
        GameEngine.player.shootingResetTimer = 0;
        if(GameEngine.player.isLookingRight) {
            if (directionX < GameEngine.player.getX()) {
                isTargetBehind = true;
            } else {
                isTargetBehind = false;
            }
        }else{
            if (directionX < GameEngine.player.getX()) {
                isTargetBehind = false;
            } else {
                isTargetBehind = true;
            }
        }
        if(GameEngine.player.isLookingRight) { //LOOK RIGHT
            if(isTargetBehind){
                GameEngine.updater.fireflyList.add(new Firefly(main, GameEngine.player.getX() , GameEngine.player.getY() + Constants.FIREFLY_OFFSET_Y, directionX, directionY, false));
            }else{
                GameEngine.updater.fireflyList.add(new Firefly(main, GameEngine.player.getX() + 5f, GameEngine.player.getY() + Constants.FIREFLY_OFFSET_Y, directionX, directionY, false));

            }
        }else{ //LOOK LEFT
            if(isTargetBehind){
                GameEngine.updater.fireflyList.add(new Firefly(main, GameEngine.player.getX() + 4.8f, GameEngine.player.getY() + Constants.FIREFLY_OFFSET_Y, directionX, directionY, false));
            }else{
                GameEngine.updater.fireflyList.add(new Firefly(main, GameEngine.player.getX() - 0f, GameEngine.player.getY() + Constants.FIREFLY_OFFSET_Y, directionX, directionY, false));
            }
        }

    }

    //TOOLS
    public void useTool(){
        if(currentTool.equals("hook")){
            shootHook();
        }else if(currentTool.equals("pogo")){
            pogoing();
        }

    }

    public void equipTool(String tool){
        currentTool = tool;

        GameEngine.stageHandler.isQuicktoolMenupanelVisible = false;
        GameEngine.stageHandler.loadToolButton(currentTool);

    }

    //hook
    public void shootHook(){
        if(!hook.destroyed) {
            hook.destroy();
        }else{
            hook.createHook(GameEngine.player.getX() + 6, GameEngine.player.getY() + 5);
        }
    }

    //hook
    public void pogoing(){

        if(!GameEngine.player.isPogoing) {
            GameEngine.player.isPogoing = true;
        }else {
            if (transformCooldown > 1){
                GameEngine.player.isPogoing = false;
                transformCooldown = 0;
            }
        }
    }






}
