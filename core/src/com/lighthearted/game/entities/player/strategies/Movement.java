package com.lighthearted.game.entities.player.strategies;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public class Movement   {
    private static final String TAG = Movement.class.getName();

    private LightheartEd main;
    public float movingSpeed = 4f;
    public float maxSpeed = Constants.PLAYER_DEFAULT_MAXSPEED;
    public float jumpForce = Constants.PLAYER_DEFAULT_JUMPFORCE;
    public int jumpCounter = 0;

    public boolean saltoing = false;
    public float rotate = 0;

    public float groundDelta = 0;
    public float previousGroundDelta = 0;
    public float highestGroundDelta = 0;
    public boolean toCatchHGD = true;
    public boolean firstZoom = true, secondZoom = true, thirdZoom = true;
    public Movement(LightheartEd main){
        this.main = main;

    }
    
    public void update(){

        determineGroundDelta();

        if(GameEngine.player.getY() < 150){
            GameEngine.updater.restart();
        }

        if(GameEngine.physics.b2DContactListener.playerLavaPlatformContact){
            GameEngine.player.health -= 1f;
        }

        saltoCheck();


    }

    public void startSwimming(){
        GameEngine.player.isSwimming = true;
        saltoing = false;
        GameEngine.player.body.setLinearDamping(10);
    }

    public void endSwimming(){
        GameEngine.player.isSwimming = false;
        GameEngine.player.body.setLinearDamping(0);
    }

    public void moveLeft(){
        if(GameEngine.player.isLookingRight) {
            GameEngine.player.isLookingRight = false;
            if(!GameEngine.player.actions.hook.hooked) {

                GameEngine.player.actions.hook.flip(true, false);
            }

            //GameEngine.player.headLight.remove();
            //GameEngine.player.attachHeadlamp(GameEngine.physics.lightning);
        }
        if (GameEngine.player.body.getLinearVelocity().x > -maxSpeed) {
            GameEngine.player.body.applyLinearImpulse(new Vector2(-movingSpeed, 0), GameEngine.player.body.getLocalCenter(), true);
        }
    }

    public void moveRight(){
        if(!GameEngine.player.isLookingRight) {
            GameEngine.player.isLookingRight = true;
            if(!GameEngine.player.actions.hook.hooked){
                GameEngine.player.actions.hook.flip(true, false);

            }
            //GameEngine.player.headLight.remove();
            //GameEngine.player.attachHeadlamp(GameEngine.physics.lightning);
        }

        if (GameEngine.player.body.getLinearVelocity().x < maxSpeed) {
            GameEngine.player.body.applyLinearImpulse(new Vector2(movingSpeed, 0), GameEngine.player.body.getLocalCenter(), true);
        }
    }

    public void jump(){
        //GODMODE
        //jumpCounter = 0;
        if(GameEngine.player.isAlive && !GameEngine.player.isPogoing) {
            if (main.assets.isSoundActivated) {
                main.assets.jumpSound.play();
            }
            if (GameEngine.player.actions.hook.hooked && GameEngine.physics.b2DContactListener.playerGroundContact) {
                GameEngine.player.actions.hook.destroy();
            }
            if (GameEngine.player.isSwimming) {
                jumpCounter = 0;
                GameEngine.player.body.applyLinearImpulse(new Vector2(0, jumpForce), GameEngine.player.body.getPosition(), true);
            }
            switch (jumpCounter) {
                case 0:
                    GameEngine.player.body.setLinearVelocity(GameEngine.player.body.getLinearVelocity().x, 0);
                    GameEngine.player.body.applyLinearImpulse(new Vector2(0, jumpForce), GameEngine.player.body.getPosition(), true);
                    jumpCounter++;

                    break;
                case 1:
                    GameEngine.player.body.setLinearVelocity(GameEngine.player.body.getLinearVelocity().x, 0);
                    GameEngine.player.body.applyLinearImpulse(new Vector2(0, jumpForce), GameEngine.player.body.getPosition(), true);
                    jumpCounter++;

                    break;
                case 2:
                    GameEngine.player.body.setLinearVelocity(GameEngine.player.body.getLinearVelocity().x, 0);
                    GameEngine.player.body.applyLinearImpulse(new Vector2(0, jumpForce), GameEngine.player.body.getPosition(), true);
                    jumpCounter++;
                    if (!GameEngine.player.isSwimming) {
                        saltoing = true;
                    }
                    break;
            }
        }
    }

    public void saltoCheck(){
        if(saltoing && (rotate < 360 || rotate > -360) && !GameEngine.player.actions.hook.hooked &&!GameEngine.player.isPogoing){
            if(GameEngine.player.isLookingRight) {
                GameEngine.player.rotate(- Constants.ROTATION_SPEED );
                rotate = rotate - Constants.ROTATION_SPEED ;
            }else {
                GameEngine.player.rotate(Constants.ROTATION_SPEED);
                rotate = rotate + Constants.ROTATION_SPEED ;

            }
        }
    }

    public void determineGroundDelta(){
        if(GameEngine.player.isPogoing){
            previousGroundDelta = groundDelta;
            groundDelta = Math.abs(GameEngine.physics.b2DContactListener.lastGroundedY - GameEngine.player.body.getPosition().y);
            //Gdx.app.log(TAG, "grounddelta " + groundDelta );

            if(groundDelta < previousGroundDelta){
                if(toCatchHGD){
                    highestGroundDelta = groundDelta;
                    //Gdx.app.log(TAG, "hgD " + highestGroundDelta );
                    toCatchHGD = false;
                }
            }
            /*
            if(highestGroundDelta > 10){
                if(firstZoom){
                    GameEngine.camHandler.gameCam.zoom += 0.05f;
                    firstZoom = false;
                }
            }
            if(highestGroundDelta > 40){
                if(secondZoom){
                    GameEngine.camHandler.gameCam.zoom += 0.1f;
                    secondZoom = false;
                }
            }

            if(highestGroundDelta > 70){
                if(thirdZoom){
                    GameEngine.camHandler.gameCam.zoom += 0.2f;
                    thirdZoom = false;
                }
            }*/

        }
    }


}
