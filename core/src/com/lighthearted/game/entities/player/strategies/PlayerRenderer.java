package com.lighthearted.game.entities.player.strategies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.player.strategies.tools.hook.HookRenderer;

public class PlayerRenderer{
    private static final String TAG = PlayerRenderer.class.getName();

    private LightheartEd main;

    public float runTime = 0;

    private HookRenderer hookRenderer;
    private float groundingCooldown = 0;


    public PlayerRenderer(LightheartEd main){
        this.main = main;

        hookRenderer = new HookRenderer(main);

    }
    
    public void render(float delta){

        GameEngine.player.setRegion(getFrame(delta));
        attachSprite();

        main.batch.setProjectionMatrix(GameEngine.camHandler.gameCam.combined);
        main.batch.begin();
        GameEngine.player.draw(main.batch);
        main.batch.end();

        hookRenderer.render(delta);

    }

    public void attachSprite(){
        if(GameEngine.player.isAlive) {
            if(GameEngine.player.isPogoing){
                GameEngine.player.setPosition(GameEngine.player.body.getPosition().x - GameEngine.player.getWidth() /2,
                        (GameEngine.player.body.getPosition().y - GameEngine.player.getHeight() / 2) + 3);
            }else{
                GameEngine.player.setPosition(GameEngine.player.body.getPosition().x - GameEngine.player.getWidth() / 2,
                        (GameEngine.player.body.getPosition().y - GameEngine.player.getHeight() / 2) + 1);
            }

        }else {
            GameEngine.player.setPosition(GameEngine.player.body.getPosition().x - GameEngine.player.getWidth() / 2,
                    (GameEngine.player.body.getPosition().y - GameEngine.player.getHeight() / 2) - 2);
        }

    }



    public TextureRegion getFrame(float delta){
        TextureRegion region = new TextureRegion();
        if(GameEngine.player.isAlive) {
            runTime += delta;
        }
        //Gdx.app.log(TAG, "state                " + GameEngine.player.getState() + "    " + GameEngine.player.isPogoing );
        switch (GameEngine.player.getState()){
            case STANDING:
                region = GameEngine.player.isLookingRight ? main.assets.idleRightAnim.getKeyFrame(runTime) : main.assets.idleLeftAnim.getKeyFrame(runTime);
                break;
            case RUNNING:
                region = GameEngine.player.isLookingRight ? main.assets.runRightAnim.getKeyFrame(runTime) : main.assets.runLeftAnim.getKeyFrame(runTime);
                break;
            case JUMP_UP:
                region = GameEngine.player.isLookingRight ? main.assets.jumpUpRightAnim.getKeyFrame(runTime) : main.assets.jumpUpLeftAnim.getKeyFrame(runTime);
                break;
            case FALL_DOWN:
                region = GameEngine.player.isLookingRight ? main.assets.fallDownRightAnim.getKeyFrame(runTime) : main.assets.fallDownLeftAnim.getKeyFrame(runTime);
                break;
            case STANDING_FF:
                region = GameEngine.player.isLookingRight ? main.assets.standingFFCanonRightAnim.getKeyFrame(runTime) : main.assets.standingFFCanonLeftAnim.getKeyFrame(runTime);
                break;
            case RUNNING_FF:
                region = GameEngine.player.isLookingRight ? main.assets.runningFFCanonRightAnim.getKeyFrame(runTime) : main.assets.runningFFCanonLeftAnim.getKeyFrame(runTime);
                break;
            case STANDING_FF_TURNED:
                region = GameEngine.player.isLookingRight ? main.assets.standingFFCanonTurnedRightAnim.getKeyFrame(runTime) : main.assets.standingFFCanonTurnedLeftAnim.getKeyFrame(runTime);
                break;
            case RUNNING_FF_TURNED:
                region = GameEngine.player.isLookingRight ? main.assets.runningFFCanonTurnedRightAnim.getKeyFrame(runTime) : main.assets.runningFFCanonTurnedLeftAnim.getKeyFrame(runTime);
                break;
            case POGOING:


                if(GameEngine.physics.b2DContactListener.playerGroundContact || GameEngine.physics.b2DContactListener.playerLavaPlatformContact){
                    region = GameEngine.player.isLookingRight ? main.assets.pogoRightAnim.getKeyFrame(0) : main.assets.pogoLeftAnim.getKeyFrame(0);
                    groundingCooldown += 0.1f;
                }else {
                    if(groundingCooldown > 0){
                        region = GameEngine.player.isLookingRight ? main.assets.pogoRightAnim.getKeyFrame(0) : main.assets.pogoLeftAnim.getKeyFrame(0);
                        groundingCooldown -= Gdx.graphics.getDeltaTime();
                    }else {
                        if(GameEngine.inputHandler.touchInput.isJumpTouched){
                            region = GameEngine.player.isLookingRight ? main.assets.pogoRightAnim.getKeyFrame(2) : main.assets.pogoLeftAnim.getKeyFrame(2);
                        }else{
                            region = GameEngine.player.isLookingRight ? main.assets.pogoRightAnim.getKeyFrame(1) : main.assets.pogoLeftAnim.getKeyFrame(1);
                        }

                    }
                }
                break;

        }


        return region;
    }


}
