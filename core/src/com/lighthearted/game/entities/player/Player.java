package com.lighthearted.game.entities.player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.player.strategies.Actions;
import com.lighthearted.game.entities.player.strategies.Movement;
import com.lighthearted.game.physics.B2DBodyCreator;
import com.lighthearted.game.physics.Lightning;
import com.lighthearted.utils.Constants;

import box2dLight.ConeLight;

public class Player extends Sprite {
    private static final String TAG = Player.class.getName();

    private LightheartEd main;

    public Movement movement;
    public Actions actions;
    public ConeLight headLight;

    public enum State {
        STANDING, RUNNING,
        JUMP_UP, FALL_DOWN,
        FLYING, SLIDING,
        STANDING_FF, RUNNING_FF,
        STANDING_FF_TURNED, RUNNING_FF_TURNED,
        POGOING
    }

    public float health = 100;
    public int energy = 0;

    public State currentState = State.STANDING;

    public Body body;

    public boolean isAlive = true;
    public boolean isLookingRight = true;
    public boolean isShooting = false;
    public boolean isSwimming = false;
    public boolean isPogoing = false;
    public boolean isTransformed = false;

    public boolean hasPet = false;

    public Vector2 spawnCoordinates;
    public Vector2 previousPosition;
    public Vector2 previousVelocity;



    public float shootingResetTimer = 0;
    public boolean toDestroy = false, destroyed;


    public Player(LightheartEd main, float x, float y, Lightning lightning, int energy, float health, String tool, String tools){
        this.main = main;
        previousPosition = new Vector2(x, y);
        spawnCoordinates = new Vector2(x, y);
        previousVelocity = new Vector2(0,0);

        this.energy = energy;
        this.health = health;
        setupSprite(x, y);
        createBody(x, y, Constants.PLAYER_HITBOX_WIDTH / Constants.PPM, Constants.PLAYER_HITBOX_HEIGHT / Constants.PPM, 0);
        loadStrategies(tool, tools);
    }

    public void loadStrategies(String tool, String tools){
        movement = new Movement(main);
        actions = new Actions(main, tool, tools);

    }

    public void setupSprite(float x, float y){
        setPosition(x,y);
        setBounds(x, y, Constants.PLAYER_SPRITE_WIDTH, Constants.PLAYER_SPRITE_HEIGHT);
        setOrigin(getWidth()/2, getHeight()/2);

    }

    public void update(float delta){
        if(health < 1){
            if(isAlive){
                if(GameEngine.hardmode){
                    main.assets.prefs.clear();
                    main.assets.prefs.flush();
                }
                rotate(270);
                isAlive = false;
            }

        }
        previousPosition.x = getX();
        previousPosition.y = getY();
        previousVelocity = new Vector2(body.getLinearVelocity().x ,body.getLinearVelocity().y);
        if(isShooting){
            shootingResetTimer += delta;
            if(shootingResetTimer > 0.3f ){
                isShooting = false;
            }
        }

        actions.update(delta);
        movement.update();
        actions.hook.update(delta);
    }

    public State getState() {


        if (body.getLinearVelocity().y != 0 &&
                !GameEngine.physics.b2DContactListener.playerPolylineContact && !GameEngine.physics.b2DContactListener.playerRampContact
                ) {
            if (body.getLinearVelocity().y > 0) {
                if(isPogoing){
                    return State.POGOING;
                }else {
                    return State.JUMP_UP;
                }
            } else {
                if(isPogoing){
                    return State.POGOING;
                }else {
                    return State.FALL_DOWN;
                }
            }
        } else if (body.getLinearVelocity().x != 0 && (GameEngine.inputHandler.touchInput.isLeftTouched || GameEngine.inputHandler.touchInput.isRightTouched)) {
            if (isShooting) {
                if(actions.isTargetBehind){
                    return State.RUNNING_FF_TURNED;

                }else{
                    return State.RUNNING_FF;
                }
            } else {
                if(isPogoing){
                    return State.POGOING;
                }else {
                    return State.RUNNING;
                }
            }
        } else  {
            if (isShooting) {
                if(actions.isTargetBehind){
                    return State.STANDING_FF_TURNED;

                }else{
                    return State.STANDING_FF;
                }
            } else {
                if(isPogoing) {
                    return State.POGOING;
                }else{
                    return State.STANDING;
                }

            }
        }

    }


    public void createBody(float x, float y, float w, float h, float resti){

        body = B2DBodyCreator.createBox(x, y,
                w, h,
                BodyDef.BodyType.DynamicBody,
                1, false, 0, resti,
                Constants.CATEGORY_PLAYER,
                Constants.MASK_PLAYER,
                this);

        //conserve momentum
        body.setLinearVelocity(body.getLinearVelocity().x, previousVelocity.y);

        //attachHeadlamp(lightning);
    }

    public void draw(Batch batch) {
        super.draw(batch);
    }

    public void attachHeadlamp(Lightning lightning){
        if(isLookingRight) {
            headLight = new ConeLight(lightning.rayHandler, 8,
                    new Color(242 / 255, 25 / 255, 12 / 255, 1),
                    Constants.HEADLAMP_LENGHT, getX(), getY(), -45, 60);
            headLight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.MASK_LIGHT);
            headLight.attachToBody(body, Constants.HEADLAMP_X, Constants.HEADLAMP_Y, 0);
        }else {
            headLight = new ConeLight(lightning.rayHandler, 8,
                    new Color(242 / 255, 25 / 255, 12 / 255, 1),
                    Constants.HEADLAMP_LENGHT, getX(), getY(), -45, 60);
            headLight.setContactFilter(Constants.CATEGORY_LIGHT, Constants.ZERO, Constants.MASK_LIGHT);
            headLight.attachToBody(body, -Constants.HEADLAMP_X, Constants.HEADLAMP_Y, 170);
        }
    }

    public void fillEnergy(){
        if(energy < 287){
            energy++;

        }
    }
}
