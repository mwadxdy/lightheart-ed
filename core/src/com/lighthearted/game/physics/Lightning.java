package com.lighthearted.game.physics;
import com.badlogic.gdx.physics.box2d.World;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;

import box2dLight.DirectionalLight;
import box2dLight.RayHandler;

public class Lightning   {
    private static final String TAG = Lightning.class.getName();

    private LightheartEd main;
    public RayHandler rayHandler;
    public float globalLightness = 0.4f;
    public boolean toLit = false;
    public Lightning(LightheartEd main){
        this.main = main;

    }


    public void initRayhandler(){
        rayHandler = new RayHandler(B2DWorldCreator.world);
        rayHandler.setAmbientLight(0,0,0, globalLightness);

        rayHandler.setBlurNum(0);
        rayHandler.setShadows(true);
    }


    public void update(){
        rayHandler.setCombinedMatrix(GameEngine.camHandler.gameCam);
        rayHandler.update();
        if(toLit){
            rayHandler.setAmbientLight(0,0,0, globalLightness);
            toLit = false;
        }


    }
    public void render(){

        rayHandler.render();

    }
}
