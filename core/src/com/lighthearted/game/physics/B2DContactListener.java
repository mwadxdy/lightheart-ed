package com.lighthearted.game.physics;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.mage.Mage;
import com.lighthearted.game.map.environment.Polylines;
import com.lighthearted.game.map.environment.Ramps;
import com.lighthearted.game.map.environment.Water;
import com.lighthearted.game.objects.actions.Altar;
import com.lighthearted.game.objects.actions.Torch;
import com.lighthearted.game.map.environment.Walls;
import com.lighthearted.game.objects.animals.Bear;
import com.lighthearted.game.entities.player.Player;
import com.lighthearted.game.map.environment.Grounds;
import com.lighthearted.game.objects.animals.Firefly;
import com.lighthearted.game.objects.animals.Kingfisher;
import com.lighthearted.game.objects.collectables.HealthPotion;
import com.lighthearted.game.objects.collectables.Hook;
import com.lighthearted.game.objects.collectables.Pogo;
import com.lighthearted.game.objects.collectables.SunStone;
import com.lighthearted.game.objects.environment.LavaPlatform;

public class B2DContactListener implements ContactListener {
    private static final String TAG = B2DContactListener.class.getName();

    private LightheartEd main;
    public boolean playerGroundContact;
    public boolean playerRampContact;
    public boolean playerPolylineContact;
    public boolean playerLavaPlatformContact;

    public boolean bearRampContact;
    private float pushCooldown = 0;
    public float lastGroundedY;

    public boolean playerFireFlyContact;

    public B2DContactListener(LightheartEd main){
        this.main = main;
        B2DWorldCreator.world.setContactListener(this);

    }
    
    public void update(float delta){
    
    }


    @Override
    public void beginContact(Contact contact) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();


        playerContactBegin(a, b, fixA, fixB);
        fireflyContactBegin(a, b, fixA, fixB);
        kingfisherContactBegin(a, b, fixA, fixB);
        bearContactBegin(a, b, fixA, fixB);


    }

    @Override
    public void endContact(Contact contact) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        playerContactEnd(a, b, fixA, fixB);
        fireflyContactEnd(a, b, fixA, fixB);
        bearContactEnd(a, b, fixA, fixB);
    }





    private void playerContactBegin(Body a, Body b, Fixture fixA, Fixture fixB){
        // BEGIN PLAYER GROUND
        if ((a.getUserData() instanceof Grounds && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Grounds))) {
            GameEngine.player.movement.jumpCounter = 0;
            playerGroundContact = true;
            GameEngine.player.movement.saltoing = false;
            GameEngine.inputHandler.touchInput.pushCooldown = 0;
            GameEngine.player.movement.toCatchHGD = true;
            if(GameEngine.player.isAlive) {
                GameEngine.player.setRotation(0);
            }

            if(!GameEngine.player.isPogoing){
                if(GameEngine.player.body.getLinearVelocity().y < -70){
                    GameEngine.player.health += GameEngine.player.body.getLinearVelocity().y * 0.55f;
                }

            }else {
                if(GameEngine.inputHandler.touchInput.isJumpTouched){
                    GameEngine.player.body.setLinearVelocity(GameEngine.player.body.getLinearVelocity().x, GameEngine.player.body.getLinearVelocity().y * 0.8f);
                }
            }

        }

        // BEGIN PLAYER RAMP
        if ((a.getUserData() instanceof Ramps && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Ramps))) {
            GameEngine.player.movement.jumpCounter = 0;
            playerRampContact = true;
            GameEngine.player.movement.saltoing = false;
            if(GameEngine.player.isAlive) {
                GameEngine.player.setRotation(0);
            }
        }

        // BEGIN PLAYER LAVAPLATFORM
        if ((a.getUserData() instanceof LavaPlatform && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof LavaPlatform))) {
            playerLavaPlatformContact = true;
            GameEngine.player.movement.jumpCounter = 0;
            GameEngine.inputHandler.touchInput.pushCooldown = 0;
            GameEngine.player.movement.toCatchHGD = true;
            GameEngine.player.movement.saltoing = false;
            if(GameEngine.player.isAlive) {
                GameEngine.player.setRotation(0);
            }
            if(GameEngine.player.isPogoing){
                if(GameEngine.inputHandler.touchInput.isJumpTouched){
                    GameEngine.player.body.setLinearVelocity(GameEngine.player.body.getLinearVelocity().x, GameEngine.player.body.getLinearVelocity().y * 0.7f);

                }
            }


        }


        // BEGIN PLAYER MAGE
        if ((a.getUserData() instanceof Mage && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Mage))) {
            GameEngine.player.movement.jumpCounter = 0;
            playerGroundContact = true;
            GameEngine.player.movement.saltoing = false;
            GameEngine.player.setRotation(0);
        }

        // BEGIN PLAYER POLYLINE
        if ((a.getUserData() instanceof Polylines && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Polylines))) {
            GameEngine.player.movement.jumpCounter = 0;
            playerPolylineContact = true;
        }


        // BEGIN PLAYER HOOK
        if ((a.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook))) {
            GameEngine.player.actions.hook.destroy();
        }


        // BEGIN PLAYER HOOKTREASURE
        if ((a.getUserData() instanceof Hook && b.getUserData() instanceof Player)) {
            ((Hook) a.getUserData()).collect();
        }else if ((b.getUserData() instanceof Hook && a.getUserData() instanceof Player)) {
            ((Hook) b.getUserData()).collect();
        }

        // BEGIN PLAYER POGOTREASURE
        if ((a.getUserData() instanceof Pogo && b.getUserData() instanceof Player)) {
            ((Pogo) a.getUserData()).collect();
        }else if ((b.getUserData() instanceof Pogo && a.getUserData() instanceof Player)) {
            ((Pogo) b.getUserData()).collect();
        }



        // BEGIN HOOK GROUND
        if ((a.getUserData() instanceof Grounds && b.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook ||
                (a.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook && b.getUserData() instanceof Grounds))) {
            GameEngine.player.actions.hook.toConnect = true;
        }

        // BEGIN HOOK WALLS
        if ((a.getUserData() instanceof Walls && b.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook ||
                (a.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook && b.getUserData() instanceof Walls))) {
            GameEngine.player.actions.hook.toConnect = true;
        }

        // BEGIN HOOK POLYLINE
        if ((a.getUserData() instanceof Polylines && b.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook ||
                (a.getUserData() instanceof com.lighthearted.game.entities.player.strategies.tools.hook.Hook && b.getUserData() instanceof Polylines))) {
            GameEngine.player.actions.hook.toConnect = true;
        }


        // BEGIN PLAYER FIREFLY
        if ((a.getUserData() instanceof Firefly && b.getUserData() instanceof Player)) {
            ((Player) b.getUserData()).fillEnergy();
            ((Firefly) a.getUserData()).toRemove = true;
            playerFireFlyContact = true;
        }else if ((b.getUserData() instanceof Firefly && a.getUserData() instanceof Player)) {
            ((Player) a.getUserData()).fillEnergy();
            ((Firefly) b.getUserData()).toRemove = true;
            playerFireFlyContact = true;
        }

        // BEGIN PLAYER BEAR
        if ((a.getUserData() instanceof Bear && b.getUserData() instanceof Player)) {
            ((Bear) a.getUserData()).playerContact = true;
            GameEngine.player.movement.jumpCounter = 0;
            GameEngine.player.movement.saltoing = false;
            if(GameEngine.player.isAlive) {
                GameEngine.player.setRotation(0);
            }
        }else if ((b.getUserData() instanceof Bear && a.getUserData() instanceof Player)) {
            ((Bear) b.getUserData()).playerContact = true;
            GameEngine.player.movement.jumpCounter = 0;
            GameEngine.player.movement.saltoing = false;
            if(GameEngine.player.isAlive) {
                GameEngine.player.setRotation(0);
            }

        }

        // BEGIN PLAYER KINGFISHER
        if ((a.getUserData() instanceof Player && b.getUserData() instanceof Kingfisher)) {
            if(!((Kingfisher) b.getUserData()).follow) {
                ((Kingfisher) b.getUserData()).destroy();
                ((Kingfisher) b.getUserData()).toSpawn = true;
            }
        }

        // BEGIN PLAYER SUNSTONE
        if ((a.getUserData() instanceof Player && b.getUserData() instanceof SunStone)) {
            ((SunStone) b.getUserData()).collect();
        }else if ((b.getUserData() instanceof Player && a.getUserData() instanceof SunStone)) {
            ((SunStone) a.getUserData()).collect();

        }

        // BEGIN PLAYER HEALTHPOTION
        if ((a.getUserData() instanceof Player && b.getUserData() instanceof HealthPotion)) {
            ((HealthPotion) b.getUserData()).collect();
        }else if ((b.getUserData() instanceof Player && a.getUserData() instanceof HealthPotion)) {
            ((HealthPotion) a.getUserData()).collect();

        }

        // BEGIN PLAYER WATER
        if ((a.getUserData() instanceof Player && b.getUserData() instanceof Water)) {
            ((Player) a.getUserData()).movement.startSwimming();


        }else if ((b.getUserData() instanceof Player && a.getUserData() instanceof SunStone)) {
            ((Player) b.getUserData()).movement.startSwimming();

        }


    }

    private void fireflyContactBegin(Body a, Body b, Fixture fixA, Fixture fixB){
        // BEGIN FIREFLY GROUND
        if ((a.getUserData() instanceof Grounds && b.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).groundContact = true;
        }

        // BEGIN FIREFLY TORCH
        if ((a.getUserData() instanceof Torch && b.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).toRemove = true;
            ((Torch) a.getUserData()).lit();
        }else if ((b.getUserData() instanceof Torch && a.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).toRemove = true;
            ((Torch) b.getUserData()).lit();
        }


        // BEGIN FIREFLY BEAR
        if ((a.getUserData() instanceof Bear && b.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).toRemove = true;
            ((Bear) a.getUserData()).heal();

        }else if ((b.getUserData() instanceof Bear && a.getUserData() instanceof Firefly)) {
            ((Firefly) a.getUserData()).toRemove = true;
            ((Bear) a.getUserData()).heal();


        }

        // BEGIN FIREFLY ALTAR
        if ((a.getUserData() instanceof Altar && b.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).toRemove = true;
            ((Altar) a.getUserData()).hit();

        }else if ((b.getUserData() instanceof Altar && a.getUserData() instanceof Firefly)) {
            ((Firefly) a.getUserData()).toRemove = true;
            ((Altar) a.getUserData()).hit();
        }

    }

    private void kingfisherContactBegin(Body a, Body b, Fixture fixA, Fixture fixB){
        // BEGIN KINGFISHER SUNSTONE
        if ((a.getUserData() instanceof Kingfisher && b.getUserData() instanceof SunStone)) {
            ((SunStone) b.getUserData()).collect();
        }


    }

    private void bearContactBegin(Body a, Body b, Fixture fixA, Fixture fixB){
        // BEGIN BEAR RAMP
        if ((a.getUserData() instanceof Bear && b.getUserData() instanceof Ramps)
                || (b.getUserData() instanceof Bear && a.getUserData() instanceof Ramps)) {
            bearRampContact = true;
        }


    }

    //----------------------E N D-------------------------------


    private void playerContactEnd(Body a, Body b, Fixture fixA, Fixture fixB){
        //END PLAYER GROUNDS
        if ((a.getUserData() instanceof Grounds && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Grounds))) {
            /*if(GameEngine.player.isPogoing){
                GameEngine.player.body.applyLinearImpulse(new Vector2(0, 10), GameEngine.player.body.getLocalCenter(), true);
            }*/
            playerGroundContact = false;
            lastGroundedY = GameEngine.player.body.getPosition().y;
        }

        // END PLAYER RAMP
        if ((a.getUserData() instanceof Ramps && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Ramps))) {
            playerRampContact = false;

        }

        // END PLAYER LAVAPLATFORM
        if ((a.getUserData() instanceof LavaPlatform && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof LavaPlatform))) {
            playerLavaPlatformContact = false;
            lastGroundedY = GameEngine.player.body.getPosition().y;

            /*if(GameEngine.player.isPogoing){
                GameEngine.player.body.applyLinearImpulse(new Vector2(0, 10), GameEngine.player.body.getLocalCenter(), true);
            }*/
        }

        //END PLAYER POLYLINES
        if ((a.getUserData() instanceof Polylines && b.getUserData() instanceof Player ||
                (a.getUserData() instanceof Player && b.getUserData() instanceof Polylines))) {
            playerPolylineContact = false;
        }


        // END PLAYER FIREFLY
        if ((a.getUserData() instanceof Firefly && b.getUserData() instanceof Player)) {
            playerFireFlyContact = true;
        }else if ((b.getUserData() instanceof Firefly && a.getUserData() instanceof Player)) {
            playerFireFlyContact = true;
        }
        // END PLAYER BEAR
        if ((a.getUserData() instanceof Bear && b.getUserData() instanceof Player)) {
            ((Bear) a.getUserData()).playerContact = false;
        }else if ((b.getUserData() instanceof Bear && a.getUserData() instanceof Player)) {
            ((Bear) b.getUserData()).playerContact = false;

        }

        // END PLAYER WATER
        if ((a.getUserData() instanceof Player && b.getUserData() instanceof Water)) {
            ((Player) a.getUserData()).movement.endSwimming();


        }else if ((b.getUserData() instanceof Player && a.getUserData() instanceof SunStone)) {
            ((Player) b.getUserData()).movement.endSwimming();
        }


    }

    private void fireflyContactEnd(Body a, Body b, Fixture fixA, Fixture fixB){
        // END FIREFLY GROUND

        if ((a.getUserData() instanceof Grounds && b.getUserData() instanceof Firefly)) {
            ((Firefly) b.getUserData()).groundContact = false;
        }else if ((b.getUserData() instanceof Grounds && a.getUserData() instanceof Firefly)) {
            ((Firefly) a.getUserData()).groundContact = false;
        }

    }

    private void bearContactEnd(Body a, Body b, Fixture fixA, Fixture fixB){
        // END BEAR RAMP
        if ((a.getUserData() instanceof Bear && b.getUserData() instanceof Ramps)
                || (b.getUserData() instanceof Bear && a.getUserData() instanceof Ramps)) {
            bearRampContact = false;
        }
    }










    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

