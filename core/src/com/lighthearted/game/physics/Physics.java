package com.lighthearted.game.physics;

import com.lighthearted.LightheartEd;


public class Physics   {
    private static final String TAG = Physics.class.getName();

    private LightheartEd main;
    public B2DWorldCreator b2DWorldCreator;
    public Lightning lightning;
    public B2DContactListener b2DContactListener;

    public Physics(LightheartEd main){
        this.main = main;
        lightning = new Lightning(main);

        b2DWorldCreator = new B2DWorldCreator(main, lightning);
        b2DContactListener = new B2DContactListener(main);

    }
    
    public void update(float delta){
        lightning.update();
        b2DWorldCreator.update(delta);

    }



}
