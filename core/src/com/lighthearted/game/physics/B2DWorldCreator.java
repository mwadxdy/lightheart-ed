package com.lighthearted.game.physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.game.entities.mage.Mage;
import com.lighthearted.game.map.environment.Polylines;
import com.lighthearted.game.map.environment.Ramps;
import com.lighthearted.game.map.environment.Slides;
import com.lighthearted.game.map.environment.Walls;
import com.lighthearted.game.map.environment.Water;
import com.lighthearted.game.objects.actions.Altar;
import com.lighthearted.game.objects.animals.Bear;
import com.lighthearted.game.entities.player.Player;
import com.lighthearted.game.map.environment.Grounds;
import com.lighthearted.game.objects.actions.Torch;
import com.lighthearted.game.objects.animals.Kingfisher;
import com.lighthearted.game.objects.collectables.HealthPotion;
import com.lighthearted.game.objects.collectables.Hook;
import com.lighthearted.game.objects.collectables.Pogo;
import com.lighthearted.game.objects.collectables.SunStone;
import com.lighthearted.game.objects.environment.LavaPlatform;
import com.lighthearted.utils.Constants;


public class B2DWorldCreator {
    private static final String TAG = B2DWorldCreator.class.getName();

    private LightheartEd main;

    public static World world;
    public static float player_spawn_x, player_spawn_y;

    public Grounds grounds;
    public Polylines polylines;
    public Ramps ramps;
    public Slides slides;
    public Walls walls;
    public Array<Torch> torchList;
    public Array<Altar> altarList;
    public Array<Bear> bearList;
    public Array<Kingfisher> kingfisherList;
    public Array<SunStone> sunstoneList;
    public Array<HealthPotion> healthPotionList;
    public Array<Water> waterList;
    public Array<LavaPlatform> lavaPlatformList;

    public Hook hookTreasure;
    public Pogo pogoTreasure;


    public ObjectMap bearMap;

    public String playerPos;
    public float playerHealth;
    public String playerEnergy;
    public boolean playerHasPet = false;
    public String healedBears = "";
    public String healedAltars = "";
    public int altarHoles = 0;
    public int story = 1;
    public String tools = "";
    public String toolInUse = "";
    public int wildlifeCleansed = 0;
    public int torchesLit = 0;
    public String litTorches = "";
    public int altarsCleansed = 0;

    public B2DWorldCreator(LightheartEd main, Lightning lightning){
        this.main = main;
        torchList = new Array<Torch>();
        bearList = new Array<Bear>();
        kingfisherList = new Array<Kingfisher>();
        sunstoneList = new Array<SunStone>();
        healthPotionList = new Array<HealthPotion>();
        altarList = new Array<Altar>();
        waterList = new Array<Water>();
        lavaPlatformList = new Array<LavaPlatform>();
        bearMap = new ObjectMap();
        if(!GameEngine.hardmode){
            load();
        }
        initBox2DWorld(lightning);
    }

    public void save(){
        playerPos = (int) (GameEngine.player.body.getPosition().x + GameEngine.player.getWidth() / 2) + ","
                + (int) (GameEngine.player.body.getPosition().y + GameEngine.player.getHeight() / 2) ;

        main.assets.prefs.putString("playerPos", playerPos);

        playerHasPet = GameEngine.player.hasPet;
        main.assets.prefs.putBoolean("playerHasPet", playerHasPet);



        playerHealth = GameEngine.player.health ;
        main.assets.prefs.putFloat("playerHealth", playerHealth);

        playerEnergy = GameEngine.player.energy + "";
        main.assets.prefs.putString("playerEnergy", playerEnergy);

        main.assets.prefs.putString("healedBears", healedBears);
        main.assets.prefs.putInteger("wildlifeCleansed", wildlifeCleansed);
        main.assets.prefs.putInteger("torchesLit", torchesLit);
        main.assets.prefs.putString("litTorches", litTorches);
        main.assets.prefs.putInteger("altarsCleansed", altarsCleansed);

        main.assets.prefs.putString("healedAltars", healedAltars);
        main.assets.prefs.putInteger("altarHoles", altarHoles);
        main.assets.prefs.putInteger("story", story);

        tools = GameEngine.player.actions.toolsAvailable;
        main.assets.prefs.putString("tools", tools);

        toolInUse = GameEngine.player.actions.currentTool;


        main.assets.prefs.putString("toolInUse", toolInUse);

        main.assets.prefs.flush();
    }

    public void load(){
        playerPos = main.assets.prefs.getString("playerPos");
        playerHealth = main.assets.prefs.getFloat("playerHealth");
        playerEnergy = main.assets.prefs.getString("playerEnergy");
        playerHasPet = main.assets.prefs.getBoolean("playerHasPet");
        healedBears = main.assets.prefs.getString("healedBears");
        wildlifeCleansed = main.assets.prefs.getInteger("wildlifeCleansed");
        torchesLit = main.assets.prefs.getInteger("torchesLit");
        litTorches = main.assets.prefs.getString("litTorches");
        altarsCleansed = main.assets.prefs.getInteger("altarsCleansed");
        story = main.assets.prefs.getInteger("story");
        if(story == 0){
            story = 1;
        }
        healedAltars = main.assets.prefs.getString("healedAltars");
        tools = main.assets.prefs.getString("tools");
        toolInUse = main.assets.prefs.getString("toolInUse");




    }


    public void initBox2DWorld(Lightning lightning){
        world = new World(new Vector2(0, Constants.GRAVITY), true);

        lightning.initRayhandler();

        createEnvironment(lightning);
        createTorches(lightning);
        spawnPlayer(lightning);
        spawnMage();

        spawnBears();
        spawnKingfishers();

        createSunStones(lightning);
        createHealthPotions();
        createLavaPlatforms();

        createAltars(lightning);
        createWater();
        if(!tools.contains("hook")){
            spawnHookTreasure();
        }
        if(!tools.contains("pogo")) {
            spawnPogoTreasure();
        }
    }

    public void spawnPlayer(Lightning lightning){

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_PLAYER).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            if(story < 2) {
                player_spawn_x = rect.getX() / Constants.PPM;
                player_spawn_y = rect.getY() / Constants.PPM;
                GameEngine.player = new Player(main, player_spawn_x, player_spawn_y, lightning, 0, 100, "", "");
            }else {
                float x = Float.parseFloat(playerPos.split(",")[0]);
                float y = Float.parseFloat(playerPos.split(",")[1]);
                int energy = Integer.parseInt(playerEnergy);
                float health = playerHealth;
                String tool = toolInUse;
                String toolsAvailable = tools;
                player_spawn_x = x ;
                player_spawn_y = y ;
                GameEngine.player = new Player(main, player_spawn_x, player_spawn_y, lightning, energy, health, tool, toolsAvailable);
            }

        }



    }

    public void spawnMage(){

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_MAGE).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            GameEngine.mage = new Mage(main, rect.getX()/Constants.PPM, rect.getY() / Constants.PPM, story);

        }

    }

    public void createLavaPlatforms(){

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_LAVAPLATFORMS).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            lavaPlatformList.add(new LavaPlatform(main, rect.getX()/Constants.PPM, rect.getY() / Constants.PPM));

        }

    }

    public void spawnHookTreasure(){

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_HOOKTREASURE).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            hookTreasure = new Hook(main, rect.getX()/Constants.PPM, rect.getY() / Constants.PPM);

        }

    }

    public void spawnPogoTreasure(){

        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_POGOTREASURE).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            pogoTreasure = new Pogo(main, rect.getX()/Constants.PPM, rect.getY() / Constants.PPM);

        }

    }

    public void createEnvironment(Lightning lightning){
        grounds = new Grounds(main);
        grounds.createGrounds();
        walls = new Walls(main);
        walls.createWalls();
        polylines = new Polylines(main);
        polylines.createPolylines();
        ramps = new Ramps(main);
        slides = new Slides(main);




    }

    public void createTorches(Lightning lightning){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_TORCHES).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();


            String[] torchesLitArr = litTorches.split(";");
            String currentTorch = (int) rect.getX() + "," + (int) rect.getY();
            boolean alreadyLit = false;

            for(String s : torchesLitArr){
                if(s.equals(currentTorch)){
                    alreadyLit = true;
                }
            }
            if(alreadyLit) {
                torchList.add(new Torch(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, lightning, true, (int) rect.getX(), (int) rect.getY()));
            }else{
                torchList.add(new Torch(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, lightning, false, (int) rect.getX(), (int) rect.getY()));

            }
        }
    }

    public void createAltars(Lightning lightning){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_ALTARS).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();



            String[] healedAltarsArr = healedAltars.split(";");
            String currentAltar = (int) rect.getX() + "," + (int) rect.getY();
            boolean alreadyHealed = false;

            for(String s : healedAltarsArr){
                if(s.equals(currentAltar)){
                    alreadyHealed = true;
                }
            }
            if(!alreadyHealed) {
                altarList.add(new Altar(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, lightning, false, (int) rect.getX(), (int) rect.getY()) );
            }else{
                altarList.add(new Altar(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, lightning, true, (int) rect.getX(), (int) rect.getY()));

            }
        }
    }

    public void createSunStones(Lightning lightning){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_SUNSTONES).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            //Gdx.app.log(TAG, "spawn SunStone at " + (int) rect.getX() + "," + (int) rect.getY() + ";");
            sunstoneList.add(new SunStone(main, rect.getX() / Constants.PPM, rect.getY() / Constants.PPM, lightning));


        }
    }

    public void createHealthPotions(){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_HEALTHPOTION).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            healthPotionList.add(new HealthPotion(main, rect.getX() / Constants.PPM, rect.getY() / Constants.PPM));

        }
    }

    public void spawnBears(){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_BEARS).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            //Gdx.app.log(TAG, "spawn Bear at " + (int) rect.getX() + "," + (int) rect.getY() + ";");
            String[] healedBearsArr = healedBears.split(";");
            String currentBear = (int) rect.getX() + "," + (int) rect.getY();
            boolean alreadyHealed = false;

            for(String s : healedBearsArr){
                if(s.equals(currentBear)){
                    alreadyHealed = true;
                }
            }
            if(!alreadyHealed) {
                bearList.add(new Bear(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, false, (int) rect.getX(), (int) rect.getY()));
            }else{
                bearList.add(new Bear(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, true, (int) rect.getX(), (int) rect.getY()));

            }

        }
    }

    public void spawnKingfishers(){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_KINGFISHER).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            boolean hasPet = playerHasPet;
            if(hasPet){
                kingfisherList.add(new Kingfisher(main, player_spawn_x, player_spawn_y, false));
            }else{
                kingfisherList.add(new Kingfisher(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, true));
            }

        }
    }

    public void createWater(){
        for (MapObject object : GameEngine.mapLoader.gameWorld.getLayers().get(Constants.LAYER_WATER).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            waterList.add(new Water(main, rect.getX()/ Constants.PPM, rect.getY() / Constants.PPM, rect.getWidth()/Constants.PPM, rect.getHeight()/Constants.PPM));


        }
    }


    public void update(float delta){
        world.step(1 / 60f, 6, 2);

    }



}
