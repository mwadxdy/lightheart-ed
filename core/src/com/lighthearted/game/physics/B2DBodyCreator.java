package com.lighthearted.game.physics;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;
import com.lighthearted.utils.Constants;

public class B2DBodyCreator   {
    private static final String TAG = B2DBodyCreator.class.getName();

    private LightheartEd main;

    public B2DBodyCreator(LightheartEd main){
        this.main = main;
    }

    public static Body createBox(float x, float y,
                          float w, float h,
                          BodyDef.BodyType bodyType,
                          float friction, boolean isSensor, float density, float resti,
                          short category, short mask,
                          Object userData){

        Body body;
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();

        bdef.type = bodyType;
        bdef.position.set(x,y);
        body = B2DWorldCreator.world.createBody(bdef);
        body.setUserData(userData);
        shape.setAsBox(w, h);

        fdef.shape = shape;
        fdef.isSensor = isSensor;
        fdef.friction = friction;
        fdef.restitution = resti;
        if(resti > 0) {
            fdef.restitution = resti;
        }

        if(density > 0) {
            fdef.density = density;
        }
        fdef.filter.categoryBits = category;
        fdef.filter.maskBits = mask;
        body.createFixture(fdef);


        return body;
    }

    public static Body createCircle(float x, float y,
                                 float rad,
                                 BodyDef.BodyType bodyType,
                                 float friction, boolean isSensor, float density,
                                 short category, short mask,
                                 Object userData){

        Body body;
        BodyDef bdef = new BodyDef();
        CircleShape shape = new CircleShape();

        FixtureDef fdef = new FixtureDef();

        bdef.type = bodyType;
        bdef.position.set(x,y);


        body = B2DWorldCreator.world.createBody(bdef);
        body.setUserData(userData);

        shape.setRadius(rad);

        fdef.shape = shape;
        fdef.friction = friction;
        fdef.density = density;
        fdef.filter.categoryBits = category;
        fdef.filter.maskBits = mask;
        fdef.isSensor = isSensor;
        body.createFixture(fdef);


        return body;
    }
}
