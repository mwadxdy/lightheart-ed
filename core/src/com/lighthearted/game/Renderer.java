package com.lighthearted.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.entities.mage.strategies.MageRenderer;
import com.lighthearted.game.entities.player.strategies.PlayerRenderer;
import com.lighthearted.game.hud.StageRenderer;
import com.lighthearted.game.map.MapRenderer;
import com.lighthearted.game.physics.B2DWorldCreator;

public  class Renderer   {
    private static final String TAG = Renderer.class.getName();
    
    private LightheartEd main;

    public MapRenderer mapRenderer;

    private StageRenderer stageRenderer;

    private Box2DDebugRenderer b2dRenderer;

    public Renderer(LightheartEd main){
        this.main = main;
        mapRenderer = new MapRenderer(main);
        stageRenderer = new StageRenderer(main);

        b2dRenderer = new Box2DDebugRenderer();

    }

    public void render(float delta){
        Gdx.gl.glClearColor(181 / 255f, 228 / 255f, 250 / 255f, 0.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render(delta);

        //HUD
        stageRenderer.render(delta);

        //stageRenderer.debug();

        //b2dRenderer.render(B2DWorldCreator.world, GameEngine.camHandler.gameCam.combined);
        //stageRenderer.renderRects();
    }
}
