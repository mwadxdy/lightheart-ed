package com.lighthearted;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.lighthearted.screens.GameScreen;
import com.lighthearted.screens.LoadingScreen;
import com.lighthearted.screens.MenuScreen;
import com.lighthearted.utils.Assets;


//the applications main entry point
//extends Game -> @doc This allows an application to easily have multiple screens.

public class LightheartEd extends Game {
    //create a string containing the class path (for logging)
    private static final String TAG = LightheartEd.class.getName();

    //the spritebatch holds all the sprites, it's like a sheet of paper where everything is drawn onto
    public SpriteBatch batch;
    public Assets assets;

    public int test = 5;

    @Override
    public void create () {
        Gdx.app.log(TAG, "create()");
        //init spritebatch
        batch = new SpriteBatch();
        assets = new Assets(this);
        //load gamescreen (later replaced by menu screen)
        setScreen(new MenuScreen(this));
    }
}
