package com.lighthearted.utils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.lighthearted.LightheartEd;

import java.text.DecimalFormat;

public class Assets   {
    private static final String TAG = Assets.class.getName();

    private LightheartEd main;
    public Skin skinMenuUI, skinGameUI;
    public TextureAtlas gameObjectAtlas;
    public TextureAtlas animalAtlas;
    public TextureAtlas mageAtlas;
    public TextureAtlas playerAtlas;
    public Preferences prefs;

    public boolean isSoundActivated  = false;
    public Music backgroundMusic, menuMusic;
    public Sound jumpSound, pickUpHeartSound, pickUpKeySound, changeFormSound, openCageSound;

    public Animation<TextureRegion> bearIdleRightAnim, bearIdleLeftAnim,
            bearWalkRightAnim, bearWalkLeftAnim,
            bearAttackRightAnim, bearAttackLeftAnim;

    public Animation<TextureRegion> bearIdleDarkRightAnim, bearIdleDarkLeftAnim,
            bearWalkDarkRightAnim, bearWalkDarkLeftAnim,
            bearAttackDarkRightAnim, bearAttackDarkLeftAnim;

    public Animation<TextureRegion> kingfisherAnimRight, kingfisherAnimLeft;


    public Animation<TextureRegion> lavaPlatformAnim;
    public Animation<TextureRegion> healthPotionAnim;


    public Animation mageRightAnim, mageLeftAnim, mageIdleAnim;

    public Animation<TextureRegion> idleRightAnim, idleLeftAnim,
            runRightAnim, runLeftAnim,
            jumpUpRightAnim, jumpUpLeftAnim,
            fallDownRightAnim, fallDownLeftAnim,
            standingFFCanonRightAnim, standingFFCanonLeftAnim,
            runningFFCanonRightAnim, runningFFCanonLeftAnim,
            standingFFCanonTurnedRightAnim, standingFFCanonTurnedLeftAnim,
            runningFFCanonTurnedRightAnim, runningFFCanonTurnedLeftAnim,
            pogoRightAnim, pogoLeftAnim;
    //FONTS
    public BitmapFont font, fontSmall, toolMenuPanelFont, scoreFontSmallSmall, scoreFontSmallMedium, scoreFontSmallBig,
            scoreFontMediumSmall, scoreFontMediumMedium, scoreFontMediumBig,
            scoreFontBigSmall, scoreFontBigMedium, scoreFontBigBig,
            fontBig, fontBigRed, countdownFont, scoreFont, pauseFont, nicknameFont, speakBubbleFont, speakBubbleFontS;

    public BitmapFont pressStartFont;


    public DecimalFormat timerFormat = new DecimalFormat("#.#");
    public DecimalFormat integerFormat = new DecimalFormat("#");



    public Assets(LightheartEd main){
        this.main = main;
        loadFonts();
        loadPrefs();
        loadSound();
        skinGameUI = new Skin(Gdx.files.internal(Constants.GAME_UI_JSON), new TextureAtlas(Constants.GAME_UI_TEXTURE));
        skinMenuUI = new Skin(Gdx.files.internal(Constants.MENU_UI_JSON), new TextureAtlas(Constants.MENU_UI_TEXTURE));

        gameObjectAtlas = new TextureAtlas(Constants.GAME_OBJECTS);
        animalAtlas = new TextureAtlas(Gdx.files.internal(Constants.ANIMALS));
        playerAtlas = new TextureAtlas(Gdx.files.internal(Constants.PLAYER));
        mageAtlas = new TextureAtlas(Gdx.files.internal(Constants.MAGE));

        loadPlayerAnimation();
        loadBearAnimation();
        loadKingfisherAnimation();
        loadMageAnimation();
        loadLavaPlatformAnim();
        loadHealthPotionAnim();

    }

    public void loadPrefs(){
        prefs = Gdx.app.getPreferences("prefs");

        //prefs.clear();prefs.flush();

    }


    public void update(float delta){
    
    }


    public void loadFonts() {
        font = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        font.getData().scale(1f);
        font.setColor(Color.WHITE);

        fontSmall = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        fontSmall.getData().setScale(0.9f, 0.9f);
        fontSmall.setUseIntegerPositions(false);
        fontSmall.setColor(Color.WHITE);

        pressStartFont = new BitmapFont(Gdx.files.internal(Constants.PRESSSTART_FNT), Gdx.files.internal(Constants.PRESSSTART_PNG), false);
        pressStartFont.getData().setScale(0.2f, 0.2f);
        pressStartFont.setUseIntegerPositions(false);
        pressStartFont.setColor(Color.ORANGE);


        //UI FONTS

        toolMenuPanelFont = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        toolMenuPanelFont.getData().setScale(0.6f, 0.6f);
        toolMenuPanelFont.setColor(Color.VIOLET);


        //SCORE FONTS

        scoreFontSmallSmall = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontSmallSmall.getData().setScale(0.02357f, 0.02357f);
        scoreFontSmallSmall.setColor(Color.BLACK);


        scoreFontSmallMedium = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontSmallMedium.getData().setScale(0.03357f, 0.03357f);
        scoreFontSmallMedium.setColor(Color.BLACK);


        scoreFontSmallBig = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontSmallBig.getData().setScale(0.04357f, 0.04357f);
        scoreFontSmallBig.setColor(Color.BLACK);


        scoreFontMediumSmall = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontMediumSmall.getData().setScale(0.06157f, 0.06157f);
        scoreFontMediumSmall.setColor(Color.BLACK);

        scoreFontMediumMedium = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontMediumMedium.getData().setScale(0.07157f, 0.07157f);
        scoreFontMediumMedium.setColor(Color.BLACK);

        scoreFontMediumBig = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontMediumBig.getData().setScale(0.08157f, 0.08157f);
        scoreFontMediumBig.setColor(Color.BLACK);

        scoreFontBigSmall = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontBigSmall.getData().setScale(0.09757f, 0.09757f);
        scoreFontBigSmall.setColor(Color.BLACK);

        scoreFontBigMedium = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontBigMedium.getData().setScale(0.11657f, 0.11657f);
        scoreFontBigMedium.setColor(Color.BLACK);

        scoreFontBigBig = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        scoreFontBigBig.getData().setScale(0.12757f, 0.12757f);
        scoreFontBigBig.setColor(Color.BLACK);


        fontBig = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        fontBig.getData().setScale(2f, 2f);
        fontBig.setColor(Color.WHITE);

        fontBigRed = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        fontBigRed.getData().setScale(3f, 3f);
        fontBigRed.setColor(Color.RED);


        countdownFont = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        countdownFont.getData().setScale(6);
        countdownFont.setColor(Color.BLACK);

        scoreFont = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        scoreFont.getData().setScale(0.95f, 0.95f);
        scoreFont.setColor(Color.BLACK);

        pauseFont = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        pauseFont.getData().setScale(3f, 3f);
        pauseFont.setColor(Color.BLACK);

        nicknameFont = new BitmapFont(Gdx.files.internal(Constants.FONT_FNT), Gdx.files.internal(Constants.FONT_PNG), false);
        nicknameFont.getData().setScale(1f, 1f);
        nicknameFont.setColor(Color.FOREST);

        /*FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(Constants.FONT_MR_TT));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 10;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.color = Color.BLACK;
        speakBubbleFont = generator.generateFont(parameter);*/

        speakBubbleFont = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        speakBubbleFont.getData().setScale(0.02f, 0.02f);
        speakBubbleFont.setUseIntegerPositions(false);
        speakBubbleFont.setColor(Color.BLACK);

        speakBubbleFontS = new BitmapFont(Gdx.files.internal(Constants.FONTS_FNT), Gdx.files.internal(Constants.FONTS_PNG), false);
        speakBubbleFontS.getData().setScale(0.015f, 0.015f);
        speakBubbleFontS.setUseIntegerPositions(false);
        speakBubbleFontS.setColor(Color.BLACK);


    }



    public void loadSound() {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/White Mind NES.mp3"));
        menuMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/MenuLoop.ogg"));

        jumpSound = Gdx.audio.newSound(Gdx.files.internal("sounds/Jump/Jump.wav"));

        pickUpHeartSound = Gdx.audio.newSound(Gdx.files.internal("sounds/PickUps/PickUpHeart.wav"));
        pickUpKeySound = Gdx.audio.newSound(Gdx.files.internal("sounds/PickUps/PickUpKey.wav"));

        changeFormSound = Gdx.audio.newSound(Gdx.files.internal("sounds/PowerUps/ChangeForm.wav"));
        openCageSound = Gdx.audio.newSound(Gdx.files.internal("sounds/PowerUps/OpenChest.wav"));


    }

    public void loadBearAnimation() {

        TextureRegion idleRight1, idleRight2, idleLeft1, idleLeft2;
        TextureRegion idleDarkRight1, idleDarkRight2, idleDarkLeft1, idleDarkLeft2;
        TextureRegion walkRight1, walkRight2, walkRight3, walkLeft1, walkLeft2, walkLeft3;
        TextureRegion walkDarkRight1, walkDarkRight2, walkDarkRight3, walkDarkLeft1, walkDarkLeft2, walkDarkLeft3;
        TextureRegion attackRight1, attackRight2, attackRight3, attackLeft1, attackLeft2, attackLeft3;
        TextureRegion attackDarkRight1, attackDarkRight2, attackDarkRight3, attackDarkLeft1, attackDarkLeft2, attackDarkLeft3;

        //IDLE

        idleRight1 = animalAtlas.findRegion("BearIdle0");
        idleRight2 = animalAtlas.findRegion("BearIdle1");
        idleDarkRight1 = animalAtlas.findRegion("BearIdleDark0");
        idleDarkRight2 = animalAtlas.findRegion("BearIdleDark1");


        TextureRegion[] idleRightArr = {idleRight1, idleRight2};
        bearIdleRightAnim = new Animation<TextureRegion>(Constants.BEAR_IDLE_ANIMATION_SPEED, idleRightArr);
        bearIdleRightAnim.setPlayMode(Animation.PlayMode.LOOP);
        TextureRegion[] idleDarkRightArr = {idleDarkRight1, idleDarkRight2};
        bearIdleDarkRightAnim = new Animation<TextureRegion>(Constants.BEAR_IDLE_ANIMATION_SPEED, idleDarkRightArr);
        bearIdleDarkRightAnim.setPlayMode(Animation.PlayMode.LOOP);


        idleLeft1 = new TextureRegion(idleRight1);
        idleLeft1.flip(true, false);
        idleLeft2 = new TextureRegion(idleRight2);
        idleLeft2.flip(true, false);
        idleDarkLeft1 = new TextureRegion(idleDarkRight1);
        idleDarkLeft1.flip(true, false);
        idleDarkLeft2 = new TextureRegion(idleDarkRight2);
        idleDarkLeft2.flip(true, false);

        TextureRegion[] idleLeftArr = {idleLeft1, idleLeft2};
        bearIdleLeftAnim = new Animation<TextureRegion>(Constants.BEAR_IDLE_ANIMATION_SPEED, idleLeftArr);
        bearIdleLeftAnim.setPlayMode(Animation.PlayMode.LOOP);
        TextureRegion[] idleDarkLeftArr = {idleDarkLeft1, idleDarkLeft2};
        bearIdleDarkLeftAnim = new Animation<TextureRegion>(Constants.BEAR_IDLE_ANIMATION_SPEED, idleDarkLeftArr);
        bearIdleDarkLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //RUN

        walkRight1 = animalAtlas.findRegion("BearWalk0");
        walkRight2 = animalAtlas.findRegion("BearWalk1");
        walkRight3 = animalAtlas.findRegion("BearWalk2");

        walkDarkRight1 = animalAtlas.findRegion("BearWalkDark0");
        walkDarkRight2 = animalAtlas.findRegion("BearWalkDark1");
        walkDarkRight3 = animalAtlas.findRegion("BearWalkDark2");

        TextureRegion[] walkRightArr = {walkRight1, walkRight2, walkRight3};
        bearWalkRightAnim = new Animation<TextureRegion>(Constants.BEAR_WALK_ANIMATION_SPEED, walkRightArr);
        bearWalkRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] walkDarkRightArr = {walkDarkRight1, walkDarkRight2, walkDarkRight3};
        bearWalkDarkRightAnim = new Animation<TextureRegion>(Constants.BEAR_WALK_ANIMATION_SPEED, walkDarkRightArr);
        bearWalkDarkRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        walkLeft1 = new TextureRegion(walkRight1);
        walkLeft1.flip(true, false);
        walkLeft2 = new TextureRegion(walkRight2);
        walkLeft2.flip(true, false);
        walkLeft3 = new TextureRegion(walkRight3);
        walkLeft3.flip(true, false);

        walkDarkLeft1 = new TextureRegion(walkDarkRight1);
        walkDarkLeft1.flip(true, false);
        walkDarkLeft2 = new TextureRegion(walkDarkRight2);
        walkDarkLeft2.flip(true, false);
        walkDarkLeft3 = new TextureRegion(walkDarkRight3);
        walkDarkLeft3.flip(true, false);


        TextureRegion[] walkLeftArr = {walkLeft1, walkLeft2, walkLeft3};
        bearWalkLeftAnim = new Animation<TextureRegion>(Constants.BEAR_WALK_ANIMATION_SPEED, walkLeftArr);
        bearWalkLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] walkDarkLeftArr = {walkDarkLeft1, walkDarkLeft2, walkDarkLeft3};
        bearWalkDarkLeftAnim = new Animation<TextureRegion>(Constants.BEAR_WALK_ANIMATION_SPEED, walkDarkLeftArr);
        bearWalkDarkLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //ATTACK

        attackRight1 = animalAtlas.findRegion("BearAttack0");
        attackRight2 = animalAtlas.findRegion("BearAttack1");
        attackRight3 = animalAtlas.findRegion("BearAttack2");

        attackDarkRight1 = animalAtlas.findRegion("BearAttackDark0");
        attackDarkRight2 = animalAtlas.findRegion("BearAttackDark1");
        attackDarkRight3 = animalAtlas.findRegion("BearAttackDark2");

        TextureRegion[] attackRightArr = {attackRight1, attackRight2, attackRight3};
        bearAttackRightAnim = new Animation<TextureRegion>(Constants.BEAR_ATTACK_ANIMATION_SPEED, attackRightArr);
        bearAttackRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] attackDarkRightArr = {attackDarkRight1, attackDarkRight2, attackDarkRight3};
        bearAttackDarkRightAnim = new Animation<TextureRegion>(Constants.BEAR_ATTACK_ANIMATION_SPEED, attackDarkRightArr);
        bearAttackDarkRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        attackLeft1 = new TextureRegion(attackRight1);
        attackLeft1.flip(true, false);
        attackLeft2 = new TextureRegion(attackRight2);
        attackLeft2.flip(true, false);
        attackLeft3 = new TextureRegion(attackRight3);
        attackLeft3.flip(true, false);

        attackDarkLeft1 = new TextureRegion(attackDarkRight1);
        attackDarkLeft1.flip(true, false);
        attackDarkLeft2 = new TextureRegion(attackDarkRight2);
        attackDarkLeft2.flip(true, false);
        attackDarkLeft3 = new TextureRegion(attackDarkRight3);
        attackDarkLeft3.flip(true, false);


        TextureRegion[] attackLeftArr = {attackLeft1, attackLeft2, attackLeft3};
        bearAttackLeftAnim = new Animation<TextureRegion>(Constants.BEAR_ATTACK_ANIMATION_SPEED, attackLeftArr);
        bearAttackLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] attackDarkLeftArr = {attackDarkLeft1, attackDarkLeft2, attackDarkLeft3};
        bearAttackDarkLeftAnim = new Animation<TextureRegion>(Constants.BEAR_ATTACK_ANIMATION_SPEED, attackDarkLeftArr);
        bearAttackDarkLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

    }


    public void loadKingfisherAnimation() {

        TextureRegion kingfisherRight1, kingfisherRight2, kingfisherLeft1, kingfisherLeft2;

        kingfisherRight1 = animalAtlas.findRegion("Kingfisher0");
        kingfisherRight2 = animalAtlas.findRegion("Kingfisher1");

        TextureRegion[] kingfisherRightArr = {kingfisherRight1, kingfisherRight2};
        kingfisherAnimRight = new Animation<TextureRegion>(0.1f, kingfisherRightArr);
        kingfisherAnimRight.setPlayMode(Animation.PlayMode.LOOP);

        kingfisherLeft1 = new TextureRegion(kingfisherRight1);
        kingfisherLeft1.flip(true, false);
        kingfisherLeft2 = new TextureRegion(kingfisherRight2);
        kingfisherLeft2.flip(true, false);

        TextureRegion[] kingfisherLeftArr = {kingfisherLeft1, kingfisherLeft2};
        kingfisherAnimLeft = new Animation<TextureRegion>(0.1f, kingfisherLeftArr);
        kingfisherAnimLeft.setPlayMode(Animation.PlayMode.LOOP);
    }

    public void loadMageAnimation() {


        //IDLE
        TextureRegion idle1, idle2;

        idle1 = mageAtlas.findRegion("MageIdle0");
        idle2 = mageAtlas.findRegion("MageIdle1");


        TextureRegion[] idleArr = {idle1, idle2};

        mageIdleAnim = new Animation<TextureRegion>(Constants.MAGE_IDLE_ANIMATION_SPEED, idleArr);
        mageIdleAnim.setPlayMode(Animation.PlayMode.LOOP);


        //WALK
        TextureRegion walkRight1, walkRight2;
        TextureRegion walkLeft1, walkLeft2;

        walkRight1 = mageAtlas.findRegion("MageWalk0");
        walkRight2 = mageAtlas.findRegion("MageWalk1");


        TextureRegion[] walkRightArr = {walkRight1, walkRight2};

        mageRightAnim = new Animation<TextureRegion>(Constants.MAGE_WALK_ANIMATION_SPEED, walkRightArr);
        mageRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        walkLeft1 = new TextureRegion(walkRight1);
        walkLeft1.flip(true, false);

        walkLeft2 = new TextureRegion(walkRight2);
        walkLeft2.flip(true, false);


        TextureRegion[] walkLeftArr = {walkLeft1, walkLeft2};
        mageLeftAnim = new Animation<TextureRegion>(Constants.MAGE_WALK_ANIMATION_SPEED, walkLeftArr);
        mageLeftAnim.setPlayMode(Animation.PlayMode.LOOP);
    }

    public void loadLavaPlatformAnim() {

        TextureRegion platform1, platform2, platform3;

        platform1 = gameObjectAtlas.findRegion("LavaPlatform0");
        platform2 = gameObjectAtlas.findRegion("LavaPlatform1");
        platform3 = gameObjectAtlas.findRegion("LavaPlatform2");


        TextureRegion[] platformArr = {platform1, platform2, platform3};
        lavaPlatformAnim = new Animation<TextureRegion>(0.3f, platformArr);
        lavaPlatformAnim.setPlayMode(Animation.PlayMode.LOOP);
    }

    public void loadHealthPotionAnim() {

        TextureRegion healthpotion1, healthpotion2, healthpotion3;

        healthpotion1 = gameObjectAtlas.findRegion("HealthPotion0");
        healthpotion2 = gameObjectAtlas.findRegion("HealthPotion1");
        healthpotion3 = gameObjectAtlas.findRegion("HealthPotion2");


        TextureRegion[] healthPotionArr = {healthpotion1, healthpotion2, healthpotion3};
        healthPotionAnim = new Animation<TextureRegion>(0.3f, healthPotionArr);
        healthPotionAnim.setPlayMode(Animation.PlayMode.LOOP);
    }


    public void loadPlayerAnimation() {


        TextureRegion idleRight1, idleRight2, idleLeft1, idleLeft2;
        TextureRegion runRight1, runRight2, runRight3, runRight4, runLeft1, runLeft2, runLeft3, runLeft4;
        TextureRegion jumpUpRight1, jumpUpRight2, jumpUpLeft1, jumpUpLeft2;
        TextureRegion fallDownRight1, fallDownRight2, fallDownLeft1, fallDownLeft2;
        TextureRegion standingFFCanonRight1, standingFFCanonRight2;
        TextureRegion standingFFCanonLeft1, standingFFCanonLeft2;
        TextureRegion runningFFCanonRight1, runningFFCanonRight2, runningFFCanonRight3, runningFFCanonRight4;
        TextureRegion runningFFCanonLeft1, runningFFCanonLeft2, runningFFCanonLeft3, runningFFCanonLeft4;
        TextureRegion standingFFCanonTurnedRight1, standingFFCanonTurnedRight2;
        TextureRegion standingFFCanonTurnedLeft1, standingFFCanonTurnedLeft2;
        TextureRegion runningFFCanonTurnedRight1, runningFFCanonTurnedRight2, runningFFCanonTurnedRight3, runningFFCanonTurnedRight4;
        TextureRegion runningFFCanonTurnedLeft1, runningFFCanonTurnedLeft2, runningFFCanonTurnedLeft3, runningFFCanonTurnedLeft4;
        TextureRegion pogoRight1, pogoRight2, pogoRight3;
        TextureRegion pogoLeft1, pogoLeft2, pogoLeft3;

        //IDLE

        idleRight1 = playerAtlas.findRegion("PlayerIdle0");
        idleRight2 = playerAtlas.findRegion("PlayerIdle1");

        TextureRegion[] idleRightArr = {idleRight1, idleRight2};
        idleRightAnim = new Animation<TextureRegion>(Constants.PLAYER_IDLE_ANIMATION_SPEED, idleRightArr);
        idleRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        idleLeft1 = new TextureRegion(idleRight1);
        idleLeft1.flip(true, false);
        idleLeft2 = new TextureRegion(idleRight2);
        idleLeft2.flip(true, false);

        TextureRegion[] idleLeftArr = {idleLeft1, idleLeft2};
        idleLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_IDLE_ANIMATION_SPEED, idleLeftArr);
        idleLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //RUN

        runRight1 = playerAtlas.findRegion("PlayerRun0");
        runRight2 = playerAtlas.findRegion("PlayerRun1");
        runRight3 = playerAtlas.findRegion("PlayerRun2");
        runRight4 = playerAtlas.findRegion("PlayerRun3");

        TextureRegion[] runRightArr = {runRight1, runRight2, runRight3, runRight4};
        runRightAnim = new Animation<TextureRegion>(Constants.PLAYER_RUN_ANIMATION_SPEED, runRightArr);
        runRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        runLeft1 = new TextureRegion(runRight1);
        runLeft1.flip(true, false);
        runLeft2 = new TextureRegion(runRight2);
        runLeft2.flip(true, false);
        runLeft3 = new TextureRegion(runRight3);
        runLeft3.flip(true, false);
        runLeft4 = new TextureRegion(runRight4);
        runLeft4.flip(true, false);

        TextureRegion[] runLeftArr = {runLeft1, runLeft2, runLeft3, runLeft4};
        runLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_RUN_ANIMATION_SPEED, runLeftArr);
        runLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //JUMP UP

        jumpUpRight1 = playerAtlas.findRegion("PlayerJumpUp0");
        jumpUpRight2 = playerAtlas.findRegion("PlayerJumpUp1");

        TextureRegion[] jumpUpRightArr = {jumpUpRight1, jumpUpRight2};
        jumpUpRightAnim = new Animation<TextureRegion>(Constants.PLAYER_JUMP_UP__ANIMATION_SPEED, jumpUpRightArr);
        jumpUpRightAnim.setPlayMode(Animation.PlayMode.NORMAL);

        jumpUpLeft1 = new TextureRegion(jumpUpRight1);
        jumpUpLeft1.flip(true, false);
        jumpUpLeft2 = new TextureRegion(jumpUpRight2);
        jumpUpLeft2.flip(true, false);

        TextureRegion[] jumpUpLeftArr = {jumpUpLeft1, jumpUpLeft2};
        jumpUpLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_JUMP_UP__ANIMATION_SPEED, jumpUpLeftArr);
        jumpUpLeftAnim.setPlayMode(Animation.PlayMode.NORMAL);

        //FALL DOWN

        fallDownRight1 = playerAtlas.findRegion("PlayerFallDown0");
        fallDownRight2 = playerAtlas.findRegion("PlayerFallDown1");

        TextureRegion[] fallDownRightArr = {fallDownRight1, fallDownRight2};
        fallDownRightAnim = new Animation<TextureRegion>(Constants.PLAYER_FALL_DOWN__ANIMATION_SPEED, fallDownRightArr);
        fallDownRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        fallDownLeft1 = new TextureRegion(fallDownRight1);
        fallDownLeft1.flip(true, false);
        fallDownLeft2 = new TextureRegion(fallDownRight2);
        fallDownLeft2.flip(true, false);

        TextureRegion[] fallDownLeftArr = {fallDownLeft1, fallDownLeft2};
        fallDownLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_FALL_DOWN__ANIMATION_SPEED, fallDownLeftArr);
        fallDownLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //STANDING FF CANON

        standingFFCanonRight1 = playerAtlas.findRegion("PlayerStandingFF0");
        standingFFCanonRight2 = playerAtlas.findRegion("PlayerStandingFF1");

        TextureRegion[] standingFFCanonRightArr = {standingFFCanonRight1, standingFFCanonRight2};
        standingFFCanonRightAnim = new Animation<TextureRegion>(Constants.PLAYER_STANDING_FF_ANIMATION_SPEED, standingFFCanonRightArr);
        standingFFCanonRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        standingFFCanonLeft1 = new TextureRegion(standingFFCanonRight1);
        standingFFCanonLeft1.flip(true, false);
        standingFFCanonLeft2 = new TextureRegion(standingFFCanonRight2);
        standingFFCanonLeft2.flip(true, false);

        TextureRegion[] standingFFCanonLeftArr = {standingFFCanonLeft1, standingFFCanonLeft2};
        standingFFCanonLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_STANDING_FF_ANIMATION_SPEED, standingFFCanonLeftArr);
        standingFFCanonLeftAnim.setPlayMode(Animation.PlayMode.LOOP);


        //RUNNING FF CANNON

        runningFFCanonRight1 = playerAtlas.findRegion("PlayerRunningFF0");
        runningFFCanonRight2 = playerAtlas.findRegion("PlayerRunningFF1");
        runningFFCanonRight3 = playerAtlas.findRegion("PlayerRunningFF2");
        runningFFCanonRight4 = playerAtlas.findRegion("PlayerRunningFF3");

        TextureRegion[] runningFFCanonRightArr = {runningFFCanonRight1, runningFFCanonRight2, runningFFCanonRight3, runningFFCanonRight4};
        runningFFCanonRightAnim = new Animation<TextureRegion>(Constants.PLAYER_RUNNING_FF_ANIMATION_SPEED, runningFFCanonRightArr);
        runningFFCanonRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        runningFFCanonLeft1 = new TextureRegion(runningFFCanonRight1);
        runningFFCanonLeft1.flip(true, false);
        runningFFCanonLeft2 = new TextureRegion(runningFFCanonRight2);
        runningFFCanonLeft2.flip(true, false);
        runningFFCanonLeft3 = new TextureRegion(runningFFCanonRight3);
        runningFFCanonLeft3.flip(true, false);
        runningFFCanonLeft4 = new TextureRegion(runningFFCanonRight4);
        runningFFCanonLeft4.flip(true, false);

        TextureRegion[] runningFFCanonLeftArr = {runningFFCanonLeft1, runningFFCanonLeft2, runningFFCanonLeft3, runningFFCanonLeft4};
        runningFFCanonLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_RUNNING_FF_ANIMATION_SPEED, runningFFCanonLeftArr);
        runningFFCanonLeftAnim.setPlayMode(Animation.PlayMode.LOOP);

        //STANDING FF CANON TURNED

        standingFFCanonTurnedRight1 = playerAtlas.findRegion("PlayerStandingFFTurned0");
        standingFFCanonTurnedRight2 = playerAtlas.findRegion("PlayerStandingFFTurned1");

        TextureRegion[] standingFFCanonTurnedRightArr = {standingFFCanonTurnedRight1, standingFFCanonTurnedRight2};
        standingFFCanonTurnedRightAnim = new Animation<TextureRegion>(Constants.PLAYER_STANDING_FF_ANIMATION_SPEED, standingFFCanonTurnedRightArr);
        standingFFCanonTurnedRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        standingFFCanonTurnedLeft1 = new TextureRegion(standingFFCanonTurnedRight1);
        standingFFCanonTurnedLeft1.flip(true, false);
        standingFFCanonTurnedLeft2 = new TextureRegion(standingFFCanonTurnedRight2);
        standingFFCanonTurnedLeft2.flip(true, false);

        TextureRegion[] standingFFCanonTurnedLeftArr = {standingFFCanonTurnedLeft1, standingFFCanonTurnedLeft2};
        standingFFCanonTurnedLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_STANDING_FF_ANIMATION_SPEED, standingFFCanonTurnedLeftArr);
        standingFFCanonTurnedLeftAnim.setPlayMode(Animation.PlayMode.LOOP);


        //RUNNING FF CANNON TURNED

        runningFFCanonTurnedRight1 = playerAtlas.findRegion("PlayerRunningFFTurned0");
        runningFFCanonTurnedRight2 = playerAtlas.findRegion("PlayerRunningFFTurned1");
        runningFFCanonTurnedRight3 = playerAtlas.findRegion("PlayerRunningFFTurned2");
        runningFFCanonTurnedRight4 = playerAtlas.findRegion("PlayerRunningFFTurned3");

        TextureRegion[] runningFFCanonTurnedRightArr = {runningFFCanonTurnedRight1, runningFFCanonTurnedRight2, runningFFCanonTurnedRight3, runningFFCanonTurnedRight4};
        runningFFCanonTurnedRightAnim = new Animation<TextureRegion>(Constants.PLAYER_RUNNING_FF_ANIMATION_SPEED, runningFFCanonTurnedRightArr);
        runningFFCanonTurnedRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        runningFFCanonTurnedLeft1 = new TextureRegion(runningFFCanonTurnedRight1);
        runningFFCanonTurnedLeft1.flip(true, false);
        runningFFCanonTurnedLeft2 = new TextureRegion(runningFFCanonTurnedRight2);
        runningFFCanonTurnedLeft2.flip(true, false);
        runningFFCanonTurnedLeft3 = new TextureRegion(runningFFCanonTurnedRight3);
        runningFFCanonTurnedLeft3.flip(true, false);
        runningFFCanonTurnedLeft4 = new TextureRegion(runningFFCanonTurnedRight4);
        runningFFCanonTurnedLeft4.flip(true, false);

        TextureRegion[] runningFFCanonTurnedLeftArr = {runningFFCanonTurnedLeft1, runningFFCanonTurnedLeft2, runningFFCanonTurnedLeft3, runningFFCanonTurnedLeft4};
        runningFFCanonTurnedLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_RUNNING_FF_ANIMATION_SPEED, runningFFCanonTurnedLeftArr);
        runningFFCanonTurnedLeftAnim.setPlayMode(Animation.PlayMode.LOOP);


        //POGO

        pogoRight1 = gameObjectAtlas.findRegion("Pogo0");
        pogoRight2 = gameObjectAtlas.findRegion("Pogo1");
        pogoRight3 = gameObjectAtlas.findRegion("Pogo2");

        TextureRegion[] pogoRightArr = {pogoRight1, pogoRight2, pogoRight3};
        pogoRightAnim = new Animation<TextureRegion>(Constants.PLAYER_POGO_ANIMATION_SPEED, pogoRightArr);
        pogoRightAnim.setPlayMode(Animation.PlayMode.LOOP);

        pogoLeft1 = new TextureRegion(pogoRight1);
        pogoLeft1.flip(true, false);
        pogoLeft2 = new TextureRegion(pogoRight2);
        pogoLeft2.flip(true, false);
        pogoLeft3 = new TextureRegion(pogoRight3);
        pogoLeft3.flip(true, false);


        TextureRegion[] pogoLeftArr = {pogoLeft1, pogoLeft2, pogoLeft3};
        pogoLeftAnim = new Animation<TextureRegion>(Constants.PLAYER_POGO_ANIMATION_SPEED, pogoLeftArr);
        pogoLeftAnim.setPlayMode(Animation.PlayMode.LOOP);



    }





}
