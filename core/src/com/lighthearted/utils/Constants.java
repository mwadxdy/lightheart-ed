package com.lighthearted.utils;

public class Constants {
    // PLAYER
    public static final String PLAYER = "sprites/player/player.txt";

    //MAGE
    public static final String MAGE = "sprites/mage/mage.txt";

    //ANIMALS
    public static final String ANIMALS = "sprites/animals/animals.txt";

    //BOX2D HITBOX
    public static final float PLAYER_HITBOX_WIDTH = 8;
    public static final float PLAYER_HITBOX_HEIGHT = 20;

    public static final float MAGE_HITBOX_WIDTH = 12;
    public static final float MAGE_HITBOX_HEIGHT = 35;

    public static final float BEAR_HITBOX_WIDTH = 27;
    public static final float BEAR_HITBOX_HEIGHT = 27;


    public static final int ALTAR_FRAME_NR = 10;



    //SPRITE
    public static final float PLAYER_SPRITE_WIDTH = 5.5f;
    public static final float PLAYER_SPRITE_HEIGHT = 5.5f;

    //SPRITE
    public static final float MAGE_SPRITE_WIDTH = 9.5f;
    public static final float MAGE_SPRITE_HEIGHT = 10.5f;

    public static final float BEAR_SPRITE_WIDTH = 15.5f;
    public static final float BEAR_SPRITE_HEIGHT = 15.5f;

    //MOVEMENT
    public static final float PLAYER_DEFAULT_JUMPFORCE = 37;
    public static final float PLAYER_DEFAULT_MAXSPEED = 18;
    public static final float MAGE_DEFAULT_MAXSPEED = 6;
    public static final int ROTATION_SPEED = 12;

    public static final float BEAR_DEFAULT_JUMPFORCE = 27;
    public static final float BEAR_DEFAULT_MAXSPEED = 6;

    //HEADLAMP
    public static final float HEADLAMP_LENGHT = 16f;
    public static final float HEADLAMP_X = 0.8f;
    public static final float HEADLAMP_Y = 1f;

    //PLAYER/HOOK
    public static final float HOOK_WIDTH = 5f;
    public static final float HOOK_HEIGHT = 5f;

    // GAMEOBJECTS
    public static final String GAME_OBJECTS = "gameobjects/game_objects.txt";

    //GAME SKIN
    public static final String GAME_UI_TEXTURE = "ui/screens/game/gameui.txt",
            GAME_UI_JSON = "ui/screens/game/gameui.json";
    //MENU SKIN
    public static final String MENU_UI_TEXTURE = "ui/screens/menu/menuui.txt",
            MENU_UI_JSON = "ui/screens/menu/menuui.json";


    //FIREFLY
    public static final float FIREFLY_SPEED = 0.001f;
    public static final float FIREFLY_OFFSET_Y = 2.8f;

    //COLLISION FILTER
    public static final short ZERO = 1;
    public static final short CATEGORY_LIGHT = 0x0001;
    public static final short CATEGORY_PLAYER = 0x0002;
    public static final short CATEGORY_MONSTER = 0x0004;
    public static final short CATEGORY_SCENERY = 0x0008;
    public static final short CATEGORY_XRAY = 0x0010;

    public static final short MASK_PLAYER = CATEGORY_PLAYER  | CATEGORY_SCENERY | CATEGORY_MONSTER;
    public static final short MASK_MONSTER = CATEGORY_PLAYER | CATEGORY_SCENERY | CATEGORY_XRAY | CATEGORY_MONSTER ;
    public static final short MASK_SCENERY = CATEGORY_MONSTER | CATEGORY_PLAYER | CATEGORY_SCENERY | CATEGORY_XRAY;
    public static final short MASK_LIGHT =  CATEGORY_SCENERY | CATEGORY_PLAYER | CATEGORY_MONSTER;
    public static final short MASK_XRAY =  CATEGORY_MONSTER | CATEGORY_SCENERY ;

    //WORLD PARAMETERS (BOX2D)

    //PIXEL PER METER SCALE
    public static final float PPM = 10f;

    //GRAVITY
    public static final float GRAVITY = -98f;

    //UI VIEWPORT
    public static final float UI_VIEWPORT_WIDTH = 800;
    public static final float UI_VIEWPORT_HEIGHT = 600;

    //GAMESCREEN VIEWPORT
    public static final float GAME_VIEWPORT_WIDTH = 320;
    public static final float GAME_VIEWPORT_HEIGHT = 192;
    //GAMECAM ZOOM
    public static final float DEFAULT_ZOOM = 0.25f;

    // TILEDMAP LAYER
    public static final int TILELAYERS = 7;

    public static final int LAYER_GROUNDS = TILELAYERS;
    public static final int LAYER_WALLS = TILELAYERS + 1;
    public static final int LAYER_POLYLINES = TILELAYERS + 2;
    public static final int LAYER_RAMPS = TILELAYERS + 3;
    public static final int LAYER_SlIDES = TILELAYERS + 4;
    public static final int LAYER_TORCHES = TILELAYERS + 5;

    public static final int LAYER_PLAYER = TILELAYERS + 6 ;
    public static final int LAYER_MAGE = TILELAYERS + 7;

    public static final int LAYER_KINGFISHER = TILELAYERS + 8;
    public static final int LAYER_BEARS = TILELAYERS + 9;

    public static final int LAYER_SUNSTONES = TILELAYERS + 10;
    public static final int LAYER_ALTARS = TILELAYERS + 11;

    public static final int LAYER_WATER = TILELAYERS + 12;

    public static final int LAYER_HOOKTREASURE = TILELAYERS + 13;
    public static final int LAYER_POGOTREASURE = TILELAYERS + 14;

    public static final int LAYER_LAVAPLATFORMS = TILELAYERS + 15;
    public static final int LAYER_HEALTHPOTION = TILELAYERS + 16;


    //LAYER VIEWPORTS


    // SKY

    public static final float LAYER0_VIEWPORT_WIDTH = 850;
    public static final float LAYER0_VIEWPORT_HEIGHT = 800;
    public static final float LAYER0COPY_VIEWPORT_WIDTH = 950;
    public static final float LAYER0COPY_VIEWPORT_HEIGHT = 900;
    public static final float LAYER0_OFFSET_X = 130;
    public static final float LAYER0_OFFSET_Y = 210;

    //MOUNTAINS
    public static final float LAYER1_VIEWPORT_WIDTH = 820;
    public static final float LAYER1_VIEWPORT_HEIGHT = 1492;
    public static final float LAYER1COPY_VIEWPORT_WIDTH = 820;
    public static final float LAYER1COPY_VIEWPORT_HEIGHT = 1192;
    public static final float LAYER1_OFFSET_X = 290;
    public static final float LAYER1_OFFSET_Y = 370;

    //HILLS
    public static final float LAYER2_VIEWPORT_WIDTH = 1220;
    public static final float LAYER2_VIEWPORT_HEIGHT = 1192;
    public static final float LAYER2COPY_VIEWPORT_WIDTH = 420;
    public static final float LAYER2COPY_VIEWPORT_HEIGHT = 392;
    public static final float LAYER2_OFFSET_X = 100;
    public static final float LAYER2_OFFSET_Y = 285;

    //PLATEAU
    public static final float LAYER3_VIEWPORT_WIDTH = 1120;
    public static final float LAYER3_VIEWPORT_HEIGHT = 1092;
    public static final float LAYER3COPY_VIEWPORT_WIDTH = 420;
    public static final float LAYER3COPY_VIEWPORT_HEIGHT = 392;
    public static final float LAYER3_OFFSET_X = 100;
    public static final float LAYER3_OFFSET_Y = 125;

    // ANIMATION SPEEDS
    public static final float PLAYER_IDLE_ANIMATION_SPEED = 1;
    public static final float PLAYER_RUN_ANIMATION_SPEED = 0.09f;
    public static final float PLAYER_JUMP_UP__ANIMATION_SPEED = 1f;
    public static final float PLAYER_FALL_DOWN__ANIMATION_SPEED = 1f;
    public static final float PLAYER_STANDING_FF_ANIMATION_SPEED = 1f;
    public static final float PLAYER_RUNNING_FF_ANIMATION_SPEED = 0.1f;
    public static final float PLAYER_POGO_ANIMATION_SPEED = 1f;


    public static final float BEAR_IDLE_ANIMATION_SPEED = 2;
    public static final float BEAR_WALK_ANIMATION_SPEED = 0.2f;
    public static final float BEAR_ATTACK_ANIMATION_SPEED = 0.3f;


    public static final float MAGE_IDLE_ANIMATION_SPEED = 2;
    public static final float MAGE_WALK_ANIMATION_SPEED = 0.2f;



    // INPUT RECTS
    public static final int LEFT_BUTTON_X = 0 , LEFT_BUTTON_Y = 0,
            LEFT_BUTTON_WIDTH = 150, LEFT_BUTTON_HEIGHT = 150;
    public static final int RIGHT_BUTTON_X = 180, RIGHT_BUTTON_Y = 0,
            RIGHT_BUTTON_WIDTH = 150, RIGHT_BUTTON_HEIGHT = 150;
    public static final int JUMP_BUTTON_X = 570, JUMP_BUTTON_Y = 0,
            JUMP_BUTTON_WIDTH = 250, JUMP_BUTTON_HEIGHT = 100;
    public static final int THROW_BUTTON_X = 650, THROW_BUTTON_Y = 150,
            THROW_BUTTON_WIDTH = 150, THROW_BUTTON_HEIGHT = 110;

    public static final int TOOLMENU_BUTTON_X = 490, TOOLMENU_BUTTON_Y = 60,
            TOOLMENU_BUTTON_WIDTH = 50, TOOLMENU_BUTTON_HEIGHT = 30;

    public static final int TOOLMENU_QUICKSLOT1_X = 390, TOOLMENU_QUICKSLOT1_Y = 60,
            TOOLMENU_QUICKSLOT1_WIDTH = 50, TOOLMENU_QUICKSLOT1_HEIGHT = 50;

    public static final int TOOLMENU_QUICKSLOT2_X = 450, TOOLMENU_QUICKSLOT2_Y = 120,
            TOOLMENU_QUICKSLOT2_WIDTH = 50, TOOLMENU_QUICKSLOT2_HEIGHT = 50;

    public static final int TOOLMENU_QUICKSLOT3_X = 530, TOOLMENU_QUICKSLOT3_Y = 120,
            TOOLMENU_QUICKSLOT3_WIDTH = 50, TOOLMENU_QUICKSLOT3_HEIGHT = 50;


    public static final int TOOLMENU_TOP_LEFT_BUTTON_X = 280, TOOLMENU_TOP_LEFT_BUTTON_Y = 380,
            TOOLMENU_TOP_LEFT_BUTTON_WIDTH = 100, TOOLMENU_TOP_LEFT_BUTTON_HEIGHT = 90;

    public static final int TOOLMENU_TOP_RIGHT_BUTTON_X = 415, TOOLMENU_TOP_RIGHT_BUTTON_Y = 380,
            TOOLMENU_TOP_RIGHT_BUTTON_WIDTH = 100, TOOLMENU_TOP_RIGHT_BUTTON_HEIGHT = 90;

    public static final int TOOLMENU_BOTTOM_LEFT_BUTTON_X = 280, TOOLMENU_BOTTOM_LEFT_BUTTON_Y = 240,
            TOOLMENU_BOTTOM_LEFT_BUTTON_WIDTH = 100, TOOLMENU_BOTTOM_LEFT_BUTTON_HEIGHT = 80;

    public static final int TOOLMENU_BOTTOM_RIGHT_BUTTON_X = 415, TOOLMENU_BOTTOM_RIGHT_BUTTON_Y = 240,
            TOOLMENU_BOTTOM_RIGHT_BUTTON_WIDTH = 100, TOOLMENU_BOTTOM_RIGHT_BUTTON_HEIGHT = 80;



    //FONT
    public static final String FONT_FNT = "pixelMYZ.fnt",
            FONT_PNG = "pixelMYZ.png";

    public static final String FONTS_FNT = "pixelMYZS.fnt",
            FONTS_PNG = "pixelMYZS.png";

    public static final String PRESSSTART_FNT = "pressStart.fnt",
            PRESSSTART_PNG = "pressStart.png";
    //NEW

    //public static final [type] [name] = [value];



}

