package com.lighthearted.screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.lighthearted.LightheartEd;
import com.lighthearted.utils.Constants;

import box2dLight.ChainLight;

public class MenuScreen extends AbstractScreen {
    private static final String TAG = MenuScreen.class.getName();

    private LightheartEd main;

    private Stage stage;
    private OrthographicCamera UIcam;


    public MenuScreen(LightheartEd main){
        this.main = main;

        setupCam();
        setupStage();
    }

    public void setupCam(){
        UIcam = new OrthographicCamera();
        UIcam.setToOrtho(false, Constants.UI_VIEWPORT_WIDTH, Constants.UI_VIEWPORT_HEIGHT);
        UIcam.update();
    }

    @Override
    public void render(float delta) {
        stage.act(delta);
        stage.draw();
    }

    public void setupStage(){
        Gdx.app.log(TAG, "stage init");
        stage = new Stage(new StretchViewport(UIcam.viewportWidth, UIcam.viewportHeight));
        stage.clear();
        stage.getViewport().setCamera(UIcam);

        Table backgroundTable = buildBackgroundTable();
        Table casualButtonTable = buildCasualButtonTable();
        Table hardButtonTable = buildHardButtonTable();

        Stack stageStack = new Stack();
        stageStack.setSize(UIcam.viewportWidth, UIcam.viewportHeight);
        stageStack.add(backgroundTable);
        stageStack.add(casualButtonTable);
        stageStack.add(hardButtonTable);
        stage.addActor(stageStack);

        Gdx.input.setInputProcessor(stage);
    }

    private Table buildBackgroundTable() {
        Table table = new Table();
        Image menubBackgroundImage = new Image(main.assets.skinMenuUI, "menubg");

        table.add(menubBackgroundImage).width(stage.getWidth()).height(stage.getHeight());
        return table;
    }

    private Table buildCasualButtonTable() {
        Table table = new Table();
        table.center();
        Button casualButton = new Button(main.assets.skinMenuUI, "casual");

        casualButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                main.setScreen(new LoadingScreen(main, false));

            }
        });

        table.add(casualButton).width(stage.getWidth() / 4).height(stage.getHeight() / 7).padBottom(100);
        return table;
    }

    private Table buildHardButtonTable() {
        Table table = new Table();
        table.center();
        Button hardButton = new Button(main.assets.skinMenuUI, "hard");

        hardButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                main.setScreen(new LoadingScreen(main, true));

            }
        });

        table.add(hardButton).width(stage.getWidth() / 4).height(stage.getHeight() / 7).padTop(100);
        return table;
    }


    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
