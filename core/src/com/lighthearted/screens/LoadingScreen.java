package com.lighthearted.screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.lighthearted.LightheartEd;
import com.lighthearted.utils.Constants;

public class LoadingScreen extends AbstractScreen{
    private static final String TAG = LoadingScreen.class.getName();

    private LightheartEd main;
    private OrthographicCamera UIcam;
    private float loadingtime = 0;
    private float randR, randG, randB;
    private boolean hardmode = false;

    public LoadingScreen(LightheartEd main, boolean hardmode){
        this.main = main;
        this.hardmode = hardmode;

        setupCam();
        randR = MathUtils.random(50,120);
        randG = MathUtils.random(0,10);
        randB = MathUtils.random(30,150);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(randR / 255f, randG  / 255f, randB / 255f, 0.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        debug();
        loadingtime += delta;
        if(loadingtime > 1f){
            main.setScreen(new GameScreen(main, hardmode));
        }
        super.render(delta);
    }

    public void setupCam(){
        UIcam = new OrthographicCamera();
        UIcam.setToOrtho(false, Constants.UI_VIEWPORT_WIDTH, Constants.UI_VIEWPORT_HEIGHT);
        UIcam.update();
    }


    public void debug(){
        main.batch.setProjectionMatrix(UIcam.combined);
        main.batch.begin();
        main.assets.font.draw(main.batch, "loading...",
                Constants.UI_VIEWPORT_WIDTH * 0.7f, Constants.UI_VIEWPORT_HEIGHT * 0.9f);
        main.batch.end();

    }
}
