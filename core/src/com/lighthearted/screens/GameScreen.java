package com.lighthearted.screens;

import com.badlogic.gdx.Gdx;
import com.lighthearted.LightheartEd;
import com.lighthearted.game.GameEngine;

public class GameScreen extends AbstractScreen {
    private static final String TAG = GameScreen.class.getName();

    private LightheartEd main;

    //consists of all modules needed to render the game
    private GameEngine gameEngine;


    public GameScreen(LightheartEd main, boolean hardmode){
        this.main = main;


        //load gameengine, level 0 (later replaced by input from chooselevelscreen)
        gameEngine = new GameEngine(main, hardmode);

    }


    //this method is called 60 times per second -> main gameloop
    @Override
    public void render(float delta) {
        gameEngine.run(delta);
    }

    @Override
    public void resize(int width, int height)
    {
        GameEngine.stageHandler.stage.getViewport().update(width, height, true);

    }

    @Override
    public void hide() {

        super.hide();
    }

    @Override
    public void pause() {

        super.pause();
    }

    @Override
    public void dispose() {
        //GameEngine.physics.b2DWorldCreator.save();
        super.dispose();
    }

    @Override
    public void resume() {
        //gameEngine.physics.b2DWorldCreator.load();
        super.resume();
    }


}
