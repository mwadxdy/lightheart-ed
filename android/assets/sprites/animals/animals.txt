animals.png
size: 202, 406
format: RGBA8888
filter: Nearest,Nearest
repeat: none
BearAttack0
  rotate: false
  xy: 1, 1
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearAttack1
  rotate: false
  xy: 69, 1
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearAttack2
  rotate: false
  xy: 137, 1
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearAttackDark0
  rotate: false
  xy: 1, 69
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearAttackDark1
  rotate: false
  xy: 69, 69
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearAttackDark2
  rotate: false
  xy: 137, 69
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearIdle0
  rotate: false
  xy: 1, 137
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearIdle1
  rotate: false
  xy: 69, 137
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearIdleDark0
  rotate: false
  xy: 137, 137
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearIdleDark1
  rotate: false
  xy: 1, 205
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalk0
  rotate: false
  xy: 69, 205
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalk1
  rotate: false
  xy: 137, 205
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalk2
  rotate: false
  xy: 1, 273
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalkDark0
  rotate: false
  xy: 69, 273
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalkDark1
  rotate: false
  xy: 137, 273
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
BearWalkDark2
  rotate: false
  xy: 1, 341
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
Kingfisher0
  rotate: false
  xy: 69, 341
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
Kingfisher1
  rotate: false
  xy: 137, 341
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
