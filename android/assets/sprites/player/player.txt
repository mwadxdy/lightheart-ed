player.png
size: 862, 34
format: RGBA8888
filter: Nearest,Nearest
repeat: none
PlayerFallDown0
  rotate: false
  xy: 1, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerFallDown1
  rotate: false
  xy: 37, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerIdle0
  rotate: false
  xy: 73, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerIdle1
  rotate: false
  xy: 109, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerJumpUp0
  rotate: false
  xy: 145, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerJumpUp1
  rotate: false
  xy: 181, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRun0
  rotate: false
  xy: 217, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRun1
  rotate: false
  xy: 253, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRun2
  rotate: false
  xy: 289, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRun3
  rotate: false
  xy: 325, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFF0
  rotate: false
  xy: 361, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFF1
  rotate: false
  xy: 397, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFF2
  rotate: false
  xy: 433, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFF3
  rotate: false
  xy: 469, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFFTurned0
  rotate: false
  xy: 505, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFFTurned1
  rotate: false
  xy: 541, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFFTurned2
  rotate: false
  xy: 577, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerRunningFFTurned3
  rotate: false
  xy: 613, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerSalto0
  rotate: false
  xy: 649, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerSalto1
  rotate: false
  xy: 685, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerStandingFF0
  rotate: false
  xy: 721, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerStandingFF1
  rotate: false
  xy: 757, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerStandingFFTurned0
  rotate: false
  xy: 793, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
PlayerStandingFFTurned1
  rotate: false
  xy: 829, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
