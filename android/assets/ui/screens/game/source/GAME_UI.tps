<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/wagne/Desktop/LightheartEd/android/assets/ui/screens/game/source/GAME_UI.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../gameui.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../pyxel_output/hud/game/GU_ballButton_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_down_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_heart_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_hookButton_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_jump_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_key_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_left_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_lightbar_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_lightbarframe_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_menu_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_musicOff_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_musicOn_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_pogoButton_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_restart_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_right_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_toolButton_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_toolMenuButton_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_toolMenuPanel_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/GU_up_0.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/ballButton.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/down.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/heart.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/hookButton.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/jump.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/key.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/left.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/lightbar.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/lightbarframe.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/menu.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/musicOff.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/musicOn.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/pogoButton.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/restart.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/right.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/toolButton.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/toolMenuButton.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/toolMenuPanel.png</key>
            <key type="filename">../../../../pyxel_output/hud/game/up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../pyxel_output/hud/game</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
