grounds.png
size: 280, 142
format: RGBA8888
filter: Nearest,Nearest
repeat: none
ground
  rotate: false
  xy: 1, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 0
ground
  rotate: false
  xy: 37, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 1
ground
  rotate: false
  xy: 73, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 2
ground
  rotate: false
  xy: 109, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 3
ground
  rotate: false
  xy: 145, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 4
ground
  rotate: false
  xy: 181, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 5
ground
  rotate: false
  xy: 217, 1
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 6
ground
  rotate: false
  xy: 1, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 7
ground
  rotate: false
  xy: 37, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 8
ground
  rotate: false
  xy: 73, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 9
ground
  rotate: false
  xy: 109, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 10
ground
  rotate: false
  xy: 145, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 11
ground
  rotate: false
  xy: 181, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 12
ground
  rotate: false
  xy: 217, 37
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 13
ground
  rotate: false
  xy: 1, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 14
ground
  rotate: false
  xy: 37, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 15
ground
  rotate: false
  xy: 73, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 16
ground
  rotate: false
  xy: 109, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 17
ground
  rotate: false
  xy: 145, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 18
ground
  rotate: false
  xy: 181, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 19
ground
  rotate: false
  xy: 217, 73
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 20
ground
  rotate: false
  xy: 1, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 21
ground
  rotate: false
  xy: 37, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 22
ground
  rotate: false
  xy: 73, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 23
ground
  rotate: false
  xy: 109, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 24
ground
  rotate: false
  xy: 145, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 25
ground
  rotate: false
  xy: 181, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 26
ground
  rotate: false
  xy: 217, 109
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: 27
