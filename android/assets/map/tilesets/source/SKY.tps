<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/wagne/Desktop/LightheartEd/android/assets/map/tilesets/source/SKY.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>720</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../sky.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_000.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_001.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_002.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_003.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_004.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_005.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_006.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_007.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_008.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_009.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_010.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_011.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_012.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_013.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_014.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_015.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_016.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_017.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_018.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_019.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_020.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_021.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_022.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_023.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_024.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_025.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_026.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_027.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_028.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_029.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_030.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_031.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_032.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_033.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_034.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_035.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_036.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_037.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_038.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_039.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_040.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_041.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_042.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_043.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_044.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_045.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_046.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_047.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_048.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_049.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_050.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_051.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_052.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_053.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_054.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_055.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_056.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_057.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_058.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_059.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_060.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_061.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_062.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_063.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_064.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_065.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_066.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_067.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_068.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_069.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_070.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_071.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_072.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_073.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_074.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_075.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_076.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_077.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_078.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_079.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_080.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_081.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_082.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_083.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_084.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_085.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_086.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_087.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_088.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_089.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_090.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_091.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_092.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_093.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_094.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_095.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_096.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_097.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_098.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_099.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_100.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_101.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_102.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_103.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_104.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_105.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_106.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_107.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_108.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_109.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_110.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_111.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_112.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_113.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_114.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_115.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_116.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_117.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_118.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_119.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_120.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_121.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_122.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_123.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_124.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_125.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_126.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_127.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_128.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_129.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_130.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_131.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_132.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_133.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_134.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_135.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_136.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_137.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_138.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_139.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_140.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_141.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_142.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_143.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_144.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_145.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_146.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_147.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_148.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_149.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_150.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_151.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_152.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_153.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_154.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_155.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_156.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_157.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_158.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_159.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_160.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_161.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_162.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_163.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_164.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_165.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_166.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_167.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_168.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_169.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_170.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_171.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_172.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_173.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_174.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_175.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_176.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_177.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_178.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_179.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_180.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_181.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_182.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_183.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_184.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_185.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_186.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_187.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_188.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_189.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_190.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_191.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_192.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_193.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_194.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_195.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_196.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_197.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_198.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_199.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_200.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_201.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_202.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_203.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_204.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_205.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_206.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_207.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_208.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_209.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_210.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_211.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_212.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_213.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_214.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_215.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_216.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_217.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_218.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_219.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_220.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_221.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_222.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_223.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_224.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_225.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_226.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_227.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_228.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_229.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_230.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_231.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_232.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_233.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_234.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_235.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_236.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_237.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_238.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_239.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_240.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_241.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_242.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_243.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_244.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_245.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_246.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_247.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_248.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_249.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_250.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_251.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_252.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_253.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_254.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_255.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_256.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_257.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_258.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_259.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_260.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_261.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_262.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_263.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_264.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_265.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_266.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_267.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_268.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_269.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_270.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_271.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_272.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_273.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_274.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_275.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_276.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_277.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_278.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_279.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_280.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_281.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_282.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_283.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_284.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_285.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_286.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_287.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_288.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_289.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_290.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_291.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_292.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_293.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_294.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_295.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_296.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_297.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_298.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_299.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_300.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_301.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_302.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_303.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_304.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_305.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_306.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_307.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_308.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_309.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_310.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_311.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_312.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_313.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_314.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_315.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_316.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_317.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_318.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_319.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_320.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_321.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_322.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_323.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_324.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_325.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_326.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_327.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_328.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_329.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_330.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_331.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_332.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_333.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_334.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_335.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_336.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_337.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_338.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_339.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_340.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_341.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_342.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_343.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_344.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_345.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_346.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_347.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_348.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_349.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_350.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_351.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_352.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_353.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_354.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_355.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_356.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_357.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_358.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_359.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_360.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_361.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_362.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_363.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_364.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_365.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_366.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_367.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_368.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_369.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_370.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_371.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_372.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_373.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_374.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_375.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_376.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_377.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_378.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_379.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_380.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_381.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_382.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_383.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_384.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_385.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_386.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_387.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_388.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_389.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_390.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_391.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_392.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_393.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_394.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_395.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_396.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_397.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_398.png</key>
            <key type="filename">../../../pyxel_output/map/sky/skyColorless_399.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../pyxel_output/map/sky</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
