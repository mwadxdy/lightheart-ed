<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/wagne/Desktop/LightheartEd/android/assets/map/tilesets/source/HILLS.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>720</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../hills.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../pyxel_output/map/hills/hills_000.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_001.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_002.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_003.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_004.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_005.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_006.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_007.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_008.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_009.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_010.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_011.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_012.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_013.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_014.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_015.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_016.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_017.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_018.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_019.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_020.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_021.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_022.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_023.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_024.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_025.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_026.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_027.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_028.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_029.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_030.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_031.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_032.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_033.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_034.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_035.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_036.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_037.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_038.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_039.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_040.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_041.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_042.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_043.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_044.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_045.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_046.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_047.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_048.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_049.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_050.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_051.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_052.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_053.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_054.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_055.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_056.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_057.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_058.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_059.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_060.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_061.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_062.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_063.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_064.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_065.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_066.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_067.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_068.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_069.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_070.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_071.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_072.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_073.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_074.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_075.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_076.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_077.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_078.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_079.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_080.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_081.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_082.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_083.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_084.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_085.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_086.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_087.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_088.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_089.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_090.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_091.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_092.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_093.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_094.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_095.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_096.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_097.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_098.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_099.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_100.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_101.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_102.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_103.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_104.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_105.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_106.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_107.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_108.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_109.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_110.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_111.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_112.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_113.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_114.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_115.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_116.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_117.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_118.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_119.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_120.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_121.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_122.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_123.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_124.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_125.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_126.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_127.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_128.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_129.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_130.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_131.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_132.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_133.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_134.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_135.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_136.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_137.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_138.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_139.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_140.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_141.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_142.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_143.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_144.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_145.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_146.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_147.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_148.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_149.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_150.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_151.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_152.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_153.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_154.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_155.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_156.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_157.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_158.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_159.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_160.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_161.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_162.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_163.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_164.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_165.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_166.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_167.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_168.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_169.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_170.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_171.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_172.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_173.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_174.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_175.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_176.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_177.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_178.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_179.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_180.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_181.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_182.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_183.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_184.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_185.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_186.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_187.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_188.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_189.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_190.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_191.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_192.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_193.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_194.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_195.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_196.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_197.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_198.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_199.png</key>
            <key type="filename">../../../pyxel_output/map/hills/hills_200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../pyxel_output/map/hills</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
