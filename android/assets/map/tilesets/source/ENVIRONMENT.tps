<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/wagne/Desktop/LightheartEd/android/assets/map/tilesets/source/ENVIRONMENT.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>220</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../environment.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../pyxel_output/map/environment/environment_000.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_001.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_002.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_003.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_004.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_005.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_006.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_007.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_008.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_009.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_010.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_011.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_012.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_013.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_014.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_015.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_016.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_017.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_018.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_019.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_020.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_021.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_022.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_023.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_024.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_025.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_026.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_027.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_028.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_029.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_030.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_031.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_032.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_033.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_034.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_035.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_036.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_037.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_038.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_039.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_040.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_041.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_042.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_043.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_044.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_045.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_046.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_047.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_048.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_049.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_050.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_051.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_052.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_053.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_054.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_055.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_056.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_057.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_058.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_059.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_060.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_061.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_062.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_063.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_064.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_065.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_066.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_067.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_068.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_069.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_070.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_071.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_072.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_073.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_074.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_075.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_076.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_077.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_078.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_079.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_080.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_081.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_082.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_083.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_084.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_085.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_086.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_087.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_088.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_089.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_090.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_091.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_092.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_093.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_094.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_095.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_096.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_097.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_098.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_099.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_100.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_101.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_102.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_103.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_104.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_105.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_106.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_107.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_108.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_109.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_110.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_111.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_112.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_113.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_114.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_115.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_116.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_117.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_118.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_119.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_120.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_121.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_122.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_123.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_124.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_125.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_126.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_127.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_128.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_129.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_130.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_131.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_132.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_133.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_134.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_135.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_136.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_137.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_138.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_139.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_140.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_141.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_142.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_143.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_144.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_145.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_146.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_147.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_148.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_149.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_150.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_151.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_152.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_153.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_154.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_155.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_156.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_157.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_158.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_159.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_160.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_161.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_162.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_163.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_164.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_165.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_166.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_167.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_168.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_169.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_170.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_171.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_172.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_173.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_174.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_175.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_176.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_177.png</key>
            <key type="filename">../../../pyxel_output/map/environment/environment_178.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../pyxel_output/map/environment</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
