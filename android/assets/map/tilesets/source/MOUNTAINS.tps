<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/wagne/Desktop/LightheartEd/android/assets/map/tilesets/source/MOUNTAINS.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename>../mountains.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Nearest</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>720</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Good</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../mountains.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../pyxel_output/map/mountains/mountains_000.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_001.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_002.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_003.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_004.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_005.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_006.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_007.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_008.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_009.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_010.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_011.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_012.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_013.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_014.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_015.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_016.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_017.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_018.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_019.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_020.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_021.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_022.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_023.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_024.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_025.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_026.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_027.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_028.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_029.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_030.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_031.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_032.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_033.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_034.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_035.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_036.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_037.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_038.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_039.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_040.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_041.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_042.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_043.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_044.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_045.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_046.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_047.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_048.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_049.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_050.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_051.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_052.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_053.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_054.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_055.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_056.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_057.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_058.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_059.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_060.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_061.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_062.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_063.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_064.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_065.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_066.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_067.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_068.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_069.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_070.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_071.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_072.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_073.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_074.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_075.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_076.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_077.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_078.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_079.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_080.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_081.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_082.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_083.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_084.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_085.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_086.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_087.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_088.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_089.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_090.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_091.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_092.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_093.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_094.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_095.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_096.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_097.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_098.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_099.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_100.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_101.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_102.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_103.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_104.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_105.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_106.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_107.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_108.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_109.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_110.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_111.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_112.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_113.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_114.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_115.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_116.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_117.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_118.png</key>
            <key type="filename">../../../pyxel_output/map/mountains/mountains_119.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../pyxel_output/map/mountains</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
